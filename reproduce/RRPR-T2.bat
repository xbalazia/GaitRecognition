@ECHO off
ECHO Workshop on Reproducible Research in Pattern Recognition
ECHO An Evaluation Framework and Database for MoCap-Based Gait Recognition Methods
IF NOT EXIST amc302.0 (
	ECHO Downloading database
	PAUSE
	wget --no-check-certificate https://gait.fi.muni.cz/files/extracted-302.0.zip
	unzip extracted-302.0.zip
	DEL extracted-302.0.zip
)
IF NOT EXIST skeleton.asf (
	ECHO Downloading database
	PAUSE
	wget --no-check-certificate https://gait.fi.muni.cz/files/extracted-302.0.zip
	unzip extracted-302.0.zip
	DEL extracted-302.0.zip
)
ECHO Calculating Table 2
PAUSE
java -jar GaitRecognition-RRPR-T2.jar > Output-RRPR-T2.csv