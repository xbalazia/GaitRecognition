@ECHO OFF
ECHO International Workshops on Structural and Syntactic Pattern Recognition and Statistical Techniques in Pattern Recognition
ECHO Walker-Independent Features for Gait Recognition from Motion Capture Data
IF NOT EXIST amc302.0 (
	ECHO Downloading database
	PAUSE
	wget --no-check-certificate https://gait.fi.muni.cz/files/extracted-302.0.zip
	unzip extracted-302.0.zip
	DEL extracted-302.0.zip
)
IF NOT EXIST skeleton.asf (
	ECHO Downloading database
	PAUSE
	wget --no-check-certificate https://gait.fi.muni.cz/files/extracted-302.0.zip
	unzip extracted-302.0.zip
	DEL extracted-302.0.zip
)
ECHO Calculating Figure 3
PAUSE
java -jar GaitRecognition-S+SSPR-F3.jar > Output-S+SSPR-F3.csv