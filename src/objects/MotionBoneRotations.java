package objects;

import Jama.Matrix;
import java.util.ArrayList;
import java.util.List;

public class MotionBoneRotations extends Motion {

    public MotionBoneRotations(String identifier, List<PoseBoneRotations> poses) {
        super(identifier, poses);
    }

    @Override
    public Matrix getMatrix() {
        double[][] data2 = getTensor();
        double[] dataVector = new double[data2.length * data2[0].length];
        int d = 0;
        for (double[] data1 : data2) {
            for (double data0 : data1) {
                dataVector[d++] = data0;
            }
        }
        return new Matrix(dataVector, dataVector.length);
    }

    private double[][] getTensor() {
        int length = getLength();
        double[][] data = new double[length][((PoseBoneRotations) getPose(0)).getBoneRotations().length];
        for (int l = 0; l < length; l++) {
            double[] boneRotations = ((PoseBoneRotations) getPose(l)).getBoneRotations();
            System.arraycopy(boneRotations, 0, data[l], 0, boneRotations.length);
        }
        return data;
    }

    @Override
    public List extractRawFeatures() {
        List<Feature> features = new ArrayList();
        int length = getLength();
        int dimension = ((PoseBoneRotations) getPose(0)).getBoneRotations().length;
        for (int d = 0; d < dimension; d++) {
            Parameter[] parameters = new Parameter[length];
            for (int l = 0; l < length; l++) {
                parameters[l] = new Parameter(String.valueOf(l) + ":" + String.valueOf(d), ((PoseBoneRotations) getPose(l)).getBoneRotations()[d]);
            }
            features.add(new Feature(parameters));
        }
        return features;
    }
}
