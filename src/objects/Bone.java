package objects;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Bone {

    private int id;
    private String name = "";
    private Triple direction = new Triple(1, 1, 1);
    private double length = 1;  // distance to translate local origin
    private Triple orientation; // angles to rotate local coordinate system
    private String axisOrder;   // order of roations (ignored for now)
    private final Set<String> dof = new HashSet();  // names of DOFs (ignored for now)
    private int dofIndex;   // pointer into dofValue list
    private final List<Triple> dofValues = new ArrayList(); // DOF values (from .AMC file)
    private final Limit[] limit = {new Limit(-360, 360), new Limit(-360, 360), new Limit(-360, 360)};				// limits on DOFs
    private final Set<Bone> childs = new LinkedHashSet();   // child of this bone
    private Triple position = new Triple(0, 0, 0);  // origin of local coordinate system
    private double[][] tucs;    // (local) transformed unit coordinate system

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Triple getDirection() {
        return direction;
    }

    public void setDirection(Triple direction) {
        this.direction = direction;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public Triple getOrientation() {
        return orientation;
    }

    public void setOrientation(Triple orientation) {
        this.orientation = orientation;
    }

    public String getAxisOrder() {
        return axisOrder;
    }

    public void setAxisOrder(String axisOrder) {
        this.axisOrder = axisOrder;
    }

    public int getNumberOfDOFs() {
        return dof.size();
    }

    public Set<String> getDof() {
        return dof;
    }

    public int getDofIndex() {
        return dofIndex;
    }

    public void setDofIndex(int dofIndex) {
        this.dofIndex = dofIndex;
    }

    public List<Triple> getDofValues() {
        return dofValues;
    }

    public Limit getLimit(int i) {
        return limit[i];
    }

    public void setLimit(int i, Limit limit) {
        this.limit[i] = limit;
    }

    public Set<Bone> getChilds() {
        return childs;
    }

    public Triple getPosition() {
        return position;
    }

    public void setPosition(Triple position) {
        this.position = position;
    }

    public double[][] getTucs() {
        return tucs;
    }

    public void setTucs(double[][] tucs) {
        this.tucs = tucs;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Bone) {
            Bone b = (Bone) obj;
            return b.id == this.id;
        }
        return false;
    }
}
