package objects;

import Jama.Matrix;

public final class Template extends Sample {

    public Template(String identifier, Matrix matrix) {
        super(identifier, matrix);
    }

    public void addFeature(Feature feature) {
        int length = feature.getLength();
        int dimension = getDimension();
        double[][] newData = new double[dimension + 1][length];
        if (dimension == 0) {
            for (int l = 0; l < length; l++) {
                newData[0][l] = feature.getParameter(l).getValue();
            }
            setMatrix(new Matrix(newData));
        } else {
            if (length == getLength()) {
                for (int l = 0; l < length; l++) {
                    for (int d = 0; d < dimension; d++) {
                        newData[d][l] = getParameterValue(d, l);
                    }
                    newData[dimension][l] = feature.getParameter(l).getValue();
                }
                setMatrix(new Matrix(newData));
            } else {
                System.out.println("wrong length");
            }
        }
    }
}
