package objects;

import executor.Constants;
import java.util.List;

public class PoseJointCoordinates extends Pose {

    private final List<Joint> joints;

    public PoseJointCoordinates(List<Joint> joints) {
        this.joints = joints;
    }

    public List<Joint> getJoints() {
        return joints;
    }

    public Joint getJoint(String jointName) {
        int i = 0;
        while (!jointName.equals(joints.get(i).getName())) {
            i++;
        }
        return joints.get(i);
    }

    public Parameter extractInterJointDistanceParameter(String jointName1, String jointName2) {
        return new Parameter(jointName1 + "-" + jointName2, getJoint(jointName1).getDistanceL2To(getJoint(jointName2)));
    }

    public Parameter extractJointAngleParameter(String jointName1, String jointName2, String jointName3) {
        Triple vector1 = getJoint(jointName2).getCoordinates().minus(getJoint(jointName1).getCoordinates());
        Triple vector2 = getJoint(jointName2).getCoordinates().minus(getJoint(jointName3).getCoordinates());
        double invCosineVal = vector1.dotProduct(vector2) / (vector1.getMagnitude() * vector2.getMagnitude());
        return new Parameter(jointName1 + "-" + jointName2 + "-" + jointName3, (Math.acos(invCosineVal) * ((double) 180 / Math.PI)));
    }

    public Parameter extractJointAxisCoordinateParameter(Axis axis, String jointName) {
        Triple coordinates = getJoint(jointName).getCoordinates();
        double value = coordinates.x * axis.is(Constants.axes[0]) + coordinates.y * axis.is(Constants.axes[1]) + coordinates.z * axis.is(Constants.axes[2]);
        return new Parameter(axis.getLabel() + ":" + jointName, value);
    }

    public Parameter extractJointAxisDistanceParameter(Axis axis, String jointName1, String jointName2) {
        return new Parameter(axis.getLabel() + ":" + jointName1 + "-" + jointName2, Math.abs(extractJointAxisCoordinateParameter(axis, jointName1).getValue() - extractJointAxisCoordinateParameter(axis, jointName2).getValue()));
    }

    public Parameter extractBoneAxisAngleParameter(Axis axis, String jointName1, String jointName2) {
        Triple coordinates1 = getJoint(jointName1).getCoordinates();
        Triple coordinates2 = getJoint(jointName2).getCoordinates();
        Triple coordinates3 = new Triple(coordinates2.x + axis.is(Constants.axes[0]), coordinates2.y + axis.is(Constants.axes[1]), coordinates2.z + axis.is(Constants.axes[2]));
        Triple vector1 = coordinates2.minus(coordinates1);
        Triple vector2 = coordinates2.minus(coordinates3);
        double invCosineVal = vector1.dotProduct(vector2) / (vector1.getMagnitude() * vector2.getMagnitude());
        return new Parameter(jointName1 + "-" + jointName2 + ":" + axis.getLabel(), Math.acos(invCosineVal) * ((double) 180 / Math.PI));
    }

    public Parameter extractCentroidsDistanceParameter(String[] jointNames1, String[] jointNames2) {
        Joint centroidJoint1 = extractCentroidJoint(jointNames1);
        Joint centroidJoint2 = extractCentroidJoint(jointNames2);
        return new Parameter(centroidJoint1.getName() + "--" + centroidJoint2.getName(), centroidJoint1.getDistanceL2To(centroidJoint2));
    }

    public Parameter extractTriangleAreaParameter(String jointName1, String jointName2, String jointName3) { //Heron formula
        double a = getJoint(jointName1).getDistanceL2To(getJoint(jointName2));
        double b = getJoint(jointName2).getDistanceL2To(getJoint(jointName3));
        double c = getJoint(jointName3).getDistanceL2To(getJoint(jointName1));
        double s = (a + b + c) / 2;
        return new Parameter("area:" + jointName1 + "-" + jointName2 + "-" + jointName3, Math.sqrt(s * (s - a) * (s - b) * (s - c)));
    }

    public Parameter extractProjectedPolygonAreaParameter(String[] jointNames, Axis projectionAxis1, Axis projectionAxis2) { //Shoelace formula
        int length = jointNames.length;
        double sum1 = 0;
        double sum2 = 0;
        for (int i = 0; i < length - 1; i++) {
            sum1 += getJoint(jointNames[i]).getCoordinates().getElementByAxis(projectionAxis1) * getJoint(jointNames[i + 1]).getCoordinates().getElementByAxis(projectionAxis2);
            sum2 += getJoint(jointNames[i + 1]).getCoordinates().getElementByAxis(projectionAxis1) * getJoint(jointNames[i]).getCoordinates().getElementByAxis(projectionAxis2);
        }
        StringBuilder sb = new StringBuilder();
        for (String jointName : jointNames) {
            sb.append(jointName);
        }
        return new Parameter("area:" + sb, Math.abs(sum1 + getJoint(jointNames[length - 1]).getCoordinates().getElementByAxis(projectionAxis1) * getJoint(jointNames[0]).getCoordinates().getElementByAxis(projectionAxis2) - sum2 - getJoint(jointNames[0]).getCoordinates().getElementByAxis(projectionAxis1) * getJoint(jointNames[length - 1]).getCoordinates().getElementByAxis(projectionAxis2)) / 2);
    }

    private Joint extractCentroidJoint(String[] jointNames) {
        Triple centroidJointCoordinates = new Triple(0, 0, 0);
        for (String jointName : jointNames) {
            centroidJointCoordinates.plus(getJoint(jointName).getCoordinates());
        }
        centroidJointCoordinates = centroidJointCoordinates.divide(jointNames.length);
        StringBuilder sb = new StringBuilder();
        for (String jointName : jointNames) {
            sb.append(jointName);
        }
        return new Joint("centroid:" + sb.toString(), centroidJointCoordinates);
    }
}
