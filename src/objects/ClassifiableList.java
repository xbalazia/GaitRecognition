package objects;

import java.util.ArrayList;
import java.util.List;

public class ClassifiableList <T extends Classifiable> {
    
    private final List<T> objects;

    public ClassifiableList(List<T> objects) {
        this.objects = objects;
    }

    public List<List<T>> splitByIdentifier() {
        List<List<T>> objectsByIdentifier = new ArrayList();
        for (T object : objects) {
            boolean idIsInObjectsByIdentifier = false;
            for (List<T> objectsOfIdentifier : objectsByIdentifier) {
                if (object.hasSameIdentifierAs(objectsOfIdentifier.get(0))) {
                    idIsInObjectsByIdentifier = true;
                    objectsOfIdentifier.add(object);
                    break;
                }
            }
            if (!idIsInObjectsByIdentifier) {
                List<T> objectsOfIdentifier = new ArrayList();
                objectsOfIdentifier.add(object);
                objectsByIdentifier.add(objectsOfIdentifier);
            }
        }
        return objectsByIdentifier;
    }

    public List<List<T>> splitBySubject() {
        List<List<T>> objectsBySubject = new ArrayList();
        for (T object : objects) {
            boolean subjectIsInObjectsBySubject = false;
            for (List<T> objectsOfSubject : objectsBySubject) {
                if (object.hasSameSubjectAs(objectsOfSubject.get(0))) {
                    subjectIsInObjectsBySubject = true;
                    objectsOfSubject.add(object);
                    break;
                }
            }
            if (!subjectIsInObjectsBySubject) {
                List<T> objectsOfSubject = new ArrayList();
                objectsOfSubject.add(object);
                objectsBySubject.add(objectsOfSubject);
            }
        }
        return objectsBySubject;
    }

    public List<List<T>> splitBySequence() {
        List<List<T>> objectsBySequence = new ArrayList();
        for (T object : objects) {
            boolean sequenceIsInObjectsBySequence = false;
            for (List<T> objectsOfSequence : objectsBySequence) {
                if (object.hasSameSequenceAs(objectsOfSequence.get(0))) {
                    sequenceIsInObjectsBySequence = true;
                    objectsOfSequence.add(object);
                    break;
                }
            }
            if (!sequenceIsInObjectsBySequence) {
                List<T> objectsOfSequence = new ArrayList();
                objectsOfSequence.add(object);
                objectsBySequence.add(objectsOfSequence);
            }
        }
        return objectsBySequence;
    }
}
