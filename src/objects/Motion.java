package objects;

import Jama.Matrix;
import java.util.ArrayList;
import java.util.List;

public abstract class Motion<T> extends Classifiable {

    private List<Pose> poses;

    public Motion(String identifier, List<Pose> poses) {
        super(identifier);
        this.poses = poses;
    }

    public int getLength() {
        return poses.size();
    }

    public List<Pose> getPoses() {
        return poses;
    }

    public Pose getPose(int index) {
        return poses.get(index);
    }

    public void adjustLinear(int length) {
        List<Pose> adjustedPoses = new ArrayList();
        for (int l = 0; l < length; l++) {
            adjustedPoses.add(getPose(Math.round((float) l * (getLength() - 1) / (length - 1))));
        }
        this.poses = adjustedPoses;
    }

    public abstract Matrix getMatrix();

    public abstract List<Feature> extractRawFeatures();
}
