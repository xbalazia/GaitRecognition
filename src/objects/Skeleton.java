package objects;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Skeleton {

    private Set<Bone> bones = new LinkedHashSet();
    private Root root = new Root();
    private int numberOfPoses = 0;
    private String name = "";

    private Map<String, Trajectory3d> trajectories = new HashMap();

    private static final Logger logger = Logger.getLogger(Skeleton.class.getName());

    private static final String ASF_UNITS = ":units";
    private static final String ASF_DOCUMENTATION = ":documentation";
    private static final String ASF_ROOT = ":root";
    private static final String ASF_BONEDATE = ":bonedata";
    private static final String ASF_HIERARCHY = ":hierarchy";
    private static final String ASF_BEGIN = "begin";
    private static final String ASF_END = "end";
    private static final String ASF_ID = "id";
    private static final String ASF_NAME = "name";
    private static final String ASF_DIRECTION = "direction";
    private static final String ASF_LENGTH = "length";
    private static final String ASF_AXIS = "axis";
    private static final String ASF_DOF = "dof";
    private static final String ASF_LIMITS = "limits";
    private static final String ASF_HIERARCHY_ROOT = "root";
    private static final Locale USED_LOCALE = Locale.US;

    public int getNumberOfPoses() {
        return numberOfPoses;
    }

    public Bone findBone(String name) {
        if (name == null) {
            return null;
        }
        for (Bone b : bones) {
            if (name.equals(b.getName())) {
                return b;
            }
        }
        return null;
    }

    public int getNumberOfBones() {
        return bones.size();
    }

    public Set<Bone> getBones() {
        return bones;
    }

    public Trajectory3d getTrajectory3d(String b) {
        return trajectories.get(b);
    }

    public void readASF(String asfFileName) throws FileNotFoundException {
        String particle = null;

        Scanner scan = new Scanner(new File(asfFileName));
        scan.useLocale(USED_LOCALE);

        // --------------------------------------------------------
        // process ASF file
        logger.info("Scanning ASF file");
        bones = new LinkedHashSet();
        root = new Root();

        // -----------------------------------
        // find line that contains 'units'
        while (scan.hasNext()) {
            particle = scan.next();
            if (ASF_UNITS.equals(particle)) {
                break;
            }
        }

        // -----------------------------------
        // skip down to DOCUMENTATION line
        while (scan.hasNext()) {
            particle = scan.next();
            if (ASF_DOCUMENTATION.equals(particle)) {
                break;
            }
        }

        // -----------------------------------
        // skip down to ROOT line
        while (scan.hasNext()) {
            particle = scan.next();
            if (ASF_ROOT.equals(particle)) {
                break;
            }
        }

        // -----------------------------------
        // skip down to BONEDATA line
        while (scan.hasNext()) {
            particle = scan.next();
            if (ASF_BONEDATE.equals(particle)) {
                break;
            }
        }

        // found stuff to here at least
        logger.info("Bone processing started.");

        // -----------------------------------
        // PROCESS BONES
        while (scan.hasNext()) {
            particle = scan.next();
            if (ASF_HIERARCHY.equals(particle)) {
                break;
            }
            if (ASF_BEGIN.equals(particle)) {
                Bone newBone = new Bone();
                bones.add(newBone);

                // process BONE attribute-value pairs
                while (scan.hasNext()) {
                    particle = scan.next();

                    if (ASF_END.equals(particle)) {
                        break;
                    }

                    // ID
                    if (ASF_ID.equals(particle)) {
                        newBone.setId(scan.nextInt());
                    }

                    // NAME
                    if (ASF_NAME.equals(particle)) {
                        newBone.setName(scan.next());

                    }

                    // DIRECTION
                    if (ASF_DIRECTION.equals(particle)) {
                        newBone.setDirection(new Triple(scan.nextFloat(), scan.nextFloat(), scan.nextFloat()));
                    }

                    // LENGTH
                    if (ASF_LENGTH.equals(particle)) {
                        newBone.setLength(scan.nextDouble());
                    }

                    // AXIS
                    if (ASF_AXIS.equals(particle)) {
                        newBone.setOrientation(new Triple(scan.nextFloat(), scan.nextFloat(), scan.nextFloat()));
                        newBone.setAxisOrder(scan.next());
                    }

                    // DOFs
                    if (ASF_DOF.equals(particle)) {
                        String dofs = scan.nextLine();
                        Scanner sc2 = new Scanner(dofs);
                        sc2.useLocale(USED_LOCALE);
                        while (sc2.hasNext()) {
                            newBone.getDof().add(sc2.next());
                        }
                    }

                    // LIMITS
                    // assumes if any limits are given, then all limits are
                    // given
                    if (ASF_LIMITS.equals(particle)) {
                        Locale locale = scan.locale();
                        Pattern delimiter = scan.delimiter();
                        scan.useDelimiter("[\\p{javaWhitespace}\\(\\)]+");
                        for (int i = 0; i < newBone.getNumberOfDOFs(); ++i) {
                            newBone.setLimit(i, new Limit(scan.nextDouble(), scan.nextDouble()));

                        }
                        scan.reset();
                        scan.useLocale(locale);
                        scan.useDelimiter(delimiter);
                        scan.nextLine();
                    }
                }

                // END of this bone - clean up
                if (ASF_END.equals(particle)) {
                    // logger.info("Bone end.");
                }

            }

        }

        logger.info("Bone processing finished.");
        logger.info("Hierarchy processing started.");

        // -----------------------------------
        // PROCESS HIERARCHY
        if (ASF_HIERARCHY.equals(particle)) {

            // search for 'begin'
            while (scan.hasNext()) {
                particle = scan.next();
                if (ASF_BEGIN.equals(particle)) {
                    break;
                }
            }

            // get 'parent' joint
            while (scan.hasNext()) {
                Bone parentBone = null;
                String parent = scan.next();
                if (ASF_END.equals(parent)) {
                    break;
                }
                if (!ASF_HIERARCHY_ROOT.equals(parent)) {
                    parentBone = findBone(parent);
                }

                // if parent pointer is set or parent is 'root', continue
                if (parentBone != null || ASF_HIERARCHY_ROOT.equals(parent)) {
                    String line = scan.nextLine();
                    Scanner sc2 = new Scanner(line);
                    sc2.useLocale(USED_LOCALE);
                    List<Bone> siblings = new ArrayList();
                    while (sc2.hasNext()) {
                        Bone b = findBone(sc2.next());
                        if (b != null) {
                            siblings.add(b);
                        }
                    }
                    if (parentBone != null) {
                        parentBone.getChilds().addAll(siblings);
                    } else {
                        root.getChilds().addAll(siblings);
                    }
                }

            }

        } // end of hierarchy

        logger.info("Done");
    }

    public void readAMC(String amcFileName) throws FileNotFoundException {
        String particle;

        Scanner scan = new Scanner(new File(amcFileName));
        scan.useLocale(USED_LOCALE);

        // --------------------------------------------------------
        // process AMC file
        logger.info("Scanning AMC file");

        numberOfPoses = 0;
        // get the next line
        while (scan.hasNext()) {
            scan.nextLine();
            if (scan.hasNextInt()) {
                break;
            }
        }
        while (scan.hasNext()) {
            // pose number
            if (scan.hasNextInt()) {
                for (Bone b : bones) {
                    while (b.getDofValues().size() < numberOfPoses) {
                        b.getDofValues().add(new Triple(0, 0, 0));
                        // logger.info(numberOfPoses + ": "+b.getName());
                    }
                }

                ++numberOfPoses;
                scan.next(); // We don't need number of pose
            }
            particle = scan.next();
            if (ASF_HIERARCHY_ROOT.equals(particle)) {
                root.getPositions().add(new Triple(scan.nextFloat(), scan.nextFloat(), scan.nextFloat()));
                root.getOrientations().add(new Triple(scan.nextFloat(), scan.nextFloat(), scan.nextFloat()));
            } else {
                Bone b = findBone(particle);
                if (b == null) {
                    logger.log(Level.SEVERE, "Error while reading AMC file. Unknown bone {0}", particle);
                    scan.nextLine();
                } else {
                    String dofs = scan.nextLine();
                    Scanner sc2 = new Scanner(dofs);
                    sc2.useLocale(USED_LOCALE);
//					int count = b.getNumberOfDOFs();
//					if (count == 3) {
//						b.getDofValues().add(new Triple(sc2.nextDouble(), sc2.nextDouble(), sc2.nextDouble()));
//					} else if (count == 2) {
//						b.getDofValues().add(new Triple(sc2.nextDouble(), sc2.nextDouble(), 0));
//					} else if (count == 1) {
//						b.getDofValues().add(new Triple(sc2.nextDouble(), 0, 0));
//					}
                    float rotX = 0f, rotY = 0f, rotZ = 0f;
                    if (b.getDof().contains("rx")) {
                        rotX = sc2.nextFloat();
                    }
                    if (b.getDof().contains("ry")) {
                        rotY = sc2.nextFloat();
                    }
                    if (b.getDof().contains("rz")) {
                        rotZ = sc2.nextFloat();
                    }
                    b.getDofValues().add(new Triple(rotX, rotY, rotZ));

                }

            }

        }

        logger.log(Level.INFO, "Number of Poses: {0}", numberOfPoses);
        logger.info("Computing Trajectories");
        computeTrajectories();
        logger.info("Finished");
    }

    public List<Triple> drawStickFigure(int pose, boolean inplace) {
        Triple p;
        MatrixTriple tucs = new MatrixTriple();
        MatrixTriple m = new MatrixTriple();
        List<Triple> result = new ArrayList();

        while (pose > numberOfPoses) {
            pose -= numberOfPoses;
        }

        if ((pose >= 1) && (pose <= numberOfPoses)) {

            // initialize unit coordinate system
            tucs.setIdentity();

            // translate root position;
            if (inplace) {
                p = new Triple(0, 0, 0);
            } else {
                // rotate unit coordinate system according to root orientation
                m.formXrotMatrix(root.getOrientations().get(pose - 1).x);
                tucs = m.matrixMultiply(tucs);
                m.formYrotMatrix(root.getOrientations().get(pose - 1).y);
                tucs = m.matrixMultiply(tucs);
                m.formZrotMatrix(root.getOrientations().get(pose - 1).z);
                tucs = m.matrixMultiply(tucs);

                p = new Triple(root.getPositions().get(pose - 1).x, root.getPositions().get(pose - 1).y, root.getPositions().get(pose - 1).z);
            }

            for (Bone b : root.getChilds()) {
                result.addAll(drawStickSubFigure(1, b, p, tucs.copyMatrix(), pose));
            }

        }
        return result;
    }

    private List<Triple> drawStickSubFigure(int level, Bone b, Triple P, MatrixTriple tucs, int pose) {
        MatrixTriple m = new MatrixTriple();
        MatrixTriple t;
        MatrixTriple tDof = new MatrixTriple();
        MatrixTriple tOrient = new MatrixTriple();
        MatrixTriple tOrientInv = new MatrixTriple();
        Triple PP;
        List<Triple> result = new ArrayList();

        if (b == null) {
            return result;
        }

        // for rotation matrix from DOFs
        tDof.setIdentity();
        if (b.getDof().contains("rx")) {
            m.formXrotMatrix(b.getDofValues().get(pose - 1).x);
            tDof = m.matrixMultiply(tDof);
        }
        if (b.getDof().contains("ry")) {
            m.formYrotMatrix(b.getDofValues().get(pose - 1).y);
            tDof = m.matrixMultiply(tDof);
        }
        if (b.getDof().contains("rz")) {
            m.formZrotMatrix(b.getDofValues().get(pose - 1).z);
            tDof = m.matrixMultiply(tDof);
        }

        // form rotation matrix from axis orientation
        tOrient.setIdentity();
        m.formXrotMatrix(b.getOrientation().x);
        tOrient = m.matrixMultiply(tOrient);
        m.formYrotMatrix(b.getOrientation().y);
        tOrient = m.matrixMultiply(tOrient);
        m.formZrotMatrix(b.getOrientation().z);
        tOrient = m.matrixMultiply(tOrient);

        // for inverse rotation matrix from axis orientation
        tOrientInv.setIdentity();
        m.formZrotMatrix(-b.getOrientation().z);
        tOrientInv = m.matrixMultiply(tOrientInv);
        m.formYrotMatrix(-b.getOrientation().y);
        tOrientInv = m.matrixMultiply(tOrientInv);
        m.formXrotMatrix(-b.getOrientation().x);
        tOrientInv = m.matrixMultiply(tOrientInv);

        // transform the 'tucs' and apply to the origin of the next bone
        t = tucs.copyMatrix();
        t = t.matrixMultiply(tOrient);
        t = t.matrixMultiply(tDof);
        t = t.matrixMultiply(tOrientInv);

        // translate PP in the local x direction (defined by tucs) for 'length' distance
        PP = new Triple(P.x, P.y, P.z);
        PP.x += b.getLength() * b.getDirection().x * t.a[0][0];
        PP.x += b.getLength() * b.getDirection().y * t.a[0][1];
        PP.x += b.getLength() * b.getDirection().z * t.a[0][2];

        // translate P in the local y direction (defined by tucs) for 'length' distance
        PP.y += b.getLength() * b.getDirection().x * t.a[1][0];
        PP.y += b.getLength() * b.getDirection().y * t.a[1][1];
        PP.y += b.getLength() * b.getDirection().z * t.a[1][2];

        // translate P in the local z direction (defined by tucs) for 'length' distance
        PP.z += b.getLength() * b.getDirection().x * t.a[2][0];
        PP.z += b.getLength() * b.getDirection().y * t.a[2][1];
        PP.z += b.getLength() * b.getDirection().z * t.a[2][2];

        // draw from P to PP
		/*
         * glBegin(GL_LINES); glVertex3f(P.x,P.y,P.z); glVertex3f(PP.x,PP.y,PP.z); glEnd();
         */
        result.add(new Triple(P.x, P.y, P.z));
        result.add(new Triple(PP.x, PP.y, PP.z));

        for (Bone c : b.getChilds()) {
            result.addAll(drawStickSubFigure(level + 1, c, PP, t, pose));
        }
        return result;
    }

    // =================================================================
    // PRINT BONES
    public void printBones() {
        System.out.println();
        System.out.print("bones:");

        for (Bone b : bones) {
            System.out.print(" ");
            System.out.print(b.getId());
            System.out.print(":");
            System.out.print(b.getName());
        }

        System.out.println();
    }

    public Map<String, Trajectory3d> getTrajectories() {
        return trajectories;
    }

    private void computeTrajectories() {
        trajectories = new LinkedHashMap();

        trajectories.put("root", new Trajectory3d("root"));

        for (Bone b : bones) {
            trajectories.put(b.getName(), new Trajectory3d(b.getName()));
        }

        for (int i = 1; i < numberOfPoses; ++i) {
            Map<String, Triple> pos = getPositions(i);
            for (String key : trajectories.keySet()) {
                if (pos.containsKey(key)) {
                    trajectories.get(key).getPositions().add(pos.get(key));
                } else {
                    trajectories.get(key).getPositions().add(new Triple(0, 0, 0));
                }
            }
        }

    }

    private Map<String, Triple> getPositions(int pose) {
        MatrixTriple tucs = new MatrixTriple();
        MatrixTriple m = new MatrixTriple();

        Map<String, Triple> result = new HashMap();

        // initialize unit coordinate system
        tucs.setIdentity();

        // rotate unit coordinate system according to root orientation
        m.formXrotMatrix(root.getOrientations().get(pose - 1).x);
        tucs = m.matrixMultiply(tucs);
        m.formYrotMatrix(root.getOrientations().get(pose - 1).y);
        tucs = m.matrixMultiply(tucs);
        m.formZrotMatrix(root.getOrientations().get(pose - 1).z);
        tucs = m.matrixMultiply(tucs);

        result.put("root", root.getPositions().get(pose - 1));

        for (Bone c : root.getChilds()) {
            getSubPositions(1, c, root.getPositions().get(pose - 1), tucs.copyMatrix(), pose, result);
        }

        return result;
    }

    private void getSubPositions(int level, Bone b, Triple position, MatrixTriple tucs, int pose, Map<String, Triple> result) {
        Triple p;
        MatrixTriple m = new MatrixTriple();
        MatrixTriple t;
        MatrixTriple tDof = new MatrixTriple();
        MatrixTriple tOrient = new MatrixTriple();
        MatrixTriple tOrientInv = new MatrixTriple();

        if (b == null) {
            return;
        }

        // form rotation matrix from DOFs
        tDof.setIdentity();
        if (b.getDof().contains("rx")) {
            m.formXrotMatrix(b.getDofValues().get(pose - 1).x);
            tDof = m.matrixMultiply(tDof);
        }
        if (b.getDof().contains("ry")) {
            m.formYrotMatrix(b.getDofValues().get(pose - 1).y);
            tDof = m.matrixMultiply(tDof);
        }
        if (b.getDof().contains("rz")) {
            m.formZrotMatrix(b.getDofValues().get(pose - 1).z);
            tDof = m.matrixMultiply(tDof);
        }

        // form rotation matrix from axis orientation
        tOrient.setIdentity();
        m.formXrotMatrix(b.getOrientation().x);
        tOrient = m.matrixMultiply(tOrient);
        m.formYrotMatrix(b.getOrientation().y);
        tOrient = m.matrixMultiply(tOrient);
        m.formZrotMatrix(b.getOrientation().z);
        tOrient = m.matrixMultiply(tOrient);

        // form inverse rotation matrix from axis orientation
        tOrientInv.setIdentity();
        m.formZrotMatrix(-b.getOrientation().z);
        tOrientInv = m.matrixMultiply(tOrientInv);
        m.formYrotMatrix(-b.getOrientation().y);
        tOrientInv = m.matrixMultiply(tOrientInv);
        m.formXrotMatrix(-b.getOrientation().x);
        tOrientInv = m.matrixMultiply(tOrientInv);

        // transform the 'tucs' and apply to the origin of the next bone
        t = tucs.copyMatrix();
        t = t.matrixMultiply(tOrient);
        t = t.matrixMultiply(tDof);
        t = t.matrixMultiply(tOrientInv);

        // translate PP in the local x direction (defined by tucs) for 'length' distance
        p = new Triple(position.x, position.y, position.z);
        p.x += b.getLength() * b.getDirection().x * t.a[0][0];
        p.x += b.getLength() * b.getDirection().y * t.a[0][1];
        p.x += b.getLength() * b.getDirection().z * t.a[0][2];

        // translate P in the local y direction (defined by tucs) for 'length' distance
        p.y += b.getLength() * b.getDirection().x * t.a[1][0];
        p.y += b.getLength() * b.getDirection().y * t.a[1][1];
        p.y += b.getLength() * b.getDirection().z * t.a[1][2];

        // translate P in the local z direction (defined by tucs) for 'length' distance
        p.z += b.getLength() * b.getDirection().x * t.a[2][0];
        p.z += b.getLength() * b.getDirection().y * t.a[2][1];
        p.z += b.getLength() * b.getDirection().z * t.a[2][2];

        result.put(b.getName(), p);

        for (Bone c : b.getChilds()) {
            getSubPositions(level + 1, c, p, t.copyMatrix(), pose, result);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Root getRoot() {
        return root;
    }

}
