package objects;

public class MatrixTriple {

    public static int RANK = 3;
    public double[][] a = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};

    public void formXrotMatrix(double degrees) {
        double angle;

        angle = Math.PI * degrees / 180;

        a[0][0] = 1;
        a[0][1] = 0;
        a[0][2] = 0;
        a[1][0] = 0;
        a[1][1] = Math.cos(angle);
        a[1][2] = -Math.sin(angle);
        a[2][0] = 0;
        a[2][1] = Math.sin(angle);
        a[2][2] = Math.cos(angle);
    }

    // =================================================================
    // FORM YROT MATRIX
    public void formYrotMatrix(double degrees) {
        double angle;

        angle = Math.PI * degrees / 180;

        a[0][0] = Math.cos(angle);
        a[0][1] = 0;
        a[0][2] = Math.sin(angle);
        a[1][0] = 0;
        a[1][1] = 1;
        a[1][2] = 0;
        a[2][0] = -Math.sin(angle);
        a[2][1] = 0;
        a[2][2] = Math.cos(angle);
    }

    // =================================================================
    // FORM ZROT MATRIX
    public void formZrotMatrix(double degrees) {
        double angle;

        angle = Math.PI * degrees / 180;

        a[0][0] = Math.cos(angle);
        a[0][1] = -Math.sin(angle);
        a[0][2] = 0;
        a[1][0] = Math.sin(angle);
        a[1][1] = Math.cos(angle);
        a[1][2] = 0;
        a[2][0] = 0;
        a[2][1] = 0;
        a[2][2] = 1;
    }

    // =================================================================
    // SET IDENTITY
    public void setIdentity() {
        a[0][0] = 1;
        a[0][1] = 0;
        a[0][2] = 0;
        a[1][0] = 0;
        a[1][1] = 1;
        a[1][2] = 0;
        a[2][0] = 0;
        a[2][1] = 0;
        a[2][2] = 1;
    }

    // =================================================================
    // COPY MATRIX A = B
    public MatrixTriple copyMatrix() {
        MatrixTriple m = new MatrixTriple();

        m.a[0][0] = this.a[0][0];
        m.a[0][1] = this.a[0][1];
        m.a[0][2] = this.a[0][2];
        m.a[1][0] = this.a[1][0];
        m.a[1][1] = this.a[1][1];
        m.a[1][2] = this.a[1][2];
        m.a[2][0] = this.a[2][0];
        m.a[2][1] = this.a[2][1];
        m.a[2][2] = this.a[2][2];
        return m;
    }

    // =================================================================
    // MATRIX MULTIPLY result = this x m
    public MatrixTriple matrixMultiply(MatrixTriple m) {
        MatrixTriple t = new MatrixTriple();

        for (int i = 0; i < RANK; ++i) {
            for (int j = 0; j < RANK; ++j) {
                t.a[i][j] = 0;
                for (int k = 0; k < RANK; ++k) {
                    t.a[i][j] += this.a[i][k] * m.a[k][j];
                }
            }
        }

        return t;
    }

    // =================================================================
    // PRINT MATRIX
    public void printMatrix() {
        for (int i = 0; i < RANK; i++) {
            for (int j = 0; j < RANK; j++) {
                System.out.print(a[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }

    }
}
