package objects;

import java.io.Serializable;

public class Parameter implements Serializable {

    private final String name;
    private final double value;

    public Parameter(String name, double value) {
        this.name = name;
        this.value = (Math.round(value * Math.pow(10, 3)) / Math.pow(10, 3));
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }
}
