package objects;

import java.io.Serializable;

public class Axis implements Serializable {

    private final String label;

    public Axis(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public int is(Axis axis) {
        return label.equals(axis.getLabel()) ? 1 : 0;
    }
}
