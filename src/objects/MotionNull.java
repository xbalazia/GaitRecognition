package objects;

import Jama.Matrix;
import java.util.List;

public class MotionNull extends Motion {

    public MotionNull(String identifier, List<Pose> poses) {
        super(identifier, poses);
    }

    @Override
    public Matrix getMatrix() {
        return null;
    }

    @Override
    public List extractRawFeatures() {
        return null;
    }
}
