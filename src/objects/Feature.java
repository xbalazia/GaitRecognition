package objects;

import java.io.Serializable;
import java.util.Arrays;

public class Feature implements Serializable {

    private Parameter[] parameters;

    public Feature(Parameter[] parameters) {
        this.parameters = parameters;
    }

    public Feature(Parameter parameter) {
        this.parameters = new Parameter[]{parameter};
    }

    public String getName() {
        return parameters[0].getName();
    }

    public double[] getParameterValues() {
        double[] parameterValues = new double[parameters.length];
        for (int l = 0; l < parameters.length; l++) {
            parameterValues[l] = parameters[l].getValue();
        }
        return parameterValues;
    }

    public Parameter getParameter(int index) {
        return parameters[index];
    }

    public void setParameter(int index, Parameter parameter) {
        parameters[index] = parameter;
    }

    public int getLength() {
        return parameters.length;
    }

    public Feature getSubFeature(int from, int to) {
        int length = getLength();
        if (to > length) {
            System.out.println("subfeature incomplete: " + to + ">" + length);
        }
        Parameter[] subParameters = new Parameter[to - from];
        for (int l = from; l < to; l++) {
            subParameters[l - from] = parameters[l];
        }
        return new Feature(subParameters);
    }

    public float getMean() {
        float sumValue = 0f;
        for (Parameter parameter : parameters) {
            sumValue += parameter.getValue();
        }
        return sumValue / getLength();
    }

    public float getSkew() {
        return getNthCentralMoment(3) / (float) Math.pow(getStD(), 3);
    }

    public float getStD() {
        float mean = getMean();
        float sumStandardDeviation = 0f;
        for (Parameter parameter : parameters) {
            sumStandardDeviation += Math.abs(parameter.getValue() - mean);
        }
        return sumStandardDeviation / getLength();
    }

    public float getNthCentralMoment(int n) { // n=2 variance
        float mean = getMean();
        float sumFeatureVariance = 0f;
        for (int l = 0; l < getLength(); l++) {
            sumFeatureVariance += Math.pow(getParameter(l).getValue() - mean, n);
        }
        return sumFeatureVariance / getLength();
    }

    public double getMin() {
        double max = Double.MAX_VALUE;
        for (Parameter parameter : parameters) {
            double value = parameter.getValue();
            if (max > value) {
                max = value;
            }
        }
        return max;
    }

    public double getMax() {
        double max = 0;
        for (Parameter parameter : parameters) {
            double value = parameter.getValue();
            if (max < value) {
                max = value;
            }
        }
        return max;
    }

    public double getMeanDiff() { // mean difference between subsequent poses
        double sumDiff = 0f;
        for (int l = 1; l < getLength(); l++) {
            sumDiff += Math.abs(getParameter(l - 1).getValue() - getParameter(l).getValue());
        }
        return sumDiff / getLength();
    }

    public void adjust(int length) {
        Parameter[] adjustedParameters = new Parameter[length];
        for (int l = 0; l < length; l++) {
            double percent = (double) l / (length - 1);
            int p = (int) Math.round(percent * (getLength() - 1));
            adjustedParameters[l] = getParameter(p);
        }
        parameters = adjustedParameters;
    }

    @Override
    public String toString() {
        return getName() + Arrays.toString(getParameterValues());
    }
}
