package objects;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Root {

    private final List<Triple> positions = new ArrayList();
    private final List<Triple> orientations = new ArrayList();
    private final Set<Bone> childs = new LinkedHashSet();

    public Set<Bone> getChilds() {
        return childs;
    }

    public List<Triple> getPositions() {
        return positions;
    }

    public List<Triple> getOrientations() {
        return orientations;
    }
}
