package objects;

import Jama.Matrix;
import executor.Constants;
import java.util.ArrayList;
import java.util.List;

public class MotionJointCoordinates extends Motion {

    public MotionJointCoordinates(String identifier, List<PoseJointCoordinates> poses) {
        super(identifier, poses);
    }

    @Override
    public Matrix getMatrix() {
        double[][][] data3 = getTensor();
        double[] dataVector = new double[data3.length * data3[0].length * data3[0][0].length];
        int d = 0;
        for (double[][] data2 : data3) {
            for (double[] data1 : data2) {
                for (double data0 : data1) {
                    dataVector[d++] = data0;
                }
            }
        }
        return new Matrix(dataVector, dataVector.length);
    }

    private double[][][] getTensor() {
        int length = getLength();
        List<Joint> joints = new ArrayList(((PoseJointCoordinates) getPose(0)).getJoints());
        double[][][] data = new double[length][joints.size()][Constants.axes.length];
        for (int l = 0; l < length; l++) {
            for (int j = 0; j < joints.size(); j++) {
                for (int a = 0; a < Constants.axes.length; a++) {
                    data[l][j][a] = ((PoseJointCoordinates) getPose(l)).extractJointAxisCoordinateParameter(Constants.axes[a], joints.get(j).getName()).getValue();
                }
            }
        }
        return data;
    }

    @Override
    public List extractRawFeatures() {
        List<Feature> features = new ArrayList();
        int length = getLength();
        List<Joint> joints = new ArrayList(((PoseJointCoordinates) getPose(0)).getJoints());
        for (Joint joint : joints) {
            for (Axis axe : Constants.axes) {
                Parameter[] parameters = new Parameter[length];
                for (int l = 0; l < length; l++) {
                    parameters[l] = ((PoseJointCoordinates) getPose(l)).extractJointAxisCoordinateParameter(axe, joint.getName());
                }
                features.add(new Feature(parameters));
            }
        }
        return features;
    }

    public Feature extractFeetDistanceSideSensitiveFeature() {
        Axis z = new Axis("Z");
        int length = getLength();
        Parameter[] parameters = new Parameter[length];
        for (int l = 0; l < length; l++) {
            PoseJointCoordinates pose = (PoseJointCoordinates) getPose(l);
            parameters[l] = new Parameter("FeetDistanceSideSensitive", pose.extractJointAxisCoordinateParameter(z, "lfoot").getValue() - pose.extractJointAxisCoordinateParameter(z, "rfoot").getValue());
        }
        return new Feature(parameters);
    }

    public Feature extractInterJointDistanceFeature(String jointName1, String jointName2) {
        int length = getLength();
        Parameter[] parameters = new Parameter[length];
        for (int l = 0; l < length; l++) {
            parameters[l] = ((PoseJointCoordinates) getPose(l)).extractInterJointDistanceParameter(jointName1, jointName2);
        }
        return new Feature(parameters);
    }

    public Feature extractJointAngleFeature(String jointName1, String jointName2, String jointName3) {
        int length = getLength();
        Parameter[] parameters = new Parameter[length];
        for (int l = 0; l < length; l++) {
            parameters[l] = ((PoseJointCoordinates) getPose(l)).extractJointAngleParameter(jointName1, jointName2, jointName3);
        }
        return new Feature(parameters);
    }

    public Feature extractJointAxisCoordinateFeature(Axis axis, String jointName) {
        int length = getLength();
        Parameter[] parameters = new Parameter[length];
        for (int l = 0; l < length; l++) {
            parameters[l] = ((PoseJointCoordinates) getPose(l)).extractJointAxisCoordinateParameter(axis, jointName);
        }
        return new Feature(parameters);
    }

    public Feature extractJointAxisDistanceFeature(Axis axis, String jointName1, String jointName2) {
        int length = getLength();
        Parameter[] parameters = new Parameter[length];
        for (int l = 0; l < length; l++) {
            parameters[l] = ((PoseJointCoordinates) getPose(l)).extractJointAxisDistanceParameter(axis, jointName1, jointName2);
        }
        return new Feature(parameters);
    }

    public Feature extractBoneAxisAngleFeature(Axis axis, String jointName1, String jointName2) {
        int length = getLength();
        Parameter[] parameters = new Parameter[length];
        for (int l = 0; l < length; l++) {
            parameters[l] = ((PoseJointCoordinates) getPose(l)).extractBoneAxisAngleParameter(axis, jointName1, jointName2);
        }
        return new Feature(parameters);
    }

    public Feature extractCentroidsDistanceFeature(String[] jointNames1, String[] jointNames2) {
        int length = getLength();
        Parameter[] parameters = new Parameter[length];
        for (int l = 0; l < length; l++) {
            parameters[l] = ((PoseJointCoordinates) getPose(l)).extractCentroidsDistanceParameter(jointNames1, jointNames2);
        }
        return new Feature(parameters);
    }

    public Feature extractTriangleAreaFeature(String jointName1, String jointName2, String jointName3) {
        int length = getLength();
        Parameter[] parameters = new Parameter[length];
        for (int l = 0; l < length; l++) {
            parameters[l] = ((PoseJointCoordinates) getPose(l)).extractTriangleAreaParameter(jointName1, jointName2, jointName3);
        }
        return new Feature(parameters);
    }

    public Feature extractProjectedPolygonAreaFeature(String[] jointNames, Axis projectionAxis1, Axis projectionAxis2) {
        int length = getLength();
        Parameter[] parameters = new Parameter[length];
        for (int l = 0; l < length; l++) {
            parameters[l] = ((PoseJointCoordinates) getPose(l)).extractProjectedPolygonAreaParameter(jointNames, projectionAxis1, projectionAxis2);
        }
        return new Feature(parameters);
    }

    public Feature extractZeroFeature() {
        int length = getLength();
        Parameter[] parameters = new Parameter[length];
        for (int l = 0; l < length; l++) {
            parameters[l] = new Parameter("zero", 0);
        }
        return new Feature(parameters);
    }
}
