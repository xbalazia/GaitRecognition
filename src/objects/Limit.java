package objects;

public class Limit {

    public double min;
    public double max;

    public Limit(double min, double max) {
        this.min = min;
        this.max = max;
    }
}
