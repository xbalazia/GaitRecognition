package objects;

public class PoseBoneRotations extends Pose {

    private final double[] boneRotations;

    public PoseBoneRotations(double[] boneRotations) {
        this.boneRotations = boneRotations;
    }

    public double[] getBoneRotations() {
        return boneRotations;
    }
}
