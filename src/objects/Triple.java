package objects;

import java.io.Serializable;

public class Triple implements Serializable {

    private final Axis[] axes = new Axis[]{new Axis("X"), new Axis("Y"), new Axis("Z")};
    public double x = 0;
    public double y = 0;
    public double z = 0;

    public Triple(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getMagnitude() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public Triple plus(Triple vector) {
        return new Triple(x + vector.x, y + vector.y, z + vector.z);
    }

    public Triple minus(Triple vector) {
        return plus(vector.multiply(-1));
    }

    public Triple multiply(double multiplier) {
        return new Triple(x * multiplier, y * multiplier, z * multiplier);
    }

    public Triple divide(double divisor) {
        return multiply((double) 1 / divisor);
    }

    public double dotProduct(Triple vector) {
        return x * vector.x + y * vector.y + z * vector.z;
    }

    public double getElementByAxis(Axis axis) {
        return x * axis.is(axes[0]) + y * axis.is(axes[1]) + z * axis.is(axes[2]);
    }

    @Override
    public String toString() {
        return "[" + x + "," + y + "," + z + "]";
    }
}
