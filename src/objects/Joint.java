package objects;

import java.io.Serializable;

public class Joint implements Serializable {

    private final String name;
    private final Triple coordinates;

    public Joint(String name, Triple coordinates) {
        this.name = name;
        this.coordinates = coordinates;
    }

    public String getName() {
        return name;
    }

    public Triple getCoordinates() {
        return coordinates;
    }

    public double getDistanceL2To(Joint joint) {
        return getCoordinates().minus(joint.getCoordinates()).getMagnitude();
    }
}
