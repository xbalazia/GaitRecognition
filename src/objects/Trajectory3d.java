package objects;

import java.util.ArrayList;
import java.util.List;

public class Trajectory3d {

    private final String name;
    private final List<Triple> positions = new ArrayList();

    public Trajectory3d(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Triple> getPositions() {
        return positions;
    }

    public Triple getPosition(int index) {
        return positions.get(index);
    }

    public int getLength() {
        return positions.size();
    }
}
