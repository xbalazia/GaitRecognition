package objects;

import java.io.Serializable;
import java.util.List;

public class Trajectory implements Serializable {

    private final String name;
    private final List<Triple> positions;

    public Trajectory(String name, List<Triple> positions) {
        this.name = name;
        this.positions = positions;
    }

    public String getName() {
        return name;
    }

    public Triple getPosition(int index) {
        return positions.get(index);
    }
}
