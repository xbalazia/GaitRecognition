package objects;

import java.io.Serializable;

public class BoneRotations implements Serializable {

    private final String name;
    private final Triple rotations;

    public BoneRotations(String name, Triple rotations) {
        this.name = name;
        this.rotations = rotations;
    }

    public String getName() {
        return name;
    }

    public Triple getRotations() {
        return rotations;
    }

    public double getDistanceL2(BoneRotations joint) {
        return getRotations().minus(joint.getRotations()).getMagnitude();
    }
}
