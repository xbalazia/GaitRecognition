package objects;

import Jama.Matrix;

public class Sample extends Classifiable {

    private Matrix matrix;

    public Sample(String identifier, Matrix matrix) {
        super(identifier);
        this.matrix = matrix;
    }

    public Matrix getMatrix() {
        return matrix;
    }

    public void setMatrix(Matrix matrix) {
        this.matrix = matrix;
    }

    public double[] getPose(int l) {
        return matrix.getMatrix(0, matrix.getRowDimension() - 1, l, l).getColumnPackedCopy();
    }

    public int getDimension() { // features
        return matrix.getRowDimension();
    }

    public int getLength() { // time
        return matrix.getColumnDimension();
    }

    public double getParameterValue(int d, int l) {
        return matrix.get(d, l);
    }

    public void adjust(int length) {
        Matrix newMatrix = new Matrix(getDimension(), length);
        for (int l = 0; l < length; l++) {
            double[] column = getPose(Math.round(((float) l / (length - 1)) * (getLength() - 1)));
            newMatrix.setMatrix(0, getDimension() - 1, l, l, new Matrix(column, column.length));
        }
        setMatrix(newMatrix);
    }
}