package objects;

import java.io.Serializable;

public class Classifiable implements Serializable {

    private final String identifier;
    private final String subject;
    private final String sequence;

    public Classifiable(String identifier) {
        if (identifier == null) {
            this.identifier = "";
            this.subject = "";
            this.sequence = "";
        } else {
            this.identifier = identifier;
            this.subject = identifier.split("_")[0];
            this.sequence = identifier.split("_")[1];
        }
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getSubject() {
        return subject;
    }

    public String getSequence() {
        return sequence;
    }

    public boolean hasSameIdentifierAs(Classifiable object) {
        return identifier.equals(object.getIdentifier());
    }

    public boolean hasSameSubjectAs(Classifiable object) {
        return subject.equals(object.getSubject());
    }

    public boolean hasSameSequenceAs(Classifiable object) {
        return sequence.equals(object.getSequence());
    }
}
