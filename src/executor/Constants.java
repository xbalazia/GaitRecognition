package executor;

import java.io.File;
import objects.Axis;

public class Constants {

    public static final File prototypicalFileASF = new File("skeleton.asf");
    public static final File prototypicalFileAMC = new File("gaitcycle.amc");
    public static final File customProbeFileAMC = new File("customProbe.amc");
    public static final File customGalleryDirectory = new File("customGallery/");
    public static final File customClassifier = new File("customClassifier.classifier");

    public static final Axis[] axes = new Axis[]{new Axis("X"), new Axis("Y"), new Axis("Z")};
}
