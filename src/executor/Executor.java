package executor;

import algorithms.Classifier;
import algorithms.Classifier1NNDTWKrzeszowskiT;
import algorithms.Classifier1NNDTWL1;
import algorithms.Classifier1NNDTWL2;
import algorithms.Classifier1NNNareshKumarMS;
import algorithms.Classifier1NNL1L1;
import algorithms.ClassifierRandom;
import algorithms.ClassifierTransform1NNMMCMahalanobis;
import algorithms.ClassifierTransform1NNPCALDAMahalanobis;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import objects.PoseJointCoordinates;
import objects.MotionJointCoordinates;
import objects.Feature;
import objects.Motion;
import algorithms.Method;
import algorithms.MethodAhmedM;
import algorithms.MethodAliS;
import algorithms.MethodAnderssonVO;
import algorithms.MethodBallA;
import algorithms.MethodDikovskiB;
import algorithms.MethodAhmedF;
import algorithms.MethodJiangS;
import algorithms.MethodKrzeszowskiT;
import algorithms.MethodNareshKumarMS;
import algorithms.MethodKwolekB;
import algorithms.MethodPreisJ;
import algorithms.MethodSedmidubskyJ;
import algorithms.MethodSinhaA;
import algorithms.MethodTransform;
import algorithms.MethodRandom;
import algorithms.MethodRaw;
import objects.ClassifiableList;
import objects.Parameter;
import objects.Template;

public class Executor {

    /*
     Select distance threshold
     Each value refers to a particular sub-database
     */
    private static final double distanceThreshold = 63.3; // 56.3(2,35), 59.4(4,67), 63.3(8,130), 73.7(16,302), 173.3(32,2047), 302.0(54,3843), 495.3(64,5923)

    /*
     Select actions
     */
    private static final boolean extractDatabase = false;
    private static final boolean learnClassifiers = false;
    private static final boolean performClassifications = false;
    private static final boolean evaluateMethods = true;

    /*
     Algorithms
     */
    private static final MotionLoaderBoneRotations motionLoaderBoneRotations = new MotionLoaderBoneRotations();
    private static final MotionLoaderJointCoordinates motionLoaderJointCoordinates = new MotionLoaderJointCoordinates();
    private static final Classifier1NNDTWL1 classifier1NNDTWL1 = new Classifier1NNDTWL1();
    private static final Classifier1NNDTWL2 classifier1NNDTWL2 = new Classifier1NNDTWL2();
    private static final Classifier1NNL1L1 classifier1NNL1L1 = new Classifier1NNL1L1();
    private static final Classifier1NNDTWKrzeszowskiT classifier1NNDTWKrzeszowskiT = new Classifier1NNDTWKrzeszowskiT();
    private static final Classifier1NNNareshKumarMS classifier1NNNareshKumarMS = new Classifier1NNNareshKumarMS();
    private static final ClassifierTransform1NNMMCMahalanobis classifierTransform1NNMMCMahalanobis = new ClassifierTransform1NNMMCMahalanobis();
    private static final ClassifierTransform1NNPCALDAMahalanobis classifierTransform1NNPCALDAMahalanobis = new ClassifierTransform1NNPCALDAMahalanobis();
    private static final ClassifierRandom classifierRandom = new ClassifierRandom();

    /*
     All implemented MoCap-based gait recognition methods
     */
    private static Method[] methods = new Method[]{
        new MethodAhmedF(classifier1NNDTWL1),//0
        new MethodAhmedM(classifier1NNL1L1),//1---
        new MethodAliS(classifier1NNL1L1),//2---
        new MethodAnderssonVO(classifier1NNL1L1),//3---
        new MethodBallA(classifier1NNL1L1),//4---
        new MethodDikovskiB(classifier1NNL1L1),//5---
        new MethodJiangS(classifier1NNDTWL1),//6
        new MethodKrzeszowskiT(classifier1NNDTWKrzeszowskiT),//7
        new MethodKwolekB(classifier1NNL1L1),//8---
        new MethodNareshKumarMS(classifier1NNNareshKumarMS),//9
        new MethodPreisJ(classifier1NNL1L1),//10---
        new MethodSedmidubskyJ(classifier1NNDTWL1),//11
        new MethodSinhaA(classifier1NNL1L1),//12---
        new MethodTransform(motionLoaderBoneRotations, classifierTransform1NNMMCMahalanobis),//13
        new MethodTransform(motionLoaderJointCoordinates, classifierTransform1NNMMCMahalanobis),//14---
        new MethodTransform(motionLoaderBoneRotations, classifierTransform1NNPCALDAMahalanobis),//15
        new MethodTransform(motionLoaderJointCoordinates, classifierTransform1NNPCALDAMahalanobis),//16---
        new MethodRandom(classifierRandom),//17
        new MethodRaw(motionLoaderBoneRotations, classifier1NNDTWL2),//18
        new MethodRaw(motionLoaderJointCoordinates, classifier1NNDTWL2),//19
    };

    /*
     Main method
     Executes the actions selected above
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        if (extractDatabase) {
            extractDatabase();
        }
        if (learnClassifiers) {
            learnClassifiers();
        }
        if (performClassifications) {
            performClassifications();
        }
        if (evaluateMethods) {
            evaluateMethods(args);
        }
    }

    /*
     Normalize the whole original CMU database in amcOriginal/ directory with respect to person’s position and walk direction and saves it to amc/ directory
     Select gait cycles as sub-motions with similarity under selected distanceThreshold to prototypicalFileAMC and save them to separate AMC files {subject}_{id}[{from}-{to}].amc in amc{distanceThreshold}/ directory
     Only subjects of at least 10 gait cycles are kept.
     */
    private static void extractDatabase() throws IOException {
        new File("amc/").mkdirs();
        for (File fileAMC : new File("amcOriginal/").listFiles()) {
            BufferedReader brAMC = new BufferedReader(new FileReader(fileAMC));
            BufferedWriter wrAMC = new BufferedWriter(new FileWriter("amc/" + fileAMC.getName()));
            String lineAMC = brAMC.readLine();
            while (lineAMC != null) {
                if (lineAMC.split(" ")[0].equals("root")) {
                    wrAMC.write("root 0 0 0 0 0 0\r\n");
                } else {
                    wrAMC.write(lineAMC + "\r\n");
                }
                lineAMC = brAMC.readLine();
            }
            wrAMC.flush();
            wrAMC.close();
        }
        MotionJointCoordinates motionProto = new MotionLoaderJointCoordinates().loadMotion(Constants.prototypicalFileAMC);
        Feature featureProto = motionProto.extractFeetDistanceSideSensitiveFeature();
        int lengthProto = featureProto.getLength();
        double sigma = 2;
        int minLength = (int) Math.round(lengthProto / sigma);
        int maxLength = (int) Math.round(lengthProto * sigma);
        List<Motion> gaitCycles = new ArrayList();
        for (File fileAMC : new File("amc/").listFiles()) {
            System.out.print("Processing " + fileAMC.getName() + "\r\n");
            MotionJointCoordinates motion = new MotionLoaderJointCoordinates().loadMotion(fileAMC);
            String identifier = motion.getIdentifier();
            Feature feature = motion.extractFeetDistanceSideSensitiveFeature();
            int length = feature.getLength();
            boolean selected = true;
            while (selected) {
                selected = false;
                double minDistance = Double.MAX_VALUE;
                int minFrom = 0;
                int minTo = 0;
                for (int from = 0; from < length; from++) {
                    for (int to = from + minLength; to < Math.min(from + maxLength, length); to++) {
                        Feature subFeature = feature.getSubFeature(from, to);
                        subFeature.adjust(lengthProto);
                        double[] subFeatureParameterValues = subFeature.getParameterValues();
                        double[] featureProtoParameterValues = featureProto.getParameterValues();
                        double distance = 0;
                        for (int l = 0; l < lengthProto; l++) {
                            distance += Math.abs(subFeatureParameterValues[l] - featureProtoParameterValues[l]);
                        }
                        if (minDistance > distance) {
                            minDistance = distance;
                            minFrom = from;
                            minTo = to;
                        }
                    }
                }
                if (minDistance < distanceThreshold) {
                    List<PoseJointCoordinates> subPoses = new ArrayList();
                    for (int l = minFrom; l < minTo && l < motion.getLength(); l++) {
                        subPoses.add((PoseJointCoordinates) motion.getPose(l));
                    }
                    gaitCycles.add(new MotionJointCoordinates(identifier + "_" + (minFrom + 1) + "-" + (minTo + 1), subPoses));
                    selected = true;
                    for (int l = minFrom; l < minTo; l++) {
                        feature.setParameter(l, new Parameter("Erased", new Random().nextDouble() * Double.MAX_VALUE));
                    }
                }
            }
        }
        List<List<Motion>> gaitCyclesBySubject = new ClassifiableList(gaitCycles).splitBySubject();
        new File("amc" + distanceThreshold + "/").mkdirs();
        for (List<Motion> gaitCyclesOfSubject : gaitCyclesBySubject) {
            if (gaitCyclesOfSubject.size() >= 10) {
                for (Motion gaitCycle : gaitCyclesOfSubject) {
                    String identifier = gaitCycle.getIdentifier();
                    BufferedReader brAMC = new BufferedReader(new FileReader("amc/" + identifier + ".amc"));
                    BufferedWriter wrAMC = new BufferedWriter(new FileWriter("amc" + distanceThreshold + "/" + identifier + ".amc"));
                    String subSequence = identifier.split("_")[2];
                    String from = subSequence.split("-")[0];
                    String to = subSequence.split("-")[1];
                    String lineAMC = brAMC.readLine();
                    while (lineAMC != null && !lineAMC.equals("1")) {
                        wrAMC.write(lineAMC + "\r\n");
                        lineAMC = brAMC.readLine();
                    }
                    while (lineAMC != null && !lineAMC.equals(String.valueOf(from))) {
                        lineAMC = brAMC.readLine();
                    }
                    int f = 0;
                    while (lineAMC != null && !lineAMC.equals(to)) {
                        if (lineAMC.split(" ").length == 1) {
                            wrAMC.write(++f + "\r\n");
                        } else {
                            wrAMC.write(lineAMC + "\r\n");
                        }
                        lineAMC = brAMC.readLine();
                    }
                    wrAMC.flush();
                    wrAMC.close();
                    brAMC.close();
                }
            }
        }
    }

    /*
     Extracts classifiers for all methods learned on the sub-database determined by distanceThreshold
     Classifiers are saved as separate files {methodName}-{distanceThreshold}.classifier in classifiers/ directory
     */
    private static void learnClassifiers() throws IOException {
        new File("classifiers/").mkdirs();
        for (Method method : methods) {
            method.learnClassifier(method.loadMotions(new File("amc" + distanceThreshold + "/")));
            method.saveClassifier(new File("classifiers/" + method.getName() + "-" + distanceThreshold + ".classifier"));
        }
    }

    /*
     Performs classifications of customProbeFileAMC over customGalleryDirectory for all methods
     Instead of learning classifiers methods load their extracted classifiers {distanceThreshold}-{methodName}.classifier in classifiers/ directory that are learned on the sub-database determined by distanceThreshold
     */
    private static void performClassifications() throws IOException, ClassNotFoundException {
        ObjectInputStream oos = new ObjectInputStream(new FileInputStream(Constants.customClassifier));
        Classifier classifier = (Classifier) oos.readObject();
        oos.close();
        for (Method method : methods) {
            classifier.getDecision().importGallery(method.extractTemplates(method.loadMotions(Constants.customGalleryDirectory)));
            Template templateQuery = method.extractTemplate(method.loadMotion(Constants.customProbeFileAMC));
            System.out.print(method.getName() + ": query subject classified as " + classifier.classify(templateQuery) + "\r\n");
        }
    }

    /*
     Performs evaluations for all methods
     */
    private static void evaluateMethods(String[] args) throws IOException {
        int nClasses; // number of classes
        int nClassesLearning; // number of learning classes
        int nClassesEvaluation; // number of evaluation classes
        int nFoldsLE; // number of learning-evaluation folds
        int nFoldsPG; // number of probe-gallery folds 
       int fineness; // number of thresholds for FAR/FRR TAR/FAR RCL/PCN
        long beginning = System.currentTimeMillis();

        // DEFAULT
        nClasses = 8;//Integer.parseInt(args[0]);
        nClassesLearning = 3;
        nClassesEvaluation = 5;
        nFoldsLE = 3;
        nFoldsPG = 10;
        fineness = 30;
        methods = new Method[]{methods[2], methods[3], methods[13], methods[17]};
        Evaluator evaluator = new Evaluator(methods, new File("amc" + distanceThreshold), nFoldsPG, fineness);
        //evaluator.setSubjectsClosedSetFixed(new String[]{"15", "35", "36", "91"});
        evaluator.setSubjectsClosedSetRandom(nClasses);
        evaluator.evaluateClosedSet(nFoldsLE);
        //evaluator.setSubjectsOpenSetFixed(new String[]{"15", "35"}, new String[]{"36", "91"});
        evaluator.setSubjectsOpenSetRandom(nClassesLearning, nClassesEvaluation);
        evaluator.evaluateOpenSet();
//        
//        // IJCB Figure 3
//        nClasses = 64;
//        nFoldsPG = 10;
//        fineness = 30;
//        methods = new Method[]{methods[1], methods[2], methods[3], methods[4], methods[5], methods[8], methods[10], methods[12], methods[14], methods[16]};
//        Evaluator evaluator = new Evaluator(methods, new File("amc" + distanceThreshold), nFoldsPG, fineness);
//        for (int i = 2; i <= nClasses - 2; i++) {
//            System.out.println("i=" + i);
//            nClassesLearning = i;
//            nClassesEvaluation = nClasses - i;
//            String[] fixed = new String[]{"54", "70", "06", "19", "132", "104", "91", "38", "40", "143", "142", "94", "90", "77", "78", "03", "74", "82", "127", "14", "41", "16", "01", "56", "85", "15", "136", "35", "60", "89", "55", "137", "108", "88", "114", "17", "39", "05", "120", "07", "138", "69", "86", "12", "13", "93", "49", "135", "61", "126", "107", "22", "81", "83", "106", "36", "111", "09", "63", "131", "08", "113", "18", "133"};
//            evaluator.setSubjectsOpenSetFixed(Arrays.copyOfRange(fixed, 0, nClassesLearning), Arrays.copyOfRange(fixed, nClassesLearning, nClassesLearning + nClassesEvaluation));
//            //methodEvaluator.setSubjectsOpenSetRandom(nClassesLearning, nClassesEvaluation);
//            evaluator.evaluateOpenSet();
//        }
//        
//        // ACM-TOMM Table 1
//        nClasses = 64;
//        nFoldsLE = 3;
//        nFoldsPG = 10;
//        fineness = 30;
//        Evaluator evaluator = new Evaluator(methods, new File("amc" + distanceThreshold), nFoldsPG, fineness);
//        evaluator.setSubjectsClosedSetRandom(nClasses);
//        evaluator.evaluateClosedSet(nFoldsLE);
//
//        // S+SSPR Figure 3 and ACM-TOMM Figure 3
//        nClasses = 64;
//        nFoldsLE = 3;
//        nFoldsPG = 10;
//        fineness = 30;
//        methods = new Method[]{methods[14]};
//        Evaluator evaluator = new Evaluator(methods, new File("amc" + distanceThreshold), nFoldsPG, fineness);
//        for (int i = 2; i <= nClasses / 2; i++) {
//            System.out.println("i=" + i);
//            nClasses = i;
//            nClassesLearning = i;
//            nClassesEvaluation = i;
//            evaluator.setSubjectsClosedSetRandom(nClasses);
//            evaluator.evaluateClosedSet(nFoldsLE);
//            evaluator.setSubjectsOpenSetRandom(nClassesLearning, nClassesEvaluation);
//            evaluator.evaluateOpenSet();
//        }
//        
//        // S+SSPR Figure 4a
//        nClasses = 64;
//        nFoldsPG = 10;
//        fineness = 30;
//        methods = new Method[]{methods[14]};
//        Evaluator evaluator = new Evaluator(methods, new File("amc" + distanceThreshold), nFoldsPG, fineness);
//        for (int i = 2; i <= nClasses / 2; i++) {
//            System.out.println("i=" + i);
//            nClassesLearning = i;
//            nClassesEvaluation = nClasses / 2;
//            evaluator.setSubjectsOpenSetRandom(nClassesLearning, nClassesEvaluation);
//            evaluator.evaluateOpenSet();
//        }
//        
//        // S+SSPR Figure 4b
//        nClasses = 64;
//        nFoldsLE = 3;
//        nFoldsPG = 10;
//        fineness = 30;
//        methods = new Method[]{methods[14]};
//        Evaluator evaluator = new Evaluator(methods, new File("amc" + distanceThreshold), nFoldsPG, fineness);
//        for (int i = 2; i <= nClasses - 2; i++) {
//            System.out.println("i=" + i);
//            nClassesLearning = i;
//            nClassesEvaluation = nClasses - i;
//            evaluator.setSubjectsOpenSetRandom(nClassesLearning, nClassesEvaluation);
//            evaluator.evaluateOpenSet();
//        }
//
        System.out.println("time=" + printTime(System.currentTimeMillis() - beginning));
    }

    private static String printTime(double time) {
        String unit = "ms";
        if (time >= 1000) {
            time /= 1000;
            unit = "s";
            if (time >= 60) {
                time /= 60;
                unit = "m";
                if (time >= 60) {
                    time /= 60;
                    unit = "h";
                    if (time >= 24) {
                        time /= 24;
                        unit = "d";
                    }
                }
            }
        }
        return String.valueOf((double) Math.round(time * 10) / 10) + unit;
    }
}
