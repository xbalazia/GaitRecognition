package executor;

import java.io.File;
import java.io.IOException;
import objects.Motion;

public abstract class MotionLoader {
    
    public MotionLoader() {
    }
    
    public abstract String getDescription();
    
    public abstract Motion loadMotion(File fileAMC) throws IOException;
}
