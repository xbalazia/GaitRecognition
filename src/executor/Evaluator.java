package executor;

import algorithms.ClustererAgglomerativeHierarchical;
import algorithms.ClustererKmeans;
import algorithms.Decision;
import algorithms.QueryRange;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import algorithms.Method;
import java.util.Map;
import objects.Classifiable;
import objects.ClassifiableList;
import objects.Motion;
import objects.Template;

public class Evaluator {

    private final Method[] methods;
    private final List<Classifiable> classifiables;
    private final File directoryAMC;
    private final int fineness;
    private final int nFoldsPG;

    private final QueryRange queryRange = new QueryRange();
    private String subjectPick;

    private List<String> subjectsLearning, subjectsEvaluation;

    private List<List<Double>> scoresCCR, scoresDCT, scoresTD, scoresDBI, scoresDI, scoresSC, scoresFDR, scoresEER, scoresAUC, scoresMAP, scoresPAHC, scoresPKmeans, scoresRIAHC, scoresRIKmeans, scoresFAHC, scoresFKmeans, scoresJIAHC, scoresJIKmeans, scoresFMIAHC, scoresFMIKmeans;
    private List<List<List<Double>>> scoresListCMC, scoresListFARFRR0, scoresListFARFRR1, scoresListTARFAR0, scoresListTARFAR1, scoresListRCLPCN0, scoresListRCLPCN1;

    public Evaluator(Method[] methods, File directoryAMC, int nFoldsPG, int fineness) {
        this.methods = methods;
        this.directoryAMC = directoryAMC;
        classifiables = new ArrayList();
        for (File fileAMC : directoryAMC.listFiles()) {
            classifiables.add(new Classifiable(getIdentifier(fileAMC)));
        }
        this.nFoldsPG = nFoldsPG;
        this.fineness = fineness;
    }

    private String getIdentifier(File file) {
        return file.getName().split("\\.")[0];
    }

    private String getSubject(File file) {
        return file.getName().split("_")[0];
    }

    private int getNumberOfMethods() {
        return methods.length;
    }

    private Method getMethod(int m) {
        return methods[m];
    }

    private List<Motion> loadMotions(Method method, List<String> subjects) throws IOException {
        List<Motion> motions = new ArrayList();
        for (File fileAMC : directoryAMC.listFiles()) {
            if (subjects.contains(getSubject(fileAMC))) {
                motions.add(method.loadMotion(fileAMC));
            }
        }
        return motions;
    }

    public void setSubjectsClosedSetFixed(String[] subjectsLearningAndEvaluation) {
        subjectPick = "fixed";
        this.subjectsEvaluation = Arrays.asList(subjectsLearningAndEvaluation);
    }

    public void setSubjectsClosedSetRandom(int nClasses) {
        subjectPick = "random";
        List<List<Classifiable>> classifiablesBySubject = new ClassifiableList(classifiables).splitBySubject();
        int numberOfSubjects = classifiablesBySubject.size();
        if (nClasses < 2) {
            System.out.println("nClasses=" + nClasses + " < 2");
            return;
        }
        if (nClasses > numberOfSubjects) {
            System.out.println("nClasses=" + nClasses + " > " + "numberOfSubjects=" + numberOfSubjects);
            return;
        }
        Random random = new Random();
        subjectsEvaluation = new ArrayList();
        while (subjectsEvaluation.size() < nClasses) {
            String subject = classifiablesBySubject.get(random.nextInt(numberOfSubjects)).get(0).getSubject();
            if (!subjectsEvaluation.contains(subject)) {
                subjectsEvaluation.add(subject);
            }
        }
    }

    private void initialize() {
        scoresDCT = new ArrayList();
        scoresTD = new ArrayList();
        scoresCCR = new ArrayList();
        scoresListCMC = new ArrayList();
        scoresDBI = new ArrayList();
        scoresDI = new ArrayList();
        scoresSC = new ArrayList();
        scoresFDR = new ArrayList();
        scoresEER = new ArrayList();
        scoresAUC = new ArrayList();
        scoresMAP = new ArrayList();
        scoresListFARFRR0 = new ArrayList();
        scoresListFARFRR1 = new ArrayList();
        scoresListTARFAR0 = new ArrayList();
        scoresListTARFAR1 = new ArrayList();
        scoresListRCLPCN0 = new ArrayList();
        scoresListRCLPCN1 = new ArrayList();
        scoresPAHC = new ArrayList();
        scoresPKmeans = new ArrayList();
        scoresRIAHC = new ArrayList();
        scoresRIKmeans = new ArrayList();
        scoresFAHC = new ArrayList();
        scoresFKmeans = new ArrayList();
        scoresJIAHC = new ArrayList();
        scoresJIKmeans = new ArrayList();
        scoresFMIAHC = new ArrayList();
        scoresFMIKmeans = new ArrayList();
        for (int m = 0; m < getNumberOfMethods(); m++) {
            scoresDCT.add(new ArrayList());
            scoresTD.add(new ArrayList());
            scoresCCR.add(new ArrayList());
            scoresListCMC.add(new ArrayList());
            scoresDBI.add(new ArrayList());
            scoresDI.add(new ArrayList());
            scoresSC.add(new ArrayList());
            scoresFDR.add(new ArrayList());
            scoresEER.add(new ArrayList());
            scoresAUC.add(new ArrayList());
            scoresMAP.add(new ArrayList());
            scoresListFARFRR0.add(new ArrayList());
            scoresListFARFRR1.add(new ArrayList());
            scoresListTARFAR0.add(new ArrayList());
            scoresListTARFAR1.add(new ArrayList());
            scoresListRCLPCN0.add(new ArrayList());
            scoresListRCLPCN1.add(new ArrayList());
            scoresPAHC.add(new ArrayList());
            scoresPKmeans.add(new ArrayList());
            scoresRIAHC.add(new ArrayList());
            scoresRIKmeans.add(new ArrayList());
            scoresFAHC.add(new ArrayList());
            scoresFKmeans.add(new ArrayList());
            scoresJIAHC.add(new ArrayList());
            scoresJIKmeans.add(new ArrayList());
            scoresFMIAHC.add(new ArrayList());
            scoresFMIKmeans.add(new ArrayList());
            for (int f = 0; f <= fineness; f++) {
                scoresListFARFRR0.get(m).add(new ArrayList());
                scoresListFARFRR1.get(m).add(new ArrayList());
                scoresListTARFAR0.get(m).add(new ArrayList());
                scoresListTARFAR1.get(m).add(new ArrayList());
                scoresListRCLPCN0.get(m).add(new ArrayList());
                scoresListRCLPCN1.get(m).add(new ArrayList());
            }
            int rankTotal = subjectsEvaluation.size();
            for (int rank = 0; rank < rankTotal; rank++) {
                scoresListCMC.get(m).add(new ArrayList());
            }
        }
    }

    private void evaluate(List<Template> templatesEvaluation, int m, int f) {
        Method method = getMethod(m);
        
        System.out.print("fold=" + (f + 1) + ",");//
        System.out.print(method.getName() + ",");//
        System.out.print(method.getDescription() + ",");//
        
        List<List<Template>> foldsPG = new ArrayList();
        for (int fFoldPG = 0; fFoldPG < nFoldsPG; fFoldPG++) {
            foldsPG.add(new ArrayList());
        }
        List<List<Template>> templatesEvaluationBySequence = new ClassifiableList(templatesEvaluation).splitBySequence();
        for (int i = 0; i < templatesEvaluationBySequence.size(); i++) {
            foldsPG.get(i % nFoldsPG).addAll(templatesEvaluationBySequence.get(i));
        }
        List<Double> scoresCCRm = new ArrayList();
        List<List<Double>> scoresListCMCm = new ArrayList();
        int rankTotal = subjectsEvaluation.size();
        for (int rank = 0; rank < rankTotal; rank++) {
            scoresListCMCm.add(new ArrayList());
        }
        for (int fFoldPG = 0; fFoldPG < nFoldsPG; fFoldPG++) {
            List<Template> templatesProbe = foldsPG.get(fFoldPG);
            List<Template> templatesGallery = new ArrayList();
            for (int gFoldPG = 0; gFoldPG < nFoldsPG; gFoldPG++) {
                if (gFoldPG != fFoldPG) {
                    templatesGallery.addAll(foldsPG.get(gFoldPG));
                }
            }
            double[] cumulativeMatchCharacteristic = getCumulativeMatchCharacteristic(method, templatesProbe, templatesGallery);
            for (int rank = 0; rank < rankTotal; rank++) {
                scoresListCMCm.get(rank).add(cumulativeMatchCharacteristic[rank]);
            }
            scoresCCRm.add(cumulativeMatchCharacteristic[0]);
        }
        for (int rank = 0; rank < rankTotal; rank++) {
            scoresListCMC.get(m).get(rank).add(getMean(scoresListCMCm.get(rank)));
        }

        double ccr = getMean(scoresCCRm);//
        scoresCCR.get(m).add(ccr);//
        System.out.print(scoresCCRm.toString().replaceAll(",", ";") + "->" + ccr + ",");//

        Decision decision = method.getClassifier().getDecision();
        if (decision.getDistanceTemplates() != null) {

            long beginning = System.currentTimeMillis();
            decision.calculateDistanceMap(templatesEvaluation);
            Map distanceMap = decision.getDistanceMap();
            long end = System.currentTimeMillis();

            double dct = (double) (end - beginning) / decision.getDistanceMap().size();//
            scoresDCT.get(m).add(dct);//
            System.out.print(dct + ",");//

            double td = getTemplateDimensionality(templatesEvaluation);//
            scoresTD.get(m).add(td);//
            System.out.print(td + ",");//

            // INTERNAL EVALUATION
            queryRange.setDistanceMap(distanceMap);

            double dbi = getDaviesBouldinIndex(method, templatesEvaluation);//
            scoresDBI.get(m).add(dbi);//
            System.out.print(dbi + ",");//

            double di = getDunnIndex(method, templatesEvaluation);//
            scoresDI.get(m).add(di);//
            System.out.print(di + ",");//

            double sc = getSilhouetteCoefficient(method, templatesEvaluation);//
            scoresSC.get(m).add(sc);//
            System.out.print(sc + ",");//

            double fdr = getFishersDiscriminantRatio(method, templatesEvaluation);//
            scoresFDR.get(m).add(fdr);//
            System.out.print(fdr + ",");//

            double highRadiusFARFRR = 1;
            while (getFARFRR(highRadiusFARFRR, templatesEvaluation)[1] > 0) {
                highRadiusFARFRR *= 2;
            }
            double radiusEER = findRadiusEER(0, highRadiusFARFRR, templatesEvaluation);

            double eer = getFARFRR(radiusEER, templatesEvaluation)[0];//
            scoresEER.get(m).add(eer);//
            System.out.print(eer + ",");//

            for (int i = 0; i <= fineness; i++) {
                double radiusFARFRR = i * 2 * radiusEER / fineness;
                double[] ratesFARFRR = getFARFRR(radiusFARFRR, templatesEvaluation);
                scoresListFARFRR0.get(m).get(i).add(ratesFARFRR[0]);
                scoresListFARFRR1.get(m).get(i).add(ratesFARFRR[1]);
            }
            double highRadiusTARFAR = 1;
            while (getTARFAR(highRadiusTARFAR, templatesEvaluation)[0] < 1 || getTARFAR(highRadiusTARFAR, templatesEvaluation)[1] < 1) {
                highRadiusTARFAR *= 2;
            }
            double maxRadiusTARFAR = findMaxRadiusGARIAR(0, highRadiusTARFAR, templatesEvaluation);
            List<double[]> curveTARFAR = new ArrayList();
            List<double[]> gapsFAR = new ArrayList();
            double[] minRatesTARFAR = getTARFAR(0, templatesEvaluation);
            double[] maxRatesTARFAR = getTARFAR(maxRadiusTARFAR, templatesEvaluation);
            curveTARFAR.add(minRatesTARFAR);
            curveTARFAR.add(maxRatesTARFAR);
            gapsFAR.add(new double[]{minRatesTARFAR[1], maxRatesTARFAR[1], 0, maxRadiusTARFAR});
            for (int i = 1; i < fineness; i++) {
                double maxGapSize = 0;
                double radiusLow = 0;
                double radiusHigh = 0;
                double radiusTARFAR = 0;
                int maxGapIndex = 0;
                for (int gapIndex = 0; gapIndex < gapsFAR.size(); gapIndex++) {
                    double[] gap = gapsFAR.get(gapIndex);
                    double gapSize = gap[1] - gap[0];
                    if (maxGapSize < gapSize) {
                        maxGapSize = gapSize;
                        radiusLow = gap[2];
                        radiusHigh = gap[3];
                        radiusTARFAR = (radiusLow + radiusHigh) / 2;
                        maxGapIndex = gapIndex;
                    }
                }
                double[] maxGap = gapsFAR.get(maxGapIndex);
                double[] ratesTARFAR = getTARFAR(radiusTARFAR, templatesEvaluation);
                gapsFAR.remove(maxGap);
                gapsFAR.add(new double[]{maxGap[0], ratesTARFAR[1], radiusLow, radiusTARFAR});
                gapsFAR.add(new double[]{ratesTARFAR[1], maxGap[1], radiusTARFAR, radiusHigh});
                curveTARFAR.add(ratesTARFAR);
            }
            curveTARFAR = sortCurve(curveTARFAR, 1);
            for (int i = 0; i <= fineness; i++) {
                scoresListTARFAR0.get(m).get(i).add(curveTARFAR.get(i)[0]);
                scoresListTARFAR1.get(m).get(i).add(curveTARFAR.get(i)[1]);
            }

            double auc = getAreaUnderCurve(curveTARFAR, 1);//
            scoresAUC.get(m).add(auc);//
            System.out.print(auc + ",");//

            double highRadiusRCLPCN = 1;
            while (getRCLPCN(highRadiusRCLPCN, templatesEvaluation)[0] < 1) {
                highRadiusRCLPCN *= 2;
            }
            double maxRadiusRCLPCN = findMaxRadiusRCLPCN(0, highRadiusRCLPCN, templatesEvaluation);
            List<double[]> curveRCLPCN = new ArrayList();
            List<double[]> gapsRCL = new ArrayList();
            double[] minRatesRCLPCN = getRCLPCN(0, templatesEvaluation);
            double[] maxRatesRCLPCN = getRCLPCN(maxRadiusRCLPCN, templatesEvaluation);
            curveRCLPCN.add(minRatesRCLPCN);
            curveRCLPCN.add(maxRatesRCLPCN);
            gapsRCL.add(new double[]{minRatesRCLPCN[0], maxRatesRCLPCN[0], 0, maxRadiusRCLPCN});
            for (int i = 1; i < fineness; i++) {
                double maxGapSize = 0;
                double radiusLow = 0;
                double radiusHigh = 0;
                double radiusRCLPCN = 0;
                int maxGapIndex = 0;
                for (int gapIndex = 0; gapIndex < gapsRCL.size(); gapIndex++) {
                    double[] gap = gapsRCL.get(gapIndex);
                    double gapSize = gap[1] - gap[0];
                    if (maxGapSize < gapSize) {
                        maxGapSize = gapSize;
                        radiusLow = gap[2];
                        radiusHigh = gap[3];
                        radiusRCLPCN = (radiusLow + radiusHigh) / 2;
                        maxGapIndex = gapIndex;
                    }
                }
                double[] maxGap = gapsRCL.get(maxGapIndex);
                double[] ratesRCLPCN = getRCLPCN(radiusRCLPCN, templatesEvaluation);
                gapsRCL.remove(maxGap);
                gapsRCL.add(new double[]{maxGap[0], ratesRCLPCN[0], radiusLow, radiusRCLPCN});
                gapsRCL.add(new double[]{ratesRCLPCN[0], maxGap[1], radiusRCLPCN, radiusHigh});
                curveRCLPCN.add(ratesRCLPCN);
            }
            curveRCLPCN = sortCurve(curveRCLPCN, 0);
            for (int i = 0; i <= fineness; i++) {
                scoresListRCLPCN0.get(m).get(i).add(curveRCLPCN.get(i)[0]);
                scoresListRCLPCN1.get(m).get(i).add(curveRCLPCN.get(i)[1]);
            }

            double map = getAreaUnderCurve(curveRCLPCN, 0);//
            scoresMAP.get(m).add(map);//
            System.out.print(map + ",");//

            // EXTERNAL EVALUATION
            ClustererKmeans kmeans = new ClustererKmeans(subjectsEvaluation.size());
            kmeans.setDistanceMap(distanceMap);
            List<List<Template>> clustersKmeans = kmeans.cluster(templatesEvaluation);
            int[] contingencyKmeans = getContingency(clustersKmeans);
            int tpKmeans = contingencyKmeans[0];
            int fpKmeans = contingencyKmeans[1];
            int fnKmeans = contingencyKmeans[2];
            int tnKmeans = contingencyKmeans[3];

            ClustererAgglomerativeHierarchical ahc = new ClustererAgglomerativeHierarchical(subjectsEvaluation.size());
            ahc.setDistanceMap(distanceMap);
            List<List<Template>> clustersAHC = ahc.cluster(templatesEvaluation);
            int[] contingencyAHC = getContingency(clustersAHC);
            int tpAHC = contingencyAHC[0];
            int fpAHC = contingencyAHC[1];
            int fnAHC = contingencyAHC[2];
            int tnAHC = contingencyAHC[3];

            double pKmeans = getPurity(clustersKmeans);//
            scoresPKmeans.get(m).add(pKmeans);//
            System.out.print(pKmeans + ",");//

            double pAHC = getPurity(clustersAHC);//
            scoresPAHC.get(m).add(pAHC);//
            System.out.print(pAHC + ",");//

            double riKmeans = getRandIndex(tpKmeans, fpKmeans, fnKmeans, tnKmeans);//
            scoresRIKmeans.get(m).add(riKmeans);//
            System.out.print(riKmeans + ",");//

            double riAHC = getRandIndex(tpAHC, fpAHC, fnAHC, tnAHC);//
            scoresRIAHC.get(m).add(riAHC);//
            System.out.print(riAHC + ",");//

            double fKmeans = getFmeasure(tpKmeans, fpKmeans, fnKmeans, tnKmeans);//
            scoresFKmeans.get(m).add(fKmeans);//
            System.out.print(fKmeans + ",");//

            double fAHC = getFmeasure(tpAHC, fpAHC, fnAHC, tnAHC);//
            scoresFAHC.get(m).add(fAHC);//
            System.out.print(fAHC + ",");//

            double jiKmeans = getJaccardIndex(tpKmeans, fpKmeans, fnKmeans, tnKmeans);//
            scoresJIKmeans.get(m).add(jiKmeans);//
            System.out.print(jiKmeans + ",");//

            double jiAHC = getJaccardIndex(tpAHC, fpAHC, fnAHC, tnAHC);//
            scoresJIAHC.get(m).add(jiAHC);//
            System.out.print(jiAHC + ",");//

            double fmiKmeans = getFowlkesMallowsIndex(tpKmeans, fpKmeans, fnKmeans, tnKmeans);//
            scoresFMIKmeans.get(m).add(fmiKmeans);//
            System.out.print(fmiKmeans + ",");//

            double fmiAHC = getFowlkesMallowsIndex(tpAHC, fpAHC, fnAHC, tnAHC);//
            scoresFMIAHC.get(m).add(fmiAHC);//
            System.out.print(fmiAHC + ",");//
        }

        System.out.print("\r\n");//
    }

    // CLOSED SET
    public void evaluateClosedSet(int nFoldsLE) throws IOException {
        initialize();
        System.out.print("--- CLOSED SET EVALUATION BEGIN ---\r\n");//
        System.out.print("configuration=[" + subjectsEvaluation.size() + "]; subjectsLearningEvaluation=" + subjectsEvaluation.toString().replaceAll(",", ";") + "; subjectPick=" + subjectPick + "; dataSeparationLearningEvaluation=" + nFoldsLE + "-fold; dataSeparationProbeGallery=" + nFoldsPG + "-fold\r\n");//
        printHeader();
        for (int m = 0; m < getNumberOfMethods(); m++) {
            Method method = getMethod(m);
            List<List<Motion>> motionsBySequence = new ClassifiableList(loadMotions(method, subjectsEvaluation)).splitBySequence();
            List<List<Motion>> foldsLE = new ArrayList();
            for (int fFoldLE = 0; fFoldLE < nFoldsLE; fFoldLE++) {
                foldsLE.add(new ArrayList());
            }
            for (int i = 0; i < motionsBySequence.size(); i++) {
                foldsLE.get(i % nFoldsLE).addAll(motionsBySequence.get(i));
            }
            for (int fFoldLE = 0; fFoldLE < nFoldsLE; fFoldLE++) {
                method.learnClassifier(foldsLE.get(fFoldLE));
                List<Template> templatesEvaluation = new ArrayList();
                for (int f = 0; f < nFoldsLE; f++) {
                    if (f != fFoldLE) {
                        for (Motion motionEvaluation : foldsLE.get(f)) {
                            templatesEvaluation.add(method.extractTemplate(motionEvaluation));
                        }
                    }
                }
                evaluate(templatesEvaluation, m, fFoldLE);
            }
        }
        print();
        System.out.print("--- CLOSED SET EVALUATION END ---\r\n");//
    }

    private double[] getCumulativeMatchCharacteristic(Method method, List<Template> templatesProbe, List<Template> templatesGallery) {
        Decision decision = method.getClassifier().getDecision();
        List<List<Template>> templatesGalleryBySubject = new ClassifiableList(templatesGallery).splitBySubject();
        int nSubjects = templatesGalleryBySubject.size();
        int nTemplatesProbe = templatesProbe.size();
        double[] cmc = new double[nSubjects];
        for (Template templateProbe : templatesProbe) {
            List<Template> templatesGalleryRank = new ArrayList();
            templatesGalleryRank.addAll(templatesGallery);
            String[] ranking = new String[nSubjects];
            for (int rank = 0; rank < nSubjects; rank++) {
                decision.importGallery(templatesGalleryRank);
                String subjectClassified = decision.decide(templateProbe);
                ranking[rank] = subjectClassified;
                int i = 0;
                while (!templatesGalleryBySubject.get(i).get(0).getSubject().equals(subjectClassified)) {
                    i++;
                }
                templatesGalleryRank.removeAll(templatesGalleryBySubject.get(i));
            }
            String subject = templateProbe.getSubject();
            for (int rank = 0; rank < nSubjects; rank++) {
                boolean isIn = false;
                for (int r = 0; r <= rank; r++) {
                    if (ranking[r].equals(subject)) {
                        isIn = true;
                    }
                }
                if (isIn) {
                    cmc[rank] += (double) 1 / nTemplatesProbe;
                }
            }
        }
        return cmc;
    }

    // OPEN SET
    public void evaluateOpenSet() throws IOException {
        initialize();
        System.out.print("--- OPEN SET EVALUATION BEGIN ---\r\n");//
        System.out.print("configuration=[" + subjectsLearning.size() + ";" + subjectsEvaluation.size() + "]; subjectsLearning=" + subjectsLearning.toString().replaceAll(",", ";") + "; subjectsEvaluation=" + subjectsEvaluation.toString().replaceAll(",", ";") + "; subjectPick=" + subjectPick + "-fold;" + "; dataSeparationProbeGallery=" + nFoldsPG + "-fold\r\n");//
        printHeader();
        for (int m = 0; m < getNumberOfMethods(); m++) {
            Method method = getMethod(m);
            method.learnClassifier(loadMotions(method, subjectsLearning));
            List<Template> templatesEvaluation = method.extractTemplates(loadMotions(method, subjectsEvaluation));
            evaluate(templatesEvaluation, m, 0);
        }
        print();
        System.out.print("--- OPEN SET EVALUATION END ---\r\n");//
    }

    public void setSubjectsOpenSetFixed(String[] subjectsLearning, String[] subjectsEvaluation) {
        subjectPick = "fixed";
        this.subjectsLearning = Arrays.asList(subjectsLearning);
        this.subjectsEvaluation = Arrays.asList(subjectsEvaluation);
    }

    public void setSubjectsOpenSetRandom(int nClassesLearning, int nClassesEvaluation) {
        subjectPick = "random";
        int numberOfClasses = new ClassifiableList(classifiables).splitBySubject().size();
        if (nClassesLearning < 2) {
            System.out.println("nClassesLearning=" + nClassesLearning + " < 2");
            return;
        }
        if (nClassesEvaluation < 2) {
            System.out.println("nClassesEvaluation=" + nClassesEvaluation + " < 2");
            return;
        }
        if (nClassesLearning + nClassesEvaluation > numberOfClasses) {
            System.out.println("nClassesLearning=" + nClassesLearning + " + " + "nClassesEvaluation=" + nClassesEvaluation + " > " + "numberOfClasses=" + numberOfClasses);
            return;
        }
        List<List<Classifiable>> classifiablesBySubject = new ClassifiableList(classifiables).splitBySubject();
        int numberOfSubjects = classifiablesBySubject.size();
        Random random = new Random();
        subjectsLearning = new ArrayList();
        subjectsEvaluation = new ArrayList();
        while (subjectsLearning.size() < nClassesLearning) {
            String subject = classifiablesBySubject.get(random.nextInt(numberOfSubjects)).get(0).getSubject();
            if (!subjectsLearning.contains(subject)) {
                subjectsLearning.add(subject);
            }
        }
        while (subjectsEvaluation.size() < nClassesEvaluation) {
            String subject = classifiablesBySubject.get(random.nextInt(numberOfSubjects)).get(0).getSubject();
            if (!subjectsEvaluation.contains(subject) && !subjectsLearning.contains(subject)) {
                subjectsEvaluation.add(subject);
            }
        }
    }

    private double getDaviesBouldinIndex(Method method, List<Template> templatesEvaluation) { // low is good
        Decision decision = method.getClassifier().getDecision();
        List<List<Template>> templatesBySubject = new ClassifiableList(templatesEvaluation).splitBySubject();
        int numberOfSubjects = templatesBySubject.size();
        double sum = 0;
        for (int c1 = 0; c1 < numberOfSubjects; c1++) {
            List<Template> templatesOfSubject1 = templatesBySubject.get(c1);
            Template centroid1 = getCentroid(method, templatesOfSubject1);
            double avgDistanceToCentroid1 = getMeanDistanceToTemplate(method, templatesOfSubject1, centroid1);
            double maxFrac = 0;
            for (int c2 = c1 + 1; c2 < numberOfSubjects; c2++) {
                List<Template> templatesOfSubject2 = templatesBySubject.get(c2);
                Template centroid2 = getCentroid(method, templatesOfSubject2);
                double avgDistanceToCentroid2 = getMeanDistanceToTemplate(method, templatesOfSubject2, centroid2);
                double frac = (avgDistanceToCentroid1 + avgDistanceToCentroid2) / decision.getDistance(centroid1, centroid2);
                if (maxFrac < frac) {
                    maxFrac = frac;
                }
            }
            sum += maxFrac;
        }
        return sum / numberOfSubjects;
    }

    private double getDunnIndex(Method method, List<Template> templatesEvaluation) { // high is good
        Decision decision = method.getClassifier().getDecision();
        List<List<Template>> templatesBySubject = new ClassifiableList(templatesEvaluation).splitBySubject();
        int numberOfSubjects = templatesBySubject.size();
        double min = Double.MAX_VALUE;
        for (int c1 = 0; c1 < numberOfSubjects; c1++) {
            Template centroid1 = getCentroid(method, templatesBySubject.get(c1));
            for (int c2 = c1 + 1; c2 < numberOfSubjects; c2++) {
                Template centroid2 = getCentroid(method, templatesBySubject.get(c2));
                double dist = decision.getDistance(centroid1, centroid2);
                if (min > dist) {
                    min = dist;
                }
            }
        }
        double max = 0;
        for (int c = 0; c < numberOfSubjects; c++) {
            double dist = getMeanDistanceToCentroid(method, templatesBySubject.get(c));
            if (max < dist) {
                max = dist;
            }
        }
        return min / max;
    }

    private double getSilhouetteCoefficient(Method method, List<Template> templatesEvaluation) { // high is good
        List<List<Template>> templatesBySubject = new ClassifiableList(templatesEvaluation).splitBySubject();
        double sum = 0;
        for (List<Template> templatesOfThisSubject : templatesBySubject) {
            String thisSubject = templatesOfThisSubject.get(0).getSubject();
            for (Template template : templatesOfThisSubject) {
                double avgA = getMeanDistanceToTemplate(method, templatesOfThisSubject, template);
                double minAvgB = Double.MAX_VALUE;
                for (List<Template> templatesOfOtherSubject : templatesBySubject) {
                    String otherSubject = templatesOfOtherSubject.get(0).getSubject();
                    if (!thisSubject.equals(otherSubject)) {
                        double avgB = getMeanDistanceToTemplate(method, templatesOfOtherSubject, template);
                        if (minAvgB > avgB) {
                            minAvgB = avgB;
                        }
                    }
                }
                sum += (minAvgB - avgA) / Math.max(avgA, minAvgB);
            }
        }
        return sum / templatesEvaluation.size();
    }

    private double getFishersDiscriminantRatio(Method method, List<Template> templatesEvaluation) { // high is good
        List<List<Template>> templatesBySubject = new ClassifiableList(templatesEvaluation).splitBySubject();
        return getBetweenClassScatter(method, templatesBySubject) / getWithinClassScatter(method, templatesBySubject);
    }

    private double getPurity(List<List<Template>> clusters) { // high is good
        int numberOfTemplatesOfDominantSubjects = 0;
        int numberOfTemplatesOfAllSubjects = 0;
        for (List<Template> cluster : clusters) {
            List<List<Template>> templatesBySubject = new ClassifiableList(cluster).splitBySubject();
            int numberOfTemplatesOfDominantSubject = 0;
            for (List<Template> templatesOfSubject : templatesBySubject) {
                int numberOfTemplatesOfSubject = templatesOfSubject.size();
                if (numberOfTemplatesOfDominantSubject < numberOfTemplatesOfSubject) {
                    numberOfTemplatesOfDominantSubject = numberOfTemplatesOfSubject;
                }
            }
            numberOfTemplatesOfDominantSubjects += numberOfTemplatesOfDominantSubject;
            numberOfTemplatesOfAllSubjects += cluster.size();
        }
        return (double) numberOfTemplatesOfDominantSubjects / numberOfTemplatesOfAllSubjects;
    }

    private double getRandIndex(int tp, int fp, int fn, int tn) { // high is good
        return (double) (tp + tn) / (tp + fp + fn + tn);
    }

    private double getFmeasure(int tp, int fp, int fn, int tn) { // high is good
        double p = (double) tp / (tp + fp);
        double r = (double) tp / (tp + fn);
        return 2 * p * r / (p + r);
    }

    private double getJaccardIndex(int tp, int fp, int fn, int tn) { // high is good
        return (double) tp / (tp + fp + fn);
    }

    private double getFowlkesMallowsIndex(int tp, int fp, int fn, int tn) { // high is good
        return Math.sqrt(((double) tp / (tp + fp)) * ((double) tp / (tp + fn)));
    }

    private double getBetweenClassScatter(Method method, List<List<Template>> templatesBySubject) {
        List<Template> centroids = new ArrayList();
        for (List<Template> templatesOfSubject : templatesBySubject) {
            centroids.add(getCentroid(method, templatesOfSubject));
        }
        return getMeanDistanceToCentroid(method, centroids);
    }

    private double getWithinClassScatter(Method method, List<List<Template>> templatesBySubject) {
        double sum = 0;
        for (List<Template> templatesOfSubject : templatesBySubject) {
            sum += getMeanDistanceToCentroid(method, templatesOfSubject);
        }
        return sum / templatesBySubject.size();
    }

    private double getDistance(Method method, Template template1, Template template2) {
        return method.getClassifier().getDecision().getDistance(template1, template2);
    }

    private double getMeanDistanceToCentroid(Method method, List<Template> templates) {
        return getMeanDistanceToTemplate(method, templates, getCentroid(method, templates));
    }

    private double getMeanDistanceToTemplate(Method method, List<Template> templates, Template template) {
        double sum = 0;
        for (Template otherTemplate : templates) {
            sum += getDistance(method, template, otherTemplate);
        }
        return sum / templates.size();
    }

    private Template getCentroid(Method method, List<Template> templates) {
        Template centroid = new Template(null, null);
        if (templates.isEmpty()) {
            System.out.println("empty class");
        } else {
            double minSum = Double.MAX_VALUE;
            for (Template centroidCandidate : templates) {
                double sum = 0;
                for (Template template : templates) {
                    sum += getDistance(method, template, centroidCandidate);
                }
                if (minSum > sum) {
                    minSum = sum;
                    centroid = centroidCandidate;
                }
            }
        }
        return centroid;
    }

    private int[] getContingency(List<List<Template>> clusters) {
        List<String> subjectsList = new ArrayList();
        List<Integer> clustersList = new ArrayList();
        for (int c = 0; c < clusters.size(); c++) {
            for (Template template : clusters.get(c)) {
                subjectsList.add(template.getSubject());
                clustersList.add(c);
            }
        }
        int numberOfTemplates = subjectsList.size();
        int tp = 0;
        int fp = 0;
        int fn = 0;
        int tn = 0;
        for (int i = 0; i < numberOfTemplates - 1; i++) {
            String subjectI = subjectsList.get(i);
            int clusterI = clustersList.get(i);
            for (int j = i + 1; j < numberOfTemplates; j++) {
                String subjectJ = subjectsList.get(j);
                int clusterJ = clustersList.get(j);
                boolean sameSubject = subjectI.equals(subjectJ);
                boolean sameCluster = clusterI == clusterJ;
                if (sameSubject && sameCluster) {
                    tp++;
                }
                if (!sameSubject && sameCluster) {
                    fp++;
                }
                if (sameSubject && !sameCluster) {
                    fn++;
                }
                if (!sameSubject && !sameCluster) {
                    tn++;
                }
            }
        }
        return new int[]{tp, fp, fn, tn};
    }

    private double getTemplateDimensionality(List<Template> templatesEvaluation) {
        int sum = 0;
        for (Template template : templatesEvaluation) {
            sum += template.getDimension() * template.getLength();
        }
        return (double) sum / templatesEvaluation.size();
    }

    private double[] getFARFRR(double radius, List<Template> templatesEvaluation) {
        double far = 0;
        double frr = 0;
        int countGenuine = 0;
        int countImpostor = 0;
        for (Template templateTest : templatesEvaluation) {
            String templateTestSubject = templateTest.getSubject();
            List<Template> templatesGallery = new ArrayList(templatesEvaluation);
            templatesGallery.remove(templateTest);
            List<Template> templatesGenuine = new ArrayList();
            List<Template> templatesImpostor = new ArrayList();
            for (Template templateGallery : templatesGallery) {
                if (templateGallery.getSubject().equals(templateTestSubject)) {
                    templatesGenuine.add(templateGallery);
                } else {
                    templatesImpostor.add(templateGallery);
                }
            }
            queryRange.setR(radius);
            List<Template> templatesRetrieved = queryRange.query(templateTest, templatesGallery);
            for (Template templateRetrieved : templatesRetrieved) {
                if (!templatesGenuine.contains(templateRetrieved)) {
                    far++;
                }
            }
            for (Template templateGenuine : templatesGenuine) {
                if (!templatesRetrieved.contains(templateGenuine)) {
                    frr++;
                }
            }
            countGenuine += templatesGenuine.size();
            countImpostor += templatesImpostor.size();
        }
        far /= countImpostor;
        frr /= countGenuine;
        return new double[]{far, frr};
    }

    private double findRadiusEER(double beg, double end, List<Template> templatesEvaluation) {
        double radiusRCLPCN = (end + beg) / 2;
        double[] ratesFARFRR = getFARFRR(radiusRCLPCN, templatesEvaluation);
        if (end - beg > 1) {
            if (ratesFARFRR[0] > ratesFARFRR[1]) {
                return findRadiusEER(beg, radiusRCLPCN, templatesEvaluation);
            } else {
                return findRadiusEER(radiusRCLPCN, end, templatesEvaluation);
            }
        }
        return radiusRCLPCN;
    }

    private double[] getTARFAR(double radius, List<Template> templatesEvaluation) {
        double gar = 0;
        double iar = 0;
        int countGenuine = 0;
        int countImpostor = 0;
        for (Template templateTest : templatesEvaluation) {
            String templateTestSubject = templateTest.getSubject();
            List<Template> templatesGallery = new ArrayList(templatesEvaluation);
            templatesGallery.remove(templateTest);
            List<Template> templatesGenuine = new ArrayList();
            List<Template> templatesImpostor = new ArrayList();
            for (Template templateGallery : templatesGallery) {
                if (templateGallery.getSubject().equals(templateTestSubject)) {
                    templatesGenuine.add(templateGallery);
                } else {
                    templatesImpostor.add(templateGallery);
                }
            }
            queryRange.setR(radius);
            List<Template> templatesRetrieved = queryRange.query(templateTest, templatesGallery);
            for (Template templateRetrieved : templatesRetrieved) {
                if (templatesGenuine.contains(templateRetrieved)) {
                    gar++;
                }
                if (templatesImpostor.contains(templateRetrieved)) {
                    iar++;
                }
            }
            countGenuine += templatesGenuine.size();
            countImpostor += templatesImpostor.size();
        }
        iar /= countImpostor;
        gar /= countGenuine;
        return new double[]{gar, iar};
    }

    private double findMaxRadiusGARIAR(double beg, double end, List<Template> templatesGallery) {
        double radiusGARIAR = (end + beg) / 2;
        double[] ratesGARIAR = getTARFAR(radiusGARIAR, templatesGallery);
        if (end - beg > 1) {
            if (ratesGARIAR[0] == 1 && ratesGARIAR[1] == 1) {
                return findMaxRadiusGARIAR(beg, radiusGARIAR, templatesGallery);
            } else {
                return findMaxRadiusGARIAR(radiusGARIAR, end, templatesGallery);
            }
        }
        return end;
    }

    private double getAreaUnderCurve(List<double[]> curve, int index) {
        double auc = 0;
        for (int l = 1; l < curve.size(); l++) {
            auc += 0.5 * Math.abs(curve.get(l)[index] - curve.get(l - 1)[index]) * Math.abs(curve.get(l)[1 - index] + curve.get(l - 1)[1 - index]);
        }
        return auc;
    }

    private double[] getRCLPCN(double radius, List<Template> templatesEvaluation) {
        double rcl = 0;
        double pcn = 0;
        int countRetrieved = 0;
        int countGenuine = 0;
        for (Template templateTest : templatesEvaluation) {
            String templateTestSubject = templateTest.getSubject();
            List<Template> templatesGallery = new ArrayList(templatesEvaluation);
            templatesGallery.remove(templateTest);
            List<Template> templatesGenuine = new ArrayList();
            for (Template templateGallery : templatesGallery) {
                if (templateGallery.getSubject().equals(templateTestSubject)) {
                    templatesGenuine.add(templateGallery);
                }
            }
            queryRange.setR(radius);
            List<Template> templatesRetrieved = queryRange.query(templateTest, templatesGallery);
            for (Template templateRetrieved : templatesRetrieved) {
                if (templatesGenuine.contains(templateRetrieved)) {
                    pcn++;
                }
            }
            for (Template templateGenuine : templatesGenuine) {
                if (templatesRetrieved.contains(templateGenuine)) {
                    rcl++;
                }
            }
            countRetrieved += templatesRetrieved.size();
            countGenuine += templatesGenuine.size();
        }
        pcn = countRetrieved == 0 ? 1 : pcn / countRetrieved;
        rcl /= countGenuine;
        return new double[]{rcl, pcn};
    }

    private double findMaxRadiusRCLPCN(double beg, double end, List<Template> templatesGallery) {
        double radiusRCLPCN = (end + beg) / 2;
        double[] ratesRCLPCN = getRCLPCN(radiusRCLPCN, templatesGallery);
        if (end - beg > 1) {
            if (ratesRCLPCN[0] == 1) {
                return findMaxRadiusRCLPCN(beg, radiusRCLPCN, templatesGallery);
            } else {
                return findMaxRadiusRCLPCN(radiusRCLPCN, end, templatesGallery);
            }
        }
        return end;
    }

    private List<double[]> sortCurve(List<double[]> curve, int index) {
        List<double[]> sortedCurve = new ArrayList();
        int length = curve.size();
        for (int l = 0; l < length; l++) {
            double minPointValue = Double.MAX_VALUE;
            int minPointIndex = 0;
            for (int pointIndex = 0; pointIndex < curve.size(); pointIndex++) {
                if (minPointValue > curve.get(pointIndex)[index]) {
                    minPointValue = curve.get(pointIndex)[index];
                    minPointIndex = pointIndex;
                }
            }
            sortedCurve.add(curve.get(minPointIndex));
            curve.remove(minPointIndex);
        }
        return sortedCurve;
    }

    // PRINT
    private void print() {
        System.out.print("--- RESULTS BEGIN ---\r\n");
        printHeader();
        for (int m = 0; m < getNumberOfMethods(); m++) {
            System.out.print(getMethod(m).getName() + ",");
            System.out.print(getMethod(m).getDescription() + ",");
            System.out.print(toString(scoresCCR.get(m)));
            System.out.print(toString(scoresDCT.get(m)));
            System.out.print(toString(scoresTD.get(m)));
            System.out.print(toString(scoresDBI.get(m)));
            System.out.print(toString(scoresDI.get(m)));
            System.out.print(toString(scoresSC.get(m)));
            System.out.print(toString(scoresFDR.get(m)));
            System.out.print(toString(scoresEER.get(m)));
            System.out.print(toString(scoresAUC.get(m)));
            System.out.print(toString(scoresMAP.get(m)));
            System.out.print(toString(scoresPKmeans.get(m)));
            System.out.print(toString(scoresPAHC.get(m)));
            System.out.print(toString(scoresRIKmeans.get(m)));
            System.out.print(toString(scoresRIAHC.get(m)));
            System.out.print(toString(scoresFKmeans.get(m)));
            System.out.print(toString(scoresFAHC.get(m)));
            System.out.print(toString(scoresJIKmeans.get(m)));
            System.out.print(toString(scoresJIAHC.get(m)));
            System.out.print(toString(scoresFMIKmeans.get(m)));
            System.out.print(toString(scoresFMIAHC.get(m)));
            System.out.print("\r\n");
//            System.out.print("CMC\r\n");
//            for (int rank = 0; rank <= nClasses; rank++) {
//                System.out.print(toString(scoresListCMC.get(m).get(rank)));
//                System.out.print("\r\n");
//            }
//            System.out.print("FAR,FRR,TAR,FAR,RCL,PCN\r\n");
//            for (int gFoldPG = 0; gFoldPG <= fineness; gFoldPG++) {
//                System.out.print(toString(scoresListFARFRR0.get(m).get(gFoldPG)));
//                System.out.print(toString(scoresListFARFRR1.get(m).get(gFoldPG)));
//                System.out.print(toString(scoresListTARFAR0.get(m).get(gFoldPG)));
//                System.out.print(toString(scoresListTARFAR1.get(m).get(gFoldPG)));
//                System.out.print(toString(scoresListRCLPCN0.get(m).get(gFoldPG)));
//                System.out.print(toString(scoresListRCLPCN1.get(m).get(gFoldPG)));
//                System.out.print("\r\n");
//            }
        }
        System.out.print("--- RESULTS END ---\r\n");
    }

    private String toString(List<Double> scores) {
        return (double) Math.round(getMean(scores) * 10000) / 10000 + ",";
    }

    private double getMean(List<Double> scores) {
        double sum = 0;
        for (double score : scores) {
            sum += score;
        }
        return sum / scores.size();
    }

    private void printHeader() {
        System.out.print("method,description,CCR,DCT,TD,DBI,DI,SC,FDR,EER,ROC,PR,P-Kmeans,P-AHC,RI-Kmeans,RI-AHC,F-Kmeans,F-AHC,JI-Kmeans,JI-AHC,FMI-Kmeans,FMI-AHC\r\n");
    }
}
