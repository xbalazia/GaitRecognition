package executor;

import executor.MotionLoader;
import executor.Constants;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import objects.Bone;
import objects.PoseJointCoordinates;
import objects.Joint;
import objects.MotionJointCoordinates;
import objects.Skeleton;
import objects.Trajectory;
import objects.Trajectory3d;
import objects.Triple;

public class MotionLoaderJointCoordinates extends MotionLoader {
    
    @Override
    public String getDescription(){
        return "JointCoordinates";
    }

    @Override
    public MotionJointCoordinates loadMotion(File fileAMC) throws IOException {
        Skeleton skeleton = new Skeleton();
        skeleton.readASF(Constants.prototypicalFileASF.getPath());
        skeleton.readAMC(fileAMC.getPath());
        List<Trajectory> trajectories = new ArrayList();
        for (Bone bone : skeleton.getBones()) {
            Trajectory3d trajectory3d = skeleton.getTrajectory3d(bone.getName());
            List<Triple> positions3d = trajectory3d.getPositions();
            List<Triple> positions = new ArrayList();
            for (int l = 0; l < trajectory3d.getPositions().size(); l++) {
                positions.add(positions3d.get(l));
            }
            trajectories.add(new Trajectory(trajectory3d.getName(), positions));
        }
        List<PoseJointCoordinates> poses = new ArrayList();
        for (int l = 0; l < skeleton.getNumberOfPoses() - 1; l++) {
            List<Joint> joints = new ArrayList();
            joints.add(new Joint("root", skeleton.getRoot().getPositions().get(l))); //pelvis
            for (Bone bone : skeleton.getBones()) {
                Trajectory trajectory = trajectories.get(0);
                for (Trajectory traj : trajectories) {
                    if (traj.getName().equals(bone.getName())) {
                        trajectory = traj;
                    }
                }
//                List<String> removed = Arrays.asList(new String[]{//
//                "lhipjoint",//pelvis
//                "rhipjoint",//pelvis
//                "lfemur",//leg
//                "rfemur",//leg
//                "ltibia",//leg
//                "rtibia",//leg
//                "lfoot",//leg
//                "rfoot",//leg
//                "ltoes",//leg
//                "rtoes",//leg
//                "lowerback",//torso
//                "upperback",//torso
//                "thorax",//torso
//                "lowerneck",//torso
//                "upperneck",//torso
//                "lclavicle",//torso
//                "rclavicle",//torso
//                "lhumerus",//arm
//                "rhumerus",//arm
//                "lradius",//arm
//                "rradius",//arm
//                "lwrist",//arm
//                "rwrist",//arm
//                "lhand",//arm
//                "rhand",//arm
//                "lfingers",//arm
//                "rfingers",//arm
//                "lthumb",//arm
//                "rthumb",//arm
//                "head",//head
//                });//
//                double noise = 0.00;//
//                Random random = new Random();//
//                if (!removed.contains(bone.getName())) {//
//                    double x=trajectory.getPosition(l).x*(1-noise+2*noise*random.nextDouble());//2a
//                    double y=trajectory.getPosition(l).y*(1-noise+2*noise*random.nextDouble());//2a
//                    double z=trajectory.getPosition(l).z*(1-noise+2*noise*random.nextDouble());//2a
//                    double x=random.nextDouble()<noise?random.nextDouble():trajectory.getPosition(l).x;//2b
//                    double y=random.nextDouble()<noise?random.nextDouble():trajectory.getPosition(l).y;//2b
//                    double z=random.nextDouble()<noise?random.nextDouble():trajectory.getPosition(l).z;//2b
//                    joints.add(new Joint(bone.getName(), new Triple(x,y,z)));//
//                }//
                joints.add(new Joint(bone.getName(), trajectory.getPosition(l)));//
            }
            poses.add(new PoseJointCoordinates(joints));
        }
        return new MotionJointCoordinates(fileAMC.getName().split("\\.")[0], poses);
    }
}
