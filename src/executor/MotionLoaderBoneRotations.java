package executor;

import executor.MotionLoader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import objects.PoseBoneRotations;
import objects.Motion;
import objects.MotionBoneRotations;

public class MotionLoaderBoneRotations extends MotionLoader {

    @Override
    public String getDescription(){
        return "BoneRotations";
    }

    @Override
    public Motion loadMotion(File fileAMC) throws IOException {
        String[] excludedBones = new String[]{"root"};
        List<PoseBoneRotations> poses = new ArrayList();
        BufferedReader brAMC = new BufferedReader(new FileReader(fileAMC));
        String lineAMC = brAMC.readLine();
        while (!lineAMC.equals("1")) {
            lineAMC = brAMC.readLine();
        }
        lineAMC = brAMC.readLine();
        while (lineAMC != null) {
            List<Double> boneRotationsList = new ArrayList();
            while (lineAMC != null && lineAMC.split(" ").length != 1) {
                String[] split = lineAMC.split(" ");
                boolean notExcludedBone = true;
                for (String excludedBone : excludedBones) {
                    if (split[0].equals(excludedBone)) {
                        notExcludedBone = false;
                    }
                }
                if (notExcludedBone) {
                    for (int i = 1; i < split.length; i++) {
                        boneRotationsList.add(Double.parseDouble(split[i]));
                    }
                }
                lineAMC = brAMC.readLine();
            }
            double[] boneRotations = new double[boneRotationsList.size()];
            for (int i = 0; i < boneRotationsList.size(); i++) {
                boneRotations[i] = boneRotationsList.get(i);
            }
            poses.add(new PoseBoneRotations(boneRotations));
            lineAMC = brAMC.readLine();
        }
        return new MotionBoneRotations(fileAMC.getName().split("\\.")[0], poses);
    }
}
