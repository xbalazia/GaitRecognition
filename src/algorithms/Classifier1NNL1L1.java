package algorithms;

public class Classifier1NNL1L1 extends Classifier {

    public Classifier1NNL1L1() {
        DecisionKNN decision = new DecisionKNN(1);
        DistanceTemplatesL1 distanceTemplates = new DistanceTemplatesL1();
        distanceTemplates.setDistancePoses(new DistancePosesL1());
        decision.setDistanceTemplates(distanceTemplates);
        setDecision(decision);
    }
}
