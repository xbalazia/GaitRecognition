package algorithms;

import java.util.List;
import objects.Template;

public abstract class Query extends Retriever {

    public Query() {
    }

    public abstract List<Template> query(Template templateTest, List<Template> templatesGallery);
}
