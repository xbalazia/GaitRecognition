package algorithms;

import executor.MotionLoaderJointCoordinates;
import java.util.ArrayList;
import java.util.List;
import objects.Axis;
import objects.Feature;
import objects.Motion;
import objects.MotionJointCoordinates;
import objects.Sample;

public class MethodJiangS extends Method {

    public MethodJiangS(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=JiangS; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) {
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        List<Feature> features = new ArrayList();
        features.add(motionMJC.extractBoneAxisAngleFeature(new Axis("Y"), "rhipjoint", "rfemur"));
        features.add(motionMJC.extractBoneAxisAngleFeature(new Axis("Y"), "rfemur", "rtibia"));
        features.add(motionMJC.extractBoneAxisAngleFeature(new Axis("Y"), "rclavicle", "rhumerus"));
        features.add(motionMJC.extractBoneAxisAngleFeature(new Axis("Y"), "rhumerus", "rwrist"));
        return features;
    }
}
