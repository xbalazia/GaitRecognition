package algorithms;

import java.util.List;
import objects.Template;

public abstract class Decision extends Retriever {

    private List gallery;

    public List getGallery() {
        return gallery;
    }

    public void setGallery(List gallery) {
        this.gallery = gallery;
    }

    public abstract String getDescription();
    
    public abstract void importGallery(List<Template> templatesGallery);

    public abstract String decide(Template template);
}
