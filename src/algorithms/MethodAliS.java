package algorithms;

import executor.MotionLoaderJointCoordinates;
import java.util.ArrayList;
import java.util.List;
import objects.Feature;
import objects.Motion;
import objects.MotionJointCoordinates;
import objects.Parameter;

public class MethodAliS extends Method {

    public MethodAliS(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=AliS; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) { // mean of the hip-knee-ankle triangle areas
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        List<Feature> features = new ArrayList();
        features.add(new Feature(new Parameter("left", motionMJC.extractTriangleAreaFeature("lhipjoint", "lfemur", "ltibia").getMean())));
        features.add(new Feature(new Parameter("right", motionMJC.extractTriangleAreaFeature("rhipjoint", "rfemur", "rtibia").getMean())));
        return features;
    }
}
