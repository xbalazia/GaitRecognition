package algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import objects.ClassifiableList;
import objects.Template;

public class DecisionKNN extends Decision {

    private final int k;

    public DecisionKNN(int k) {
        this.k = k;
    }

    @Override
    public String getDescription() {
        return k + "NN; distanceTemplates=" + getDistanceTemplates().getDescription();
    }

    @Override
    public void importGallery(List<Template> templatesGallery) {
        setGallery(templatesGallery);
    }

    @Override
    public String decide(Template templateProbe) {
        List<Template> gallery = getGallery();
        if (k == 1) {
            Template closestTemplate = gallery.get(0);
            double minDistance = Double.MAX_VALUE;
            for (Template template : gallery) {
                double distance = getDistance(template, templateProbe);
                if (minDistance > distance) {
                    minDistance = distance;
                    closestTemplate = template;
                }
            }
            return closestTemplate.getSubject();
        }
        Map<Double, Template> kClosestTemplates = new HashMap(k);
        double maxCount = 0;
        for (Template template : gallery) {
            double distance = getDistance(template, templateProbe);
            if (kClosestTemplates.size() < k || distance < maxCount) {
                kClosestTemplates.put(distance, template);
                if (kClosestTemplates.size() > k) {
                    kClosestTemplates.remove(maxCount);
                }
            }
            Iterator<Double> iterator = kClosestTemplates.keySet().iterator();
            maxCount = 0;
            while (iterator.hasNext()) {
                Double key = iterator.next();
                if (maxCount < key) {
                    maxCount = key;
                }
            }
        }
        List<List<Template>> kClosestTemplatesBySubject = new ClassifiableList(new ArrayList<Template>(kClosestTemplates.values())).splitBySubject();
        String subject = "";
        maxCount = 0;
        for (List<Template> templatesOfSubject : kClosestTemplatesBySubject) {
            int count = templatesOfSubject.size();
            if (maxCount < count) {
                maxCount = count;
                subject = templatesOfSubject.get(0).getSubject();
            }
        }
        return subject;
    }
}
