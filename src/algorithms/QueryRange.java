package algorithms;

import java.util.ArrayList;
import java.util.List;
import objects.Template;

public class QueryRange extends Query {

    private double r;
    
    public QueryRange() {
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public List<Template> query(Template templateTest, List<Template> templatesGallery) {
        List<Template> retrievedSamples = new ArrayList();
        for (Template template : templatesGallery) { 
            if (getDistance(template, templateTest) <= r) {
                retrievedSamples.add(template);
            }
        }
        return retrievedSamples;
    }
}
