package algorithms;

import executor.MotionLoaderJointCoordinates;
import java.util.ArrayList;
import java.util.List;
import objects.Axis;
import objects.Feature;
import objects.Motion;
import objects.MotionJointCoordinates;
import objects.Parameter;

public class MethodSinhaA extends Method {

    public MethodSinhaA(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=SinhaA; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) {
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        List<Feature> features = new ArrayList();

        //f_A
        features.add(new Feature(new Parameter("a1", motionMJC.extractProjectedPolygonAreaFeature(new String[]{"lowerneck", "lclavicle", "lhipjoint", "root", "rhipjoint", "rclavicle"}, new Axis("X"), new Axis("Y")).getMean())));
        features.add(new Feature(new Parameter("a2", motionMJC.extractProjectedPolygonAreaFeature(new String[]{"root", "rhipjoint", "rfemur", "rtibia", "ltibia", "lfemur", "lhipjoint"}, new Axis("X"), new Axis("Y")).getMean())));

        //f_D
        String[] upperBody = new String[]{"lowerneck", "lclavicle", "lhipjoint", "rhipjoint", "rclavicle"};
        features.add(new Feature(new Parameter("b1", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"lclavicle", "lhumerus", "lwrist"}).getMean())));
        features.add(new Feature(new Parameter("b2", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"rclavicle", "rhumerus", "rwrist"}).getMean())));
        features.add(new Feature(new Parameter("b3", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"lhipjoint", "lfemur", "ltibia"}).getMean())));
        features.add(new Feature(new Parameter("b4", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"rhipjoint", "rfemur", "rtibia"}).getMean())));
        features.add(new Feature(new Parameter("b5", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"lclavicle", "lhumerus", "lwrist"}).getStD())));
        features.add(new Feature(new Parameter("b6", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"rclavicle", "rhumerus", "rwrist"}).getStD())));
        features.add(new Feature(new Parameter("b7", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"lhipjoint", "lfemur", "ltibia"}).getStD())));
        features.add(new Feature(new Parameter("b8", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"rhipjoint", "rfemur", "rtibia"}).getStD())));
        features.add(new Feature(new Parameter("b9", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"lclavicle", "lhumerus", "lwrist"}).getMax())));
        features.add(new Feature(new Parameter("b10", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"rclavicle", "rhumerus", "rwrist"}).getMax())));
        features.add(new Feature(new Parameter("b11", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"lhipjoint", "lfemur", "ltibia"}).getMax())));
        features.add(new Feature(new Parameter("b12", motionMJC.extractCentroidsDistanceFeature(upperBody, new String[]{"rhipjoint", "rfemur", "rtibia"}).getMax())));

        //f_21
        Feature feature = motionMJC.extractBoneAxisAngleFeature(new Axis("Y"), "lfemur", "lhipjoint");
        features.add(new Feature(new Parameter("mean:Y-lfemur-lhipjoint", feature.getMean())));
        features.add(new Feature(new Parameter("std:Y-lfemur-lhipjoint", feature.getStD())));
        features.add(new Feature(new Parameter("max:Y-lfemur-lhipjoint", feature.getMax())));
        feature = motionMJC.extractBoneAxisAngleFeature(new Axis("Y"), "rfemur", "rhipjoint");
        features.add(new Feature(new Parameter("mean:Y-rfemur-rhipjoint", feature.getMean())));
        features.add(new Feature(new Parameter("std:Y-rfemur-rhipjoint", feature.getStD())));
        features.add(new Feature(new Parameter("max:Y-rfemur-rhipjoint", feature.getMax())));
        feature = motionMJC.extractJointAngleFeature("ltibia", "lfemur", "lhipjoint");
        features.add(new Feature(new Parameter("mean:ltibia-lfemur-lhipjoint", feature.getMean())));
        features.add(new Feature(new Parameter("std:ltibia-lfemur-lhipjoint", feature.getStD())));
        features.add(new Feature(new Parameter("max:ltibia-lfemur-lhipjoint", feature.getMax())));
        feature = motionMJC.extractJointAngleFeature("rtibia", "rfemur", "rhipjoint");
        features.add(new Feature(new Parameter("mean:rtibia-rfemur-rhipjoint", feature.getMean())));
        features.add(new Feature(new Parameter("std:rtibia-rfemur-rhipjoint", feature.getStD())));
        features.add(new Feature(new Parameter("max:rtibia-rfemur-rhipjoint", feature.getMax())));
        feature = motionMJC.extractBoneAxisAngleFeature(new Axis("Z"), "lfoot", "ltibia");
        features.add(new Feature(new Parameter("mean:Z-lfoot-ltibia", feature.getMean())));
        features.add(new Feature(new Parameter("std:Z-lfoot-ltibia", feature.getStD())));
        features.add(new Feature(new Parameter("max:Z-lfoot-ltibia", feature.getMax())));
        feature = motionMJC.extractBoneAxisAngleFeature(new Axis("Z"), "rfoot", "rtibia");
        features.add(new Feature(new Parameter("mean:Z-rfoot-rtibia", feature.getMean())));
        features.add(new Feature(new Parameter("std:Z-rfoot-rtibia", feature.getStD())));
        features.add(new Feature(new Parameter("max:Z-rfoot-rtibia", feature.getMax())));

        //f_22
        double height = 0;
        height += motionMJC.extractInterJointDistanceFeature("ltibia", "lfemur").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("rtibia", "rfemur").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("lfemur", "lhipjoint").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("rfemur", "rhipjoint").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("lhipjoint", "root").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("rhipjoint", "root").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("root", "lowerback").getMean();
        height += motionMJC.extractInterJointDistanceFeature("lowerback", "upperback").getMean();
        height += motionMJC.extractInterJointDistanceFeature("upperback", "thorax").getMean();
        height += motionMJC.extractInterJointDistanceFeature("thorax", "lowerneck").getMean();
        height += motionMJC.extractInterJointDistanceFeature("lowerneck", "upperneck").getMean();
        height += motionMJC.extractInterJointDistanceFeature("upperneck", "head").getMean();
        features.add(new Feature(new Parameter("height", height)));
        double legs = 0;
        legs += motionMJC.extractInterJointDistanceFeature("lfoot", "ltibia").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("rfoot", "rtibia").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("ltibia", "lfemur").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("rtibia", "rfemur").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("lfemur", "lhipjoint").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("rfemur", "rhipjoint").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("lhipjoint", "root").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("rhipjoint", "root").getMean();
        features.add(new Feature(new Parameter("legs", legs)));
        double torso = 0;
        torso += motionMJC.extractInterJointDistanceFeature("root", "lowerback").getMean();
        torso += motionMJC.extractInterJointDistanceFeature("lowerback", "upperback").getMean();
        torso += motionMJC.extractInterJointDistanceFeature("upperback", "thorax").getMean();
        torso += motionMJC.extractInterJointDistanceFeature("thorax", "lowerneck").getMean();
        torso += motionMJC.extractInterJointDistanceFeature("lowerneck", "upperneck").getMean();
        torso += motionMJC.extractInterJointDistanceFeature("upperneck", "head").getMean();
        features.add(new Feature(new Parameter("torso", torso)));
        features.add(new Feature(new Parameter("lowerLegLeft", motionMJC.extractInterJointDistanceFeature("ltibia", "lfemur").getMean())));
        features.add(new Feature(new Parameter("lowerLegRight", motionMJC.extractInterJointDistanceFeature("rtibia", "rfemur").getMean())));
        features.add(new Feature(new Parameter("thighLeft", motionMJC.extractInterJointDistanceFeature("lfemur", "lhipjoint").getMean())));
        features.add(new Feature(new Parameter("thighRight", motionMJC.extractInterJointDistanceFeature("rfemur", "rhipjoint").getMean())));
        features.add(new Feature(new Parameter("upperArmLeft", motionMJC.extractInterJointDistanceFeature("lclavicle", "lhumerus").getMean())));
        features.add(new Feature(new Parameter("upperArmRight", motionMJC.extractInterJointDistanceFeature("rclavicle", "rhumerus").getMean())));
        features.add(new Feature(new Parameter("forearmLeft", motionMJC.extractInterJointDistanceFeature("lhumerus", "lwrist").getMean())));
        features.add(new Feature(new Parameter("forearmRight", motionMJC.extractInterJointDistanceFeature("rhumerus", "rwrist").getMean())));
        double stepLength = motionMJC.extractInterJointDistanceFeature("lfoot", "rfoot").getMax();
        features.add(new Feature(new Parameter("steplength", 100 * stepLength)));
        features.add(new Feature(new Parameter("speed", 100 * stepLength / motionMJC.getLength())));

        return features;
    }
}
