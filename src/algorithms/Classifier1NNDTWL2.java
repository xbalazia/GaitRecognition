package algorithms;

public class Classifier1NNDTWL2 extends Classifier {

    public Classifier1NNDTWL2() {
        DecisionKNN decision = new DecisionKNN(1);
        DistanceTemplatesDTW distanceTemplates = new DistanceTemplatesDTW();
        distanceTemplates.setDistancePoses(new DistancePosesL2());
        decision.setDistanceTemplates(distanceTemplates);
        setDecision(decision);
    }
}
