package algorithms;

import executor.MotionLoaderJointCoordinates;
import java.util.ArrayList;
import java.util.List;
import objects.Axis;
import objects.Feature;
import objects.Motion;
import objects.MotionJointCoordinates;
import objects.Parameter;

public class MethodDikovskiB extends Method {

    public MethodDikovskiB(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=DikovskiB; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) { // dataset 3
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        List<Feature> features = new ArrayList();

        // 19 pairs of adjacent joint distances
        features.add(new Feature(new Parameter("mean:root-lhipjoint", motionMJC.extractInterJointDistanceFeature("root", "lhipjoint").getMean())));
        features.add(new Feature(new Parameter("mean:lhipjoint-lfemur", motionMJC.extractInterJointDistanceFeature("lhipjoint", "lfemur").getMean())));
        features.add(new Feature(new Parameter("mean:lfemur-ltibia", motionMJC.extractInterJointDistanceFeature("lfemur", "ltibia").getMean())));
        features.add(new Feature(new Parameter("mean:ltibia-lfoot", motionMJC.extractInterJointDistanceFeature("ltibia", "lfoot").getMean())));
        features.add(new Feature(new Parameter("mean:root-rhipjoint", motionMJC.extractInterJointDistanceFeature("root", "rhipjoint").getMean())));
        features.add(new Feature(new Parameter("mean:rhipjoint-rfemur", motionMJC.extractInterJointDistanceFeature("rhipjoint", "rfemur").getMean())));
        features.add(new Feature(new Parameter("mean:rfemur-rtibia", motionMJC.extractInterJointDistanceFeature("rfemur", "rtibia").getMean())));
        features.add(new Feature(new Parameter("mean:rtibia-rfoot", motionMJC.extractInterJointDistanceFeature("rtibia", "rfoot").getMean())));
        features.add(new Feature(new Parameter("mean:root-lowerback", motionMJC.extractInterJointDistanceFeature("root", "lowerback").getMean())));
        features.add(new Feature(new Parameter("mean:lowerback-thorax", motionMJC.extractInterJointDistanceFeature("lowerback", "thorax").getMean())));
        features.add(new Feature(new Parameter("mean:thorax-head", motionMJC.extractInterJointDistanceFeature("thorax", "head").getMean())));
        features.add(new Feature(new Parameter("mean:thorax-lclavicle", motionMJC.extractInterJointDistanceFeature("thorax", "lclavicle").getMean())));
        features.add(new Feature(new Parameter("mean:lclavicle-lhumerus", motionMJC.extractInterJointDistanceFeature("lclavicle", "lhumerus").getMean())));
        features.add(new Feature(new Parameter("mean:lhumerus-lwrist", motionMJC.extractInterJointDistanceFeature("lhumerus", "lwrist").getMean())));
        features.add(new Feature(new Parameter("mean:lwrist-lhand", motionMJC.extractInterJointDistanceFeature("lwrist", "lhand").getMean())));
        features.add(new Feature(new Parameter("mean:thorax-rclavicle", motionMJC.extractInterJointDistanceFeature("thorax", "rclavicle").getMean())));
        features.add(new Feature(new Parameter("mean:rclavicle-rhumerus", motionMJC.extractInterJointDistanceFeature("rclavicle", "rhumerus").getMean())));
        features.add(new Feature(new Parameter("mean:rhumerus-rwrist", motionMJC.extractInterJointDistanceFeature("rhumerus", "rwrist").getMean())));
        features.add(new Feature(new Parameter("mean:rwrist-rhand", motionMJC.extractInterJointDistanceFeature("rwrist", "rhand").getMean())));

        // 1 step length
        features.add(new Feature(new Parameter("steplength", motionMJC.extractInterJointDistanceFeature("ltibia", "rtibia").getMean())));

        // 1 height
        double height = 0;
        height += motionMJC.extractInterJointDistanceFeature("ltibia", "lfemur").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("rtibia", "rfemur").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("lfemur", "lhipjoint").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("rfemur", "rhipjoint").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("lhipjoint", "root").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("rhipjoint", "root").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("root", "lowerback").getMean();
        height += motionMJC.extractInterJointDistanceFeature("lowerback", "upperback").getMean();
        height += motionMJC.extractInterJointDistanceFeature("upperback", "thorax").getMean();
        height += motionMJC.extractInterJointDistanceFeature("thorax", "lowerneck").getMean();
        height += motionMJC.extractInterJointDistanceFeature("lowerneck", "upperneck").getMean();
        height += motionMJC.extractInterJointDistanceFeature("upperneck", "head").getMean();
        features.add(new Feature(new Parameter("height", height)));

        // 5 * 9 joint angles
        List<Feature> featuresPartial = new ArrayList();
        featuresPartial.add(motionMJC.extractJointAngleFeature("head", "thorax", "root"));
        featuresPartial.add(motionMJC.extractJointAngleFeature("lwrist", "lhumerus", "lclavicle"));
        featuresPartial.add(motionMJC.extractJointAngleFeature("rwrist", "rhumerus", "rclavicle"));
        featuresPartial.add(motionMJC.extractJointAngleFeature("ltibia", "lfemur", "lhipjoint"));
        featuresPartial.add(motionMJC.extractJointAngleFeature("rtibia", "rfemur", "rhipjoint"));
        featuresPartial.add(motionMJC.extractJointAngleFeature("lhipjoint", "rhipjoint", "rfemur"));
        featuresPartial.add(motionMJC.extractJointAngleFeature("rhipjoint", "lhipjoint", "lfemur"));
        featuresPartial.add(motionMJC.extractJointAngleFeature("lclavicle", "rclavicle", "rhumerus"));
        featuresPartial.add(motionMJC.extractJointAngleFeature("rclavicle", "lclavicle", "lhumerus"));
        for (Feature featurePartial : featuresPartial) {
            features.add(new Feature(new Parameter("mean:" + featurePartial.getName(), featurePartial.getMean())));
            features.add(new Feature(new Parameter("std:" + featurePartial.getName(), featurePartial.getStD())));
            features.add(new Feature(new Parameter("min:" + featurePartial.getName(), featurePartial.getMin())));
            features.add(new Feature(new Parameter("max:" + featurePartial.getName(), featurePartial.getMax())));
            features.add(new Feature(new Parameter("meandiff:" + featurePartial.getName(), featurePartial.getMeanDiff())));
        }

        // 5 * angle of shoulders and hips
        Feature shoulders = motionMJC.extractBoneAxisAngleFeature(new Axis("X"), "lclavicle", "rclavicle");
        Feature hips = motionMJC.extractBoneAxisAngleFeature(new Axis("X"), "lhipjoint", "rhipjoint");
        features.add(new Feature(new Parameter("mean:shoulders-hips", shoulders.getMean() + hips.getMean())));
        features.add(new Feature(new Parameter("std:shoulders-hips", shoulders.getStD() + hips.getStD())));
        features.add(new Feature(new Parameter("min:shoulders-hips", shoulders.getMin() + hips.getMin())));
        features.add(new Feature(new Parameter("max:shoulders-hips", shoulders.getMax() + hips.getMax())));
        features.add(new Feature(new Parameter("meandiff:shoulders-hips", shoulders.getMeanDiff() + hips.getMeanDiff())));

        return features;
    }
}
