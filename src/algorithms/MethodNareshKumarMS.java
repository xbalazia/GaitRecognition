package algorithms;

import Jama.Matrix;
import executor.MotionLoaderJointCoordinates;
import executor.Constants;
import java.util.ArrayList;
import java.util.List;
import objects.Feature;
import objects.Motion;
import objects.Parameter;

public class MethodNareshKumarMS extends Method {

    private static final int length = 150; //?

    public MethodNareshKumarMS(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=raw; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
        ((DistanceTemplatesNareshKumarMS) getClassifier().getDecision().getDistanceTemplates()).setMeanMatrix(getMeanMatrix(extractTemplates(motionsLearning)));
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) {
        motion.adjustLinear(length);
        double[][] data = motion.getMatrix().getArray();
        double[] vector = new double[data.length];
        for (int i = 0; i < data.length; i++) {
            vector[i] = data[i][0];
        }
        Matrix matrix = new Matrix(vector, Constants.axes.length * length);
        List<Feature> features = new ArrayList();
        for (int r = 0; r < matrix.getRowDimension(); r++) {
            Parameter[] parameters = new Parameter[matrix.getColumnDimension()];
            for (int c = 0; c < matrix.getColumnDimension(); c++) {
                parameters[c] = new Parameter(String.valueOf(c), matrix.get(r, c));
            }
            features.add(new Feature(parameters));
        }
        return features;
    }
}
