package algorithms;

import executor.MotionLoaderJointCoordinates;
import java.util.ArrayList;
import java.util.List;
import objects.Feature;
import objects.Motion;
import objects.MotionJointCoordinates;

public class MethodAhmedF extends Method {

    public MethodAhmedF(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=AhmedF; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) { // JRD+JRA
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        List<Feature> features = new ArrayList();
        features.add(motionMJC.extractInterJointDistanceFeature("lowerneck", "lfemur"));
        features.add(motionMJC.extractInterJointDistanceFeature("lowerneck", "rfemur"));
        features.add(motionMJC.extractInterJointDistanceFeature("lowerneck", "ltibia"));
        features.add(motionMJC.extractInterJointDistanceFeature("lowerneck", "rtibia"));
        features.add(motionMJC.extractInterJointDistanceFeature("lclavicle", "lfemur"));
        features.add(motionMJC.extractInterJointDistanceFeature("lclavicle", "rfemur"));
        features.add(motionMJC.extractInterJointDistanceFeature("rclavicle", "lfemur"));
        features.add(motionMJC.extractInterJointDistanceFeature("rclavicle", "rfemur"));
        features.add(motionMJC.extractInterJointDistanceFeature("lclavicle", "ltibia"));
        features.add(motionMJC.extractInterJointDistanceFeature("lclavicle", "rtibia"));
        features.add(motionMJC.extractInterJointDistanceFeature("rclavicle", "ltibia"));
        features.add(motionMJC.extractInterJointDistanceFeature("rclavicle", "rtibia"));
        features.add(motionMJC.extractInterJointDistanceFeature("thorax", "lfemur"));
        features.add(motionMJC.extractInterJointDistanceFeature("thorax", "rfemur"));
        features.add(motionMJC.extractInterJointDistanceFeature("thorax", "ltibia"));
        features.add(motionMJC.extractInterJointDistanceFeature("thorax", "rtibia"));
        features.add(motionMJC.extractInterJointDistanceFeature("root", "lfemur"));
        features.add(motionMJC.extractInterJointDistanceFeature("root", "rfemur"));
        features.add(motionMJC.extractInterJointDistanceFeature("root", "ltibia"));
        features.add(motionMJC.extractInterJointDistanceFeature("root", "rtibia"));
        features.add(motionMJC.extractJointAngleFeature("lclavicle", "root", "lfemur"));
        features.add(motionMJC.extractJointAngleFeature("lclavicle", "root", "rfemur"));
        features.add(motionMJC.extractJointAngleFeature("rclavicle", "root", "lfemur"));
        features.add(motionMJC.extractJointAngleFeature("rclavicle", "root", "rfemur"));
        features.add(motionMJC.extractJointAngleFeature("lclavicle", "root", "ltibia"));
        features.add(motionMJC.extractJointAngleFeature("lclavicle", "root", "rtibia"));
        features.add(motionMJC.extractJointAngleFeature("rclavicle", "root", "ltibia"));
        features.add(motionMJC.extractJointAngleFeature("rclavicle", "root", "rtibia"));
        features.add(motionMJC.extractJointAngleFeature("lhipjoint", "root", "lfemur"));
        features.add(motionMJC.extractJointAngleFeature("lhipjoint", "root", "rfemur"));
        features.add(motionMJC.extractJointAngleFeature("rhipjoint", "root", "lfemur"));
        features.add(motionMJC.extractJointAngleFeature("rhipjoint", "root", "rfemur"));
        features.add(motionMJC.extractJointAngleFeature("lhipjoint", "root", "ltibia"));
        features.add(motionMJC.extractJointAngleFeature("lhipjoint", "root", "rtibia"));
        features.add(motionMJC.extractJointAngleFeature("rhipjoint", "root", "ltibia"));
        features.add(motionMJC.extractJointAngleFeature("rhipjoint", "root", "rtibia"));
        return features;
    }
}
