package algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import objects.Template;

public class QueryKNN extends Query {

    private final int k;

    public QueryKNN(int k) {
        this.k = k;
    }

    @Override
    public List<Template> query(Template templateTest, List<Template> templatesGallery) {
        Map<Double, Template> kClosestTemplates = new HashMap(k);
        double maxDistance = 0;
        for (Template template : templatesGallery) {
            double distance = getDistance(template, templateTest);
            if (kClosestTemplates.size() < k || distance < maxDistance) {
                kClosestTemplates.put(distance, template);
                if (kClosestTemplates.size() > k) {
                    kClosestTemplates.remove(maxDistance);
                }
            }
            Iterator<Double> iterator = kClosestTemplates.keySet().iterator();
            maxDistance = 0;
            while (iterator.hasNext()) {
                Double key = iterator.next();
                if (maxDistance < key) {
                    maxDistance = key;
                }
            }
        }
        List<Template> templatesRetrieved = new ArrayList();
        Iterator<Double> iterator = kClosestTemplates.keySet().iterator();
        while (iterator.hasNext()) {
            templatesRetrieved.add(kClosestTemplates.get(iterator.next()));
        }
        return templatesRetrieved;
    }
}
