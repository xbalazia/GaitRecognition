package algorithms;

import java.io.Serializable;
import objects.Template;

public abstract class Classifier implements Serializable {

    private Decision decision;

    public Decision getDecision() {
        return decision;
    }

    public void setDecision(Decision decision) {
        this.decision = decision;
    }

    public String getDescription() {
        return "decision=" + getDecision().getDescription();
    }

    public String classify(Template template) {
        return decision.decide(template);
    }
}
