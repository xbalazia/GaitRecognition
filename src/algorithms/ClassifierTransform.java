package algorithms;

import Jama.Matrix;
import java.util.ArrayList;
import java.util.List;
import objects.ClassifiableList;
import objects.Sample;

public abstract class ClassifierTransform extends Classifier {

    private Matrix transformationMatrix;

    public ClassifierTransform() {
    }

    public Matrix getTransformationMatrix() {
        return transformationMatrix;
    }

    public void setTransformationMatrix(Matrix transformationMatrix) {
        this.transformationMatrix = transformationMatrix;
    }

    public abstract void learnTransformationMatrix(List<Sample> samples);

    public <T extends Sample> Matrix getBetweenClassScatterMatrix(List<T> objects) {
        Matrix meanMatrix = getMeanOfMeansMatrix(objects);
        int dimension = objects.get(0).getDimension();
        Matrix SigmaB = new Matrix(dimension, dimension);
        List<List<T>> samplesBySubject = new ClassifiableList(objects).splitBySubject();
        for (List<T> samplesOfSubject : samplesBySubject) {
            Matrix diff = getMeanMatrix(samplesOfSubject).minus(meanMatrix);
            SigmaB.plusEquals(diff.times(diff.transpose()));
        }
        return SigmaB;
    }

    public <T extends Sample> Matrix getWithinClassScatterMatrix(List<T> objects) {
        int dimension = objects.get(0).getDimension();
        Matrix SigmaW = new Matrix(dimension, dimension);
        List<List<T>> samplesBySubject = new ClassifiableList(objects).splitBySubject();
        for (List<T> samplesOfSubject : samplesBySubject) {
            Matrix sumOfSubject = new Matrix(dimension, dimension);
            Matrix meanMatrixOfSubject = getMeanMatrix(samplesOfSubject);
            for (T object : samplesOfSubject) {
                Matrix diff = object.getMatrix().minus(meanMatrixOfSubject);
                sumOfSubject.plusEquals(diff.times(diff.transpose()));
            }
            SigmaW.plusEquals(sumOfSubject.times((double) 1 / samplesOfSubject.size()));
        }
        return SigmaW;
    }

    public <T extends Sample> Matrix getMeanMatrix(List<T> objects) {
        Matrix meanMatrix = new Matrix(objects.get(0).getDimension(), objects.get(0).getLength());
        for (T object : objects) {
            meanMatrix.plusEquals(object.getMatrix().times((double) 1 / objects.size()));
        }
        return meanMatrix;
    }

    public <T extends Sample> Matrix getMeanOfMeansMatrix(List<T> objects) {
        List<Sample> means = new ArrayList();
        List<List<T>> samplesBySubject = new ClassifiableList(objects).splitBySubject();
        for (List<T> samplesOfSubject : samplesBySubject) {
            means.add(new Sample(null, getMeanMatrix(samplesOfSubject)));
        }
        return getMeanMatrix(means);
    }
}
