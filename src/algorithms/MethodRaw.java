package algorithms;

import executor.MotionLoader;
import java.util.List;
import objects.Feature;
import objects.Motion;

public class MethodRaw extends Method {
    
    public MethodRaw(MotionLoader motionLoader, Classifier classifier) {
        super(classifier);
        setMotionLoader(motionLoader);
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=raw; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) {
        return motion.extractRawFeatures();
    }
}
