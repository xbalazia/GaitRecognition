package algorithms;

import java.util.ArrayList;
import java.util.List;
import objects.Feature;
import objects.Motion;

public class MethodRandom extends Method {

    public MethodRandom(Classifier classifier) {
        super(classifier);
    }
    
    @Override
    public String getDescription() {
        return "data=none; decision="+getClassifier().getDecision().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) {
        return new ArrayList();
    }
}
