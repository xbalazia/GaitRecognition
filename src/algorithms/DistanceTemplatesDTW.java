package algorithms;

import objects.Template;

public final class DistanceTemplatesDTW extends DistanceTemplates {

    private DistancePoses distancePoses;

    public DistanceTemplatesDTW() {
    }

    public void setDistancePoses(DistancePoses distancePoses) {
        this.distancePoses = distancePoses;
    }

    @Override
    public String getDescription() {
        return "DTW; distancePoses=" + distancePoses.getDescription();
    }

    @Override
    public double getDistance(Template template1, Template template2) {
        int n = template1.getLength(), m = template2.getLength();
        double cm[][] = new double[n][m]; // accum matrix
        // create the accum matrix
        cm[0][0] = distancePoses.getDistance(template1.getPose(0), template2.getPose(0));
        for (int i = 1; i < n; i++) {
            cm[i][0] = distancePoses.getDistance(template1.getPose(i), template2.getPose(0)) + cm[i - 1][0];
        }
        for (int j = 1; j < m; j++) {
            cm[0][j] = distancePoses.getDistance(template1.getPose(0), template2.getPose(j)) + cm[0][j - 1];
        }
        // Compute the matrix values
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                // Decide on the path with minimum distance so far
                cm[i][j] = distancePoses.getDistance(template1.getPose(i), template2.getPose(j)) + Math.min(cm[i - 1][j], Math.min(cm[i][j - 1], cm[i - 1][j - 1]));
            }
        }
        return cm[n - 1][m - 1];
    }
}
