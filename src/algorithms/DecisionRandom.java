package algorithms;

import java.util.List;
import java.util.Random;
import objects.Template;

public class DecisionRandom extends Decision {

    @Override
    public String getDescription() {
        return "random";
    }

    @Override
    public void importGallery(List<Template> templatesGallery) {
        setGallery(templatesGallery);
    }

    @Override
    public String decide(Template templateProbe) {
        List<Template> gallery = getGallery();
        return gallery.get(new Random().nextInt(gallery.size())).getSubject();
    }
}
