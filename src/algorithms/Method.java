package algorithms;

import Jama.Matrix;
import executor.MotionLoader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import objects.Feature;
import objects.Motion;
import objects.MotionNull;
import objects.Sample;
import objects.Template;

public abstract class Method implements Serializable {

    private final boolean exportTemplatesToCSV = false;
    private Classifier classifier;
    private String[] parameterNames;
    private MotionLoader motionLoader;

    public Method(Classifier classifier) {
        this.classifier = classifier;
    }
    
    public String getName() {
        return this.getClass().getName().split("Method")[1].split("\\.")[0];
    }

    public abstract String getDescription();

    public MotionLoader getMotionLoader() {
        return motionLoader;
    }

    public void setMotionLoader(MotionLoader motionLoader) {
        this.motionLoader = motionLoader;
    }

    public Classifier getClassifier() {
        return classifier;
    }

    public void setClassifier(Classifier classifier) {
        this.classifier = classifier;
    }

    public String[] getParameterNames() {
        return parameterNames;
    }

    public void setParameterNames(String[] parameterNames) {
        this.parameterNames = parameterNames;
    }

    // LOAD MOTIONS
    public Motion loadMotion(File fileAMC) throws IOException {
        if (motionLoader == null) {
            return new MotionNull(fileAMC.getName().split("\\.")[0], null);
        }
        return motionLoader.loadMotion(fileAMC);
    }

    public List<Motion> loadMotions(File directoryAMC) throws IOException {
        List<Motion> motionsLoaded = new ArrayList();
        for (File fileAMC : directoryAMC.listFiles()) {
            motionsLoaded.add(loadMotion(fileAMC));
        }
        return motionsLoaded;
    }

    // LEARN CLASSIFIER
    public abstract void learnClassifier(List<Motion> motionsLearning);

    public <T extends Sample> Matrix getTotalScatterMatrix(List<T> objects) {
        Matrix meanMatrix = getMeanMatrix(objects);
        int dimension = objects.get(0).getDimension();
        Matrix sumDiff = new Matrix(dimension, dimension);
        for (T object : objects) {
            Matrix diff = object.getMatrix().minus(meanMatrix);
            sumDiff.plusEquals(diff.times(diff.transpose()));
        }
        return sumDiff.times((double) 1 / objects.size());
    }

    public <T extends Sample> Matrix getMeanMatrix(List<T> objects) {
        Matrix meanMatrix = new Matrix(objects.get(0).getDimension(), objects.get(0).getLength());
        for (T object : objects) {
            meanMatrix.plusEquals(object.getMatrix().times((double) 1 / objects.size()));
        }
        return meanMatrix;
    }

    public void saveClassifier(File fileClassifier) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileClassifier));
        oos.writeObject(classifier);
        oos.close();
    }

    // EXTRACT TEMPLATES
    public abstract List<Feature> extractFeatures(Motion motion);

    public List<Template> extractTemplates(List<Motion> motions) {
        List<Template> templates = new ArrayList();
        for (Motion motion : motions) {
            templates.add(extractTemplate(motion));
        }
        if (exportTemplatesToCSV) {
            exportTemplatesToCSV(new File(getName() + ".csv"), templates);
        }
        return templates;
    }

    public Template extractTemplate(Motion motion) {
        List<Feature> features = extractFeatures(motion);
        int dimension = features.size();
        Matrix matrix = dimension == 0 ? new Matrix(new double[][]{new double[]{}}) : new Matrix(dimension, features.get(0).getLength());
        parameterNames = new String[dimension];
        for (int d = 0; d < dimension; d++) {
            double[] row = features.get(d).getParameterValues();
            matrix.setMatrix(d, d, 0, matrix.getColumnDimension() - 1, new Matrix(row, 1));
            parameterNames[d] = features.get(d).getName();
        }
        return new Template(motion.getIdentifier(), matrix);
    }

    private void exportTemplatesToCSV(File fileCSV, List<Template> templates) {
        try {
            System.out.print("saving " + fileCSV.getName() + "\r\n");
            FileWriter writer = new FileWriter(fileCSV);
            Template template0 = templates.get(0);
            writer.append("subject").append(",");
            for (int l = 0; l < template0.getLength(); l++) {
                for (String parameterName : parameterNames) {
                    writer.append(String.valueOf(l)).append(":").append(parameterName).append(",");
                }
            }
            writer.append("\r\n");
            for (Template template : templates) {
                writer.append(String.valueOf(template.getSubject()));
                for (int l = 0; l < template.getLength(); l++) {
                    for (int d = 0; d < template.getDimension(); d++) {
                        writer.append(",").append(String.valueOf(template.getParameterValue(d, l)));
                    }
                }
                writer.append("\r\n");
            }
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Method.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
