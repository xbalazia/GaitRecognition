package algorithms;

import executor.MotionLoaderJointCoordinates;
import java.util.ArrayList;
import java.util.List;
import objects.Axis;
import objects.Feature;
import objects.Motion;
import objects.MotionJointCoordinates;
import objects.Parameter;

public class MethodAhmedM extends Method {

    public MethodAhmedM(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=AhmedM; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) { // HDF and VDF
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        List<Feature> features = new ArrayList();

        features.add(new Feature(new Parameter("meanHDF1", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "ltibia", "rtibia").getMean())));
        features.add(new Feature(new Parameter("meanHDF2", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lfemur", "rfemur").getMean())));
        features.add(new Feature(new Parameter("meanHDF3", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lwrist", "rwrist").getMean())));
        features.add(new Feature(new Parameter("meanHDF4", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lclavicle", "rclavicle").getMean())));
        features.add(new Feature(new Parameter("stdHDF1", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "ltibia", "rtibia").getMean())));
        features.add(new Feature(new Parameter("stdHDF2", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lfemur", "rfemur").getMean())));
        features.add(new Feature(new Parameter("stdHDF3", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lwrist", "rwrist").getMean())));
        features.add(new Feature(new Parameter("stdHDF4", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lclavicle", "rclavicle").getMean())));
        features.add(new Feature(new Parameter("skewHDF1", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "ltibia", "rtibia").getMean())));
        features.add(new Feature(new Parameter("skewHDF2", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lfemur", "rfemur").getMean())));
        features.add(new Feature(new Parameter("skewHDF3", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lwrist", "rwrist").getMean())));
        features.add(new Feature(new Parameter("skewHDF4", motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lclavicle", "rclavicle").getMean())));

        features.add(new Feature(new Parameter("meanVDF1", motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "head").getMean())));
        features.add(new Feature(new Parameter("meanVDF2", motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "rwrist").getMean())));
        features.add(new Feature(new Parameter("meanVDF3", motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "rclavicle").getMean())));
        features.add(new Feature(new Parameter("meanVDF4", motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "rtibia").getMean())));
        features.add(new Feature(new Parameter("meanVDF5", motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "ltibia").getMean())));
        features.add(new Feature(new Parameter("meanVDF6", 0.5 * motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lfoot", "rfoot").getMean() * motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "root").getMean())));
        features.add(new Feature(new Parameter("stdVDF1", motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "head").getStD())));
        features.add(new Feature(new Parameter("stdVDF2", motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "rwrist").getStD())));
        features.add(new Feature(new Parameter("stdVDF3", motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "rclavicle").getStD())));
        features.add(new Feature(new Parameter("stdVDF4", motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "rtibia").getStD())));
        features.add(new Feature(new Parameter("stdVDF5", motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "ltibia").getStD())));
        features.add(new Feature(new Parameter("stdVDF6", 0.5 * motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lfoot", "rfoot").getStD() * motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "root").getStD())));

        return features;
    }
}
