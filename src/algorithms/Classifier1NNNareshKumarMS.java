package algorithms;

public class Classifier1NNNareshKumarMS extends Classifier {

    public Classifier1NNNareshKumarMS() {
        DecisionKNN decision = new DecisionKNN(1);
        decision.setDistanceTemplates(new DistanceTemplatesNareshKumarMS());
        setDecision(decision);
    }
}
