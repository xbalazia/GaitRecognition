package algorithms;

import Jama.Matrix;
import objects.Template;

public final class DistanceTemplatesMahalanobis extends DistanceTemplates {

    private Matrix totalScatterMatrix;

    public DistanceTemplatesMahalanobis() {
    }

    public void setTotalScatterMatrix(Matrix totalScatterMatrix) {
        this.totalScatterMatrix = totalScatterMatrix;
    }

    @Override
    public String getDescription() {
        return "Mahalanobis";
    }
    
    @Override
    public double getDistance(Template template1, Template template2) {
        Matrix diff = template1.getMatrix().minus(template2.getMatrix());
        return Math.sqrt(diff.transpose().times(totalScatterMatrix).times(diff).get(0, 0));
    }
}
