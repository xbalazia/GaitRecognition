package algorithms;

import executor.MotionLoaderJointCoordinates;
import java.util.ArrayList;
import java.util.List;
import objects.Axis;
import objects.Feature;
import objects.PoseJointCoordinates;
import objects.Motion;
import objects.MotionJointCoordinates;
import objects.Parameter;

public class MethodAnderssonVO extends Method {

    public MethodAnderssonVO(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=AnderssonVO; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) { //36 gait attributes: (mean and std) of ((min and max) of (4 pairs of (lower body angles))), step length (max of ltibia-rtibia), stride length (2x step length), cycle time (length / 120), velocity (stride length / cycle time); 32 anthropometric attributes: (mean and std) of bone lengths and height
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        int length = motionMJC.getLength();
        List<Feature> features = new ArrayList();
        String[][] angleStrings = new String[][]{{"Y", "lhipjoint", "lfemur"}, {"Y", "rhipjoint", "rfemur"}, {"Y", "lfemur", "ltibia"}, {"Y", "rfemur", "rtibia"}, {"Y", "ltibia", "lfoot"}, {"Y", "rtibia", "rfoot"}, {"Z", "ltibia", "lfoot"}, {"Z", "rtibia", "rfoot"}};
        for (String[] angleString : angleStrings) {
            Parameter[] angleParameters = new Parameter[length];
            for (int l = 0; l < length; l++) {
                angleParameters[l] = ((PoseJointCoordinates) motionMJC.getPose(l)).extractBoneAxisAngleParameter(new Axis(angleString[0]), angleString[1], angleString[2]);
            }
            Feature angleFeature = new Feature(angleParameters);
            List<Parameter> angleLocalMins = new ArrayList();
            angleLocalMins.add(angleFeature.getParameter(0)); // 0%
            double minValue = Double.MAX_VALUE;
            int begin = length / 4;
            Parameter angleLocalMin = angleFeature.getParameter(0);
            for (int l = begin; l < begin + length / 2; l++) {
                double value = angleFeature.getParameter(l).getValue();
                if (minValue > value) {
                    minValue = value;
                    angleLocalMin = angleFeature.getParameter(l);
                }
            }
            List<Parameter> angleLocalMaxs = new ArrayList();
            double maxValue = 0;
            begin = 0;
            Parameter angleLocalMax = angleFeature.getParameter(begin);
            for (int l = begin; l < begin + length / 2; l++) {
                double value = angleFeature.getParameter(l).getValue();
                if (maxValue < value) {
                    maxValue = value;
                    angleLocalMax = angleFeature.getParameter(l);
                }
            }
            angleLocalMaxs.add(angleLocalMax); // 25%
            angleLocalMins.add(angleLocalMin); // 50%
            maxValue = 0f;
            begin = length / 2;
            angleLocalMax = angleFeature.getParameter(begin);
            for (int l = begin; l < begin + length / 2; l++) {
                double value = angleFeature.getParameter(l).getValue();
                if (maxValue < value) {
                    maxValue = value;
                    angleLocalMax = angleFeature.getParameter(l);
                }
            }
            angleLocalMaxs.add(angleLocalMax); // 75%
            angleLocalMins.add(angleFeature.getParameter(length - 1)); // 100%
            features.add(new Feature(new Parameter("mean:min-" + angleString[0] + "-" + angleString[1] + "-" + angleString[2], new Feature(angleLocalMins.toArray(new Parameter[angleLocalMins.size()])).getMean())));
            features.add(new Feature(new Parameter("mean:max-" + angleString[0] + "-" + angleString[1] + "-" + angleString[2], new Feature(angleLocalMaxs.toArray(new Parameter[angleLocalMaxs.size()])).getMean())));
            features.add(new Feature(new Parameter("std:min-" + angleString[0] + "-" + angleString[1] + "-" + angleString[2], new Feature(angleLocalMins.toArray(new Parameter[angleLocalMins.size()])).getStD())));
            features.add(new Feature(new Parameter("std:max-" + angleString[0] + "-" + angleString[1] + "-" + angleString[2], new Feature(angleLocalMaxs.toArray(new Parameter[angleLocalMaxs.size()])).getStD())));
        }
        double stepLength = motionMJC.extractInterJointDistanceFeature("ltibia", "rtibia").getMax();
        double strideLength = stepLength * 2;
        double cycleTime = (double) motionMJC.getLength() / 120;
        double velocity = strideLength / cycleTime;
        features.add(new Feature(new Parameter("steplength", stepLength)));
        features.add(new Feature(new Parameter("strideLength", strideLength)));
        features.add(new Feature(new Parameter("cycleTime", cycleTime)));
        features.add(new Feature(new Parameter("velocity", velocity)));

        Feature footLeft = motionMJC.extractInterJointDistanceFeature("lfoot", "ltibia");
        double meanFootLeft = footLeft.getMean();
        double stdFootLeft = footLeft.getStD();
        features.add(new Feature(new Parameter("mean:footLeft", meanFootLeft)));
        features.add(new Feature(new Parameter("std:footLeft", stdFootLeft)));
        Feature footRight = motionMJC.extractInterJointDistanceFeature("rfoot", "rtibia");
        double meanFootRight = footRight.getMean();
        double stdFootRight = footRight.getStD();
        features.add(new Feature(new Parameter("mean:footRight", meanFootRight)));
        features.add(new Feature(new Parameter("std:footRight", stdFootRight)));
        Feature legLeft = motionMJC.extractInterJointDistanceFeature("ltibia", "lfemur");
        double meanLegLeft = legLeft.getMean();
        double stdLegLeft = legLeft.getStD();
        features.add(new Feature(new Parameter("mean:legLeft", meanLegLeft)));
        features.add(new Feature(new Parameter("std:legLeft", stdLegLeft)));
        Feature legRight = motionMJC.extractInterJointDistanceFeature("rtibia", "rfemur");
        double meanLegRight = legRight.getMean();
        double stdLegRight = legRight.getStD();
        features.add(new Feature(new Parameter("mean:legRight", meanLegRight)));
        features.add(new Feature(new Parameter("std:legRight", stdLegRight)));
        Feature thighLeft = motionMJC.extractInterJointDistanceFeature("lfemur", "lhipjoint");
        double meanThighLeft = thighLeft.getMean();
        double stdThighLeft = thighLeft.getStD();
        features.add(new Feature(new Parameter("mean:thighLeft", meanThighLeft)));
        features.add(new Feature(new Parameter("std:thighLeft", stdThighLeft)));
        Feature thighRight = motionMJC.extractInterJointDistanceFeature("rfemur", "rhipjoint");
        double meanThighRight = thighRight.getMean();
        double stdThighRight = thighRight.getStD();
        features.add(new Feature(new Parameter("mean:thighRight", meanThighRight)));
        features.add(new Feature(new Parameter("std:thighRight", stdThighRight)));
        Feature hipLeft = motionMJC.extractInterJointDistanceFeature("lhipjoint", "root");
        double meanHipLeft = hipLeft.getMean();
        double stdHipLeft = hipLeft.getStD();
        features.add(new Feature(new Parameter("mean:hipLeft", meanHipLeft)));
        features.add(new Feature(new Parameter("std:hipLeft", stdHipLeft)));
        Feature hipRight = motionMJC.extractInterJointDistanceFeature("rhipjoint", "root");
        double meanHipRight = hipRight.getMean();
        double stdHipRight = hipRight.getStD();
        features.add(new Feature(new Parameter("mean:hipRight", meanHipRight)));
        features.add(new Feature(new Parameter("std:hipRight", stdHipRight)));
        Feature forearmLeft = motionMJC.extractInterJointDistanceFeature("lwrist", "lhumerus");
        features.add(new Feature(new Parameter("mean:forearmLeft", forearmLeft.getMean())));
        features.add(new Feature(new Parameter("std:forearmLeft", forearmLeft.getStD())));
        Feature forearmRight = motionMJC.extractInterJointDistanceFeature("rwrist", "rhumerus");
        features.add(new Feature(new Parameter("mean:forearmRight", forearmRight.getMean())));
        features.add(new Feature(new Parameter("std:forearmRight", forearmRight.getStD())));
        Feature upperArmLeft = motionMJC.extractInterJointDistanceFeature("lhumerus", "lclavicle");
        features.add(new Feature(new Parameter("mean:upperArmLeft", upperArmLeft.getMean())));
        features.add(new Feature(new Parameter("std:upperArmLeft", upperArmLeft.getStD())));
        Feature upperArmRight = motionMJC.extractInterJointDistanceFeature("rhumerus", "rclavicle");
        features.add(new Feature(new Parameter("mean:upperArmRight", upperArmRight.getMean())));
        features.add(new Feature(new Parameter("std:upperArmRight", upperArmRight.getStD())));
        Feature dRootLowerback = motionMJC.extractInterJointDistanceFeature("root", "lowerback");
        Feature dLowerbackUpperback = motionMJC.extractInterJointDistanceFeature("lowerback", "upperback");
        Feature dUpperbackThorax = motionMJC.extractInterJointDistanceFeature("upperback", "thorax");
        double meanLowerSpine = dRootLowerback.getMean() + dLowerbackUpperback.getMean() + dUpperbackThorax.getMean();
        double stdLowerSpine = dRootLowerback.getStD() + dLowerbackUpperback.getStD() + dUpperbackThorax.getStD();
        features.add(new Feature(new Parameter("mean:lowerSpine", meanLowerSpine)));
        features.add(new Feature(new Parameter("std:lowerSpine", stdLowerSpine)));
        Feature upperSpine = motionMJC.extractInterJointDistanceFeature("thorax", "lowerneck");
        double meanUpperSpine = upperSpine.getMean();
        double stdUpperSpine = upperSpine.getStD();
        features.add(new Feature(new Parameter("mean:upperSpine", meanUpperSpine)));
        features.add(new Feature(new Parameter("std:upperSpine", stdUpperSpine)));
        Feature dLowerneckUpperneck = motionMJC.extractInterJointDistanceFeature("lowerneck", "upperneck");
        Feature dUpperneckHead = motionMJC.extractInterJointDistanceFeature("upperneck", "head");
        double meanNeck = dLowerneckUpperneck.getMean() + dUpperneckHead.getMean();
        double stdNeck = dLowerneckUpperneck.getStD() + dUpperneckHead.getStD();
        features.add(new Feature(new Parameter("mean:neck", meanNeck)));
        features.add(new Feature(new Parameter("std:neck", stdNeck)));
        features.add(new Feature(new Parameter("mean:height", (meanFootLeft + meanFootRight + meanLegLeft + meanLegRight + meanThighLeft + meanThighRight + meanHipLeft + meanHipRight) / 2 + meanLowerSpine + meanUpperSpine + meanNeck)));
        features.add(new Feature(new Parameter("std:height", (stdFootLeft + stdFootRight + stdLegLeft + stdLegRight + stdThighLeft + stdThighRight + stdHipLeft + stdHipRight) / 2 + stdLowerSpine + stdUpperSpine + stdNeck)));
        return features;
    }
}
