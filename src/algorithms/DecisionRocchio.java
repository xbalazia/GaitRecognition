package algorithms;

import java.util.ArrayList;
import java.util.List;
import objects.ClassifiableList;
import objects.Template;

public class DecisionRocchio extends Decision {

    @Override
    public String getDescription() {
        return "Rocchio";
    }

    @Override
    public void importGallery(List<Template> templatesGallery) {
        List<Template> centroids = new ArrayList();
        List<List<Template>> templatesBySubject = new ClassifiableList(templatesGallery).splitBySubject();
        for (List<Template> templatesOfSubject : templatesBySubject) {
            if (templatesOfSubject.isEmpty()) {
                System.out.println("empty class");
            } else {
                Template centroid = templatesOfSubject.get(0);
                double minSum = Double.MAX_VALUE;
                for (Template centroidCandidate : templatesOfSubject) {
                    double sum = 0;
                    for (Template template : templatesOfSubject) {
                        sum += getDistance(template, centroidCandidate);
                    }
                    if (minSum > sum) {
                        minSum = sum;
                        centroid = centroidCandidate;
                    }
                }
                centroids.add(centroid);
            }
        }
        setGallery(centroids);
    }

    @Override
    public String decide(Template templateProbe) {
        List<Template> centroids = getGallery();
        Template closestCentroid = centroids.get(0);
        double minDistance = Double.MAX_VALUE;
        for (Template centroid : centroids) {
            double distance = getDistance(centroid, templateProbe);
            if (minDistance > distance) {
                minDistance = distance;
                closestCentroid = centroid;
            }
        }
        return closestCentroid.getSubject();
    }
}
