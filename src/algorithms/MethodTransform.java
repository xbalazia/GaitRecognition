package algorithms;

import executor.MotionLoader;
import java.util.ArrayList;
import java.util.List;
import objects.Feature;
import objects.Motion;
import objects.Parameter;
import objects.Sample;

public class MethodTransform extends Method {

    private final int length = 150;

    public MethodTransform(MotionLoader motionLoader, ClassifierTransform classifier) {
        super(classifier);
        setMotionLoader(motionLoader);
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {

        // TRANSFORMATION MATRIX
        List<Sample> samples = new ArrayList();
        for (Motion motion : motionsLearning) {
            motion.adjustLinear(length);
            samples.add(new Sample(motion.getIdentifier(), motion.getMatrix()));
        }
        ClassifierTransform classifier = (ClassifierTransform) getClassifier();
        classifier.learnTransformationMatrix(samples);

        // DISTANCE TEMPLATES
        ((DistanceTemplatesMahalanobis) classifier.getDecision().getDistanceTemplates()).setTotalScatterMatrix(getTotalScatterMatrix(extractTemplates(motionsLearning)).inverse());
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) {
        motion.adjustLinear(length);
        double[][] data = ((ClassifierTransform) getClassifier()).getTransformationMatrix().transpose().times(motion.getMatrix()).getArray();
        List<Feature> features = new ArrayList();
        for (int d = 0; d < data.length; d++) {
            features.add(new Feature(new Parameter[]{new Parameter(String.valueOf(d), data[d][0])}));
        }
        return features;
    }
}
