package algorithms;

import executor.MotionLoaderJointCoordinates;
import executor.Constants;
import java.util.ArrayList;
import java.util.List;
import objects.Axis;
import objects.Feature;
import objects.Motion;
import objects.MotionJointCoordinates;

public class MethodKrzeszowskiT extends Method {

    public MethodKrzeszowskiT(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=KrzeszowskiT; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) { // 8 bones * 3 angles + height + ankledistance
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        List<Feature> features = new ArrayList();
        for (Axis axis : Constants.axes) {
            features.add(motionMJC.extractBoneAxisAngleFeature(axis, "ltibia", "lfemur"));
            features.add(motionMJC.extractBoneAxisAngleFeature(axis, "rtibia", "rfemur"));
            features.add(motionMJC.extractBoneAxisAngleFeature(axis, "lfemur", "lhipjoint"));
            features.add(motionMJC.extractBoneAxisAngleFeature(axis, "rfemur", "rhipjoint"));
            features.add(motionMJC.extractBoneAxisAngleFeature(axis, "lwrist", "lhumerus"));
            features.add(motionMJC.extractBoneAxisAngleFeature(axis, "rwrist", "rhumerus"));
            features.add(motionMJC.extractBoneAxisAngleFeature(axis, "lhumerus", "lclavicle"));
            features.add(motionMJC.extractBoneAxisAngleFeature(axis, "rhumerus", "rclavicle"));
        }
        features.add(motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "head"));
        features.add(motionMJC.extractInterJointDistanceFeature("ltibia", "rtibia"));
        return features;
    }
}
