package algorithms;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import objects.Template;

public final class DistanceTemplatesNareshKumarMS extends DistanceTemplates {
    
    private Matrix meanMatrix;

    public DistanceTemplatesNareshKumarMS() {
    }

    public void setMeanMatrix(Matrix meanMatrix) {
        this.meanMatrix = meanMatrix;
    }

    @Override
    public String getDescription() {
        return "NareshKumarMS";
    }

    @Override
    public double getDistance(Template template1, Template template2) {
        double sum = 0.0;
        double[] generalizedEigenvalues = new EigenvalueDecomposition(getCovarianceMatrix(template2).inverse().times(getCovarianceMatrix(template1))).getRealEigenvalues();
        for (int l = 0; l < generalizedEigenvalues.length; l++) {
            sum += Math.pow(Math.log(Math.abs(generalizedEigenvalues[l])), 2);
        }
        return Math.sqrt(sum);
    }

    private Matrix getCovarianceMatrix(Template template) {
        Matrix matrix = template.getMatrix();
        int rows = matrix.getRowDimension();
        int columns = matrix.getColumnDimension();
        Matrix covarianceMatrix = new Matrix(columns, columns);
        for (int r = 0; r < rows; r++) {
            Matrix d = matrix.getMatrix(r, r, 0, columns - 1);
            Matrix u = meanMatrix.getMatrix(r, r, 0, columns - 1);
            Matrix diff = d.minus(u);
            covarianceMatrix.plusEquals((diff.transpose().times(diff)).times((double) 1 / (rows - 1)));
        }
        return covarianceMatrix;
    }
}
