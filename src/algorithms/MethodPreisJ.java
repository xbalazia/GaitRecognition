package algorithms;

import executor.MotionLoaderJointCoordinates;
import java.util.ArrayList;
import java.util.List;
import objects.Feature;
import objects.Motion;
import objects.MotionJointCoordinates;
import objects.Parameter;

public class MethodPreisJ extends Method {

    public MethodPreisJ(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=PreisJ; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) {
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        List<Feature> features = new ArrayList();

        // height
        double height = 0;
        height += motionMJC.extractInterJointDistanceFeature("ltibia", "lfemur").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("rtibia", "rfemur").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("lfemur", "lhipjoint").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("rfemur", "rhipjoint").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("lhipjoint", "root").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("rhipjoint", "root").getMean() / 2;
        height += motionMJC.extractInterJointDistanceFeature("root", "lowerback").getMean();
        height += motionMJC.extractInterJointDistanceFeature("lowerback", "upperback").getMean();
        height += motionMJC.extractInterJointDistanceFeature("upperback", "thorax").getMean();
        height += motionMJC.extractInterJointDistanceFeature("thorax", "lowerneck").getMean();
        height += motionMJC.extractInterJointDistanceFeature("lowerneck", "upperneck").getMean();
        height += motionMJC.extractInterJointDistanceFeature("upperneck", "head").getMean();
        features.add(new Feature(new Parameter("height", height)));

        // legs
        double legs = 0;
        legs += motionMJC.extractInterJointDistanceFeature("lfoot", "ltibia").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("rfoot", "rtibia").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("ltibia", "lfemur").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("rtibia", "rfemur").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("lfemur", "lhipjoint").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("rfemur", "rhipjoint").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("lhipjoint", "root").getMean();
        legs += motionMJC.extractInterJointDistanceFeature("rhipjoint", "root").getMean();
        features.add(new Feature(new Parameter("legs", legs)));

        // torso
        double torso = 0;
        torso += motionMJC.extractInterJointDistanceFeature("root", "lowerback").getMean();
        torso += motionMJC.extractInterJointDistanceFeature("lowerback", "upperback").getMean();
        torso += motionMJC.extractInterJointDistanceFeature("upperback", "thorax").getMean();
        torso += motionMJC.extractInterJointDistanceFeature("thorax", "lowerneck").getMean();
        torso += motionMJC.extractInterJointDistanceFeature("lowerneck", "upperneck").getMean();
        torso += motionMJC.extractInterJointDistanceFeature("upperneck", "head").getMean();
        features.add(new Feature(new Parameter("torso", torso)));

        // lower legs
        features.add(new Feature(new Parameter("lowerLegLeft", motionMJC.extractInterJointDistanceFeature("ltibia", "lfemur").getMean())));
        features.add(new Feature(new Parameter("lowerLegRight", motionMJC.extractInterJointDistanceFeature("rtibia", "rfemur").getMean())));

        // thighs
        features.add(new Feature(new Parameter("thighLeft", motionMJC.extractInterJointDistanceFeature("lfemur", "lhipjoint").getMean())));
        features.add(new Feature(new Parameter("thighRight", motionMJC.extractInterJointDistanceFeature("rfemur", "rhipjoint").getMean())));

        // upper arms
        features.add(new Feature(new Parameter("upperArmLeft", motionMJC.extractInterJointDistanceFeature("lclavicle", "lhumerus").getMean())));
        features.add(new Feature(new Parameter("upperArmRight", motionMJC.extractInterJointDistanceFeature("rclavicle", "rhumerus").getMean())));

        // forearms
        features.add(new Feature(new Parameter("forearmLeft", motionMJC.extractInterJointDistanceFeature("lhumerus", "lwrist").getMean())));
        features.add(new Feature(new Parameter("forearmRight", motionMJC.extractInterJointDistanceFeature("rhumerus", "rwrist").getMean())));

        // step length and speed
        double stepLength = motionMJC.extractInterJointDistanceFeature("lfoot", "rfoot").getMax();
        features.add(new Feature(new Parameter("steplength", 100 * stepLength)));
        features.add(new Feature(new Parameter("speed", 100 * stepLength / motionMJC.getLength())));
        return features;
    }
}
