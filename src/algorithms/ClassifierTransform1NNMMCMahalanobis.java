package algorithms;

import Jama.Matrix;
import Jama.SingularValueDecomposition;
import java.util.ArrayList;
import java.util.List;
import objects.ClassifiableList;
import objects.Sample;

public class ClassifierTransform1NNMMCMahalanobis extends ClassifierTransform {

    @Override
    public String getDescription() {
        return "features=MMC; decision=" + getDecision().getDescription();
    }

    public ClassifierTransform1NNMMCMahalanobis() {
        DecisionKNN decision = new DecisionKNN(1);
        decision.setDistanceTemplates(new DistanceTemplatesMahalanobis());
        setDecision(decision);
    }

    @Override
    public void learnTransformationMatrix(List<Sample> samples) {
        int dimension = samples.get(0).getDimension();
        Matrix meanSampleMatrix = getMeanOfMeansMatrix(samples);
        int numberOfSamples = samples.size();
        List<List<Sample>> samplesBySubject = new ClassifiableList(samples).splitBySubject();
        int numberOfClasses = samplesBySubject.size();
        Matrix Chi = new Matrix(dimension, numberOfSamples);
        for (int n = 0; n < numberOfSamples; n++) {
            double[] column = (samples.get(n).getMatrix().minus(meanSampleMatrix)).getColumnPackedCopy();
            Chi.setMatrix(0, dimension - 1, n, n, new Matrix(column, column.length));
        }
        Chi.timesEquals((double) 1 / Math.sqrt(numberOfSamples));
        Matrix Upsilon = new Matrix(dimension, numberOfClasses);
        for (int c = 0; c < numberOfClasses; c++) {
            List<Sample> samplesOfClass = samplesBySubject.get(c);
            double[] column = (getMeanMatrix(samplesOfClass).minus(meanSampleMatrix)).times(Math.sqrt((double) samplesOfClass.size() / numberOfSamples)).getColumnPackedCopy();
            Upsilon.setMatrix(0, dimension - 1, c, c, new Matrix(column, column.length));
        }
        SingularValueDecomposition svdChi;
        Matrix Omega;
        if (dimension < numberOfSamples) {
            svdChi = new SingularValueDecomposition(Chi.transpose());
            Omega = svdChi.getV().transpose();
        } else {
            svdChi = new SingularValueDecomposition(Chi);
            Omega = svdChi.getU();
        }
        Matrix ThetaInverseSquareRoot = svdChi.getS();
        int dim = Math.min(ThetaInverseSquareRoot.getRowDimension(), ThetaInverseSquareRoot.getColumnDimension());
        for (int d = 0; d < dim; d++) {
            ThetaInverseSquareRoot.set(d, d, (double) 1 / Math.sqrt(ThetaInverseSquareRoot.get(d, d)));
        }
        SingularValueDecomposition svdThetaOmegaUpsilon = new SingularValueDecomposition(ThetaInverseSquareRoot.times(Omega.transpose()).times(Upsilon));
        Matrix Xi = svdThetaOmegaUpsilon.getU();
        Matrix Psi = Omega.times(ThetaInverseSquareRoot).times(Xi);
        Matrix Delta = Psi.transpose().times(getBetweenClassScatterMatrix(samples)).times(Psi);
        List<Integer> indexes = new ArrayList();
        for (int c = 0; c < numberOfClasses; c++) {
            if (Delta.get(c, c) >= 0.5) {
                indexes.add(c);
            }
        }
        int dimensionTemplate = indexes.size();
        Matrix Phi = new Matrix(Psi.getRowDimension(), dimensionTemplate);
        for (int d = 0; d < dimensionTemplate; d++) {
            int index = indexes.get(d);
            double[] column = Psi.getMatrix(0, dimension - 1, index, index).getColumnPackedCopy();
            Phi.setMatrix(0, dimension - 1, d, d, new Matrix(column, column.length));
        }
        setTransformationMatrix(Phi);
    }
}
