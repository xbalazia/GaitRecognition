package algorithms;

public class Classifier1NNDTWKrzeszowskiT extends Classifier {

    public Classifier1NNDTWKrzeszowskiT() {
        DecisionKNN decision = new DecisionKNN(1);
        DistanceTemplatesDTW distanceTemplates = new DistanceTemplatesDTW();
        distanceTemplates.setDistancePoses(new DistancePosesKrzeszowskiT());
        decision.setDistanceTemplates(distanceTemplates);
        setDecision(decision);
    }

    public final class DistancePosesKrzeszowskiT extends DistancePoses {

        @Override
        public String getDescription() {
            return "KrzeszowskiT";
        }

        @Override
        public double getDistance(double[] pose1, double[] pose2) {
            double w1 = 57.3;
            double w2 = 1.7;
            int length = pose1.length;
            if (pose2.length != length) {
                System.out.println("wrong dimensions");
            }
            double rho = 0;
            int d = 0;
            while (d < length - 2) {
                double sumAngles = 0;
                for (int i = 0; i < 3; i++) {
                    sumAngles += Math.pow(180 - Math.abs(Math.abs(pose1[d + i] - pose2[d + i]) - 180), 2);
                    d++;
                }
                rho += Math.sqrt(sumAngles);
            }
            double heightAndAnkleDistanceDifference = 0;
            while (d < length) {
                heightAndAnkleDistanceDifference += Math.abs(pose1[d] - pose2[d]);
                d++;
            }
            return w1 * rho + w2 * heightAndAnkleDistanceDifference;
        }
    }
}
