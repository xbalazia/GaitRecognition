package algorithms;

import java.util.List;
import objects.Template;

public abstract class Clusterer extends Retriever {

    public Clusterer() {
    }

    public abstract List<List<Template>> cluster(List<Template> templatesGallery);
    
    public Template getCentroid(List<Template> templates) {
        Template centroid = new Template(null, null);
        if (templates.isEmpty()) {
            System.out.println("empty class");
        } else {
            double minSum = Double.MAX_VALUE;
            for (Template centroidCandidate : templates) {
                double sum = 0;
                for (Template template : templates) {
                    sum += getDistance(template, centroidCandidate);
                }
                if (minSum > sum) {
                    minSum = sum;
                    centroid = centroidCandidate;
                }
            }
        }
        return centroid;
    }
}
