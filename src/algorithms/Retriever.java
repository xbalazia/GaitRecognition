package algorithms;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import objects.Template;

public abstract class Retriever implements Serializable {

    private DistanceTemplates distanceTemplates;
    private Map<String, Double> distanceMap = new HashMap();

    public DistanceTemplates getDistanceTemplates() {
        return distanceTemplates;
    }

    public void setDistanceTemplates(DistanceTemplates distanceTemplates) {
        this.distanceTemplates = distanceTemplates;
    }

    public Map<String, Double> getDistanceMap() {
        return distanceMap;
    }

    public void setDistanceMap(Map<String, Double> distanceMap) {
        this.distanceMap = distanceMap;
    }

    private String createKey(Template template1, Template template2) {
        return template1.getIdentifier() + "#" + template2.getIdentifier();
    }

    public void calculateDistanceMap(List<Template> templates) {
        distanceMap = new HashMap();
        for (Template template1 : templates) {
            for (Template template2 : templates) {
                putDistance(template1, template2);
            }
        }
    }

    public double getDistance(Template template1, Template template2) {
        String key = createKey(template1, template2);
        if (!distanceMap.containsKey(key)) {
            putDistance(template1, template2);
        }
        return distanceMap.get(key);
    }

    private void putDistance(Template template1, Template template2) {
        distanceMap.put(createKey(template1, template2), distanceTemplates.getDistance(template1, template2));
    }
}
