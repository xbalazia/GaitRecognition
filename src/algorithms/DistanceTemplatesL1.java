package algorithms;

import objects.Template;

public final class DistanceTemplatesL1 extends DistanceTemplates {

    private DistancePoses distancePoses;

    public DistanceTemplatesL1() {
    }

    public void setDistancePoses(DistancePoses distancePoses) {
        this.distancePoses = distancePoses;
    }

    @Override
    public String getDescription() {
        return "L1; distancePoses=" + distancePoses.getDescription();
    }

    @Override
    public double getDistance(Template template1, Template template2) {
        int length = template1.getLength();
        if (length != template2.getLength()) {
            System.out.println("different lengths");
            return 0;
        }
        double distance = 0;
        for (int l = 0; l < length; l++) {
            distance += Math.abs(distancePoses.getDistance(template1.getPose(l), template2.getPose(l)));
        }
        return distance;
    }
}
