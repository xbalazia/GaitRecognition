package algorithms;

import executor.MotionLoaderJointCoordinates;
import java.util.ArrayList;
import java.util.List;
import objects.Feature;
import objects.Motion;
import objects.MotionJointCoordinates;

public class MethodSedmidubskyJ extends Method {

    public MethodSedmidubskyJ(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=SedmidubskyJ; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) { // DTDS (left clavicle, left hand) and (right clavicle, right hand)
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        List<Feature> features = new ArrayList();
        features.add(motionMJC.extractInterJointDistanceFeature("lclavicle", "lhand"));
        features.add(motionMJC.extractInterJointDistanceFeature("rclavicle", "rhand"));
        return features;
    }
}
