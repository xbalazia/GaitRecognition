package algorithms;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import com.mkobos.pca_transform.PCA;
import java.util.List;
import objects.ClassifiableList;
import objects.Sample;

public class ClassifierTransform1NNPCALDAMahalanobis extends ClassifierTransform {

    public ClassifierTransform1NNPCALDAMahalanobis() {
        DecisionKNN decision = new DecisionKNN(1);
        decision.setDistanceTemplates(new DistanceTemplatesMahalanobis());
        setDecision(decision);
    }

    @Override
    public String getDescription() {
        return "features=PCA+LDA; decision=" + getDecision().getDescription();
    }

    @Override
    public void learnTransformationMatrix(List<Sample> samples) {
        int dimension = samples.get(0).getDimension();
        Matrix meanSampleMatrix = getMeanOfMeansMatrix(samples);
        Matrix samplesMatrix = new Matrix(samples.size(), dimension);
        for (int d = 0; d < dimension; d++) {
            for (int s = 0; s < samples.size(); s++) {
                samplesMatrix.set(s, d, samples.get(s).getParameterValue(d, 0) - meanSampleMatrix.get(d, 0));
            }
        }
        Matrix PhiPCA = new PCA(samplesMatrix).getEigenvectorsMatrix().getMatrix(0, dimension - 1, 0, new ClassifiableList(samples).splitBySubject().size() - 1);
        Matrix PhiLDA = new EigenvalueDecomposition((PhiPCA.transpose().times(getWithinClassScatterMatrix(samples)).times(PhiPCA)).inverse().times(PhiPCA.transpose().times(getBetweenClassScatterMatrix(samples)).times(PhiPCA))).getV();
        setTransformationMatrix(PhiPCA.times(PhiLDA));
    }
}
