package algorithms;

import java.util.ArrayList;
import java.util.List;
import objects.Template;

public class ClustererAgglomerativeHierarchical extends Clusterer {

    private final int k;
    private final double d;

    public ClustererAgglomerativeHierarchical(int k) {
        this.k = k;
        this.d = Double.MAX_VALUE;
    }

    public ClustererAgglomerativeHierarchical(double d) {
        this.k = Integer.MAX_VALUE;
        this.d = d;
    }

    @Override
    public List<List<Template>> cluster(List<Template> templatesGallery) {
        int numberOfTemplates = templatesGallery.size();
        if (numberOfTemplates < k) {
            System.out.println(numberOfTemplates + " < " + k);
        }
        List<List<Template>> clusters = new ArrayList();
        List<Template> centroids = new ArrayList();
        for (Template template : templatesGallery) {
            List<Template> cluster = new ArrayList();
            cluster.add(template);
            clusters.add(cluster);
            centroids.add(template);
        }
        double minDistance = 0;
        while (clusters.size() > k && minDistance < d) {
            int numberOfClusters = clusters.size();
            List<Template> clusterI = clusters.get(0);
            List<Template> clusterJ = clusters.get(0);
            Template centroidI = centroids.get(0);
            Template centroidJ = centroids.get(0);
            minDistance = Double.MAX_VALUE;
            for (int i = 0; i < numberOfClusters - 1; i++) {
                for (int j = i + 1; j < numberOfClusters; j++) {
                    double distance;
                    distance = getDistance(centroids.get(i), centroids.get(j));
                    if (minDistance > distance) {
                        minDistance = distance;
                        clusterI = clusters.get(i);
                        clusterJ = clusters.get(j);
                        centroidI = centroids.get(i);
                        centroidJ = centroids.get(j);
                    }
                }
            }
            clusters.remove(clusterI);
            clusters.remove(clusterJ);
            centroids.remove(centroidI);
            centroids.remove(centroidJ);
            clusterI.addAll(clusterJ);
            clusters.add(clusterI);
            centroids.add(getCentroid(clusterI));
        }
        return clusters;
    }
}
