package algorithms;

import java.io.Serializable;
    
public abstract class DistancePoses implements Serializable {
    
    public DistancePoses() {
    }
    
    public abstract String getDescription();

    public abstract double getDistance(double[] pose1, double[] pose2);
}
