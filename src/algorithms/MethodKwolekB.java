package algorithms;

import executor.MotionLoaderJointCoordinates;
import java.util.ArrayList;
import java.util.List;
import objects.Axis;
import objects.Feature;
import objects.Motion;
import objects.MotionJointCoordinates;

public class MethodKwolekB extends Method {

    private static final int length = 30;
    
    public MethodKwolekB(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=KwolekB; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) { // 20 angles + height + step length
        motion.adjustLinear(length);
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        List<Feature> features = new ArrayList();

        // spine
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("X"), "root", "upperneck"));
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Y"), "root", "upperneck"));
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "root", "upperneck"));

        // head
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("X"), "upperneck", "head"));
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Y"), "upperneck", "head"));
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "upperneck", "head"));

        // right upper leg
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Y"), "rhipjoint", "rfemur"));
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "rhipjoint", "rfemur"));
        
        // right lower leg
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Y"), "rfemur", "rtibia"));

        // left upper leg
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Y"), "lhipjoint", "lfemur"));
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lhipjoint", "lfemur"));
        
        // left lower leg
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Y"), "lfemur", "ltibia"));

        // right upper arm
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("X"), "rhipjoint", "rfemur"));
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Y"), "rhipjoint", "rfemur"));
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "rhipjoint", "rfemur"));
        
        // right lower arm
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Y"), "rfemur", "rtibia"));

        // left upper arm
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("X"), "lhipjoint", "lfemur"));
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Y"), "lhipjoint", "lfemur"));
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Z"), "lhipjoint", "lfemur"));
        
        // left lower arm
        features.add(motionMJC.extractJointAxisDistanceFeature(new Axis("Y"), "lfemur", "ltibia"));
        
        // step length and height
        features.add(motionMJC.extractInterJointDistanceFeature("ltibia", "rtibia"));
        features.add(motionMJC.extractJointAxisCoordinateFeature(new Axis("Y"), "head"));
        
        return features;
    }
}
