package algorithms;

public final class DistancePosesL2 extends DistancePoses {

    public DistancePosesL2() {
    }

    @Override
    public String getDescription() {
        return "L2";
    }
    
    @Override
    public double getDistance(double[] pose1, double[] pose2) {
        if (pose1.length != pose2.length) {
            System.out.println("wrong dimensions");
            return 0;
        }
        double sum = 0;
        for (int d = 0; d < pose1.length; d++) {
            sum += Math.pow(pose1[d] - pose2[d], 2);
        }
        return Math.sqrt(sum);
    }
}
