package algorithms;

import java.io.Serializable;
import objects.Template;

public abstract class DistanceTemplates implements Serializable {
    
    public abstract String getDescription();

    public abstract double getDistance(Template template1, Template template2);
}
