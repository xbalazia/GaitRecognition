package algorithms;

public class Classifier1NNDTWL1 extends Classifier {

    public Classifier1NNDTWL1() {
        DecisionKNN decision = new DecisionKNN(1);
        DistanceTemplatesDTW distanceTemplates = new DistanceTemplatesDTW();
        distanceTemplates.setDistancePoses(new DistancePosesL1());
        decision.setDistanceTemplates(distanceTemplates);
        setDecision(decision);
    }
}
