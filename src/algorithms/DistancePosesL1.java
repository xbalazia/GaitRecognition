package algorithms;

public final class DistancePosesL1 extends DistancePoses {

    public DistancePosesL1() {
    }

    @Override
    public String getDescription() {
        return "L1";
    }
    
    @Override
    public double getDistance(double[] pose1, double[] pose2) {
        if (pose1.length != pose2.length) {
            System.out.println("wrong dimensions");
            return 0;
        }
        double sum = 0;
        for (int d = 0; d < pose1.length; d++) {
            sum += Math.abs(pose1[d] - pose2[d]);
        }
        return sum;
    }
}
