package algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import objects.Template;

public class ClustererKmeans extends Clusterer {

    private final int k;

    public ClustererKmeans(int k) {
        this.k = k;
    }

    @Override
    public List<List<Template>> cluster(List<Template> templatesGallery) {
        int numberOfTemplates = templatesGallery.size();
        if (numberOfTemplates < k) {
            System.out.println(numberOfTemplates + " < " + k);
            return null;
        }
        Random random = new Random();
        List<Integer> seeds = new ArrayList();
        for (int i = 0; i < k; i++) {
            int seed = random.nextInt(numberOfTemplates);
            while (seeds.contains(seed)) {
                seed = random.nextInt(numberOfTemplates);
            }
            seeds.add(seed);
        }
        List<Template> centroids = new ArrayList();
        for (int i = 0; i < k; i++) {
            centroids.add(templatesGallery.get(seeds.get(i)));
        }
        List<List<Template>> clusters = new ArrayList();
        boolean done = false;
        while (!done) {//CHANGE
            done = true;
            clusters = new ArrayList();
            for (int i = 0; i < k; i++) {
                clusters.add(new ArrayList());
            }
            for (Template template : templatesGallery) {
                double minDistance = Double.MAX_VALUE;
                int closestCentroidIndex = 0;
                for (int i = 0; i < k; i++) {
                    double distance = getDistance(template, centroids.get(i));
                    if (minDistance > distance) {
                        minDistance = distance;
                        closestCentroidIndex = i;
                    }
                }
                clusters.get(closestCentroidIndex).add(template);
            }
            for (int i = 0; i < k; i++) {
                Template centroidOld = centroids.get(i);
                Template centroidNew = getCentroid(clusters.get(i));
                centroids.set(i, centroidNew);
                if(!centroidOld.equals(centroidNew)){
                    done = false;
                }
            }
        }
        return clusters;
    }
}
