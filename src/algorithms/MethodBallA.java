package algorithms;

import executor.MotionLoaderJointCoordinates;
import java.util.ArrayList;
import java.util.List;
import objects.Axis;
import objects.Feature;
import objects.Motion;
import objects.MotionJointCoordinates;
import objects.Parameter;

public class MethodBallA extends Method {

    public MethodBallA(Classifier classifier) {
        super(classifier);
        setMotionLoader(new MotionLoaderJointCoordinates());
    }

    @Override
    public String getDescription() {
        return "data=" + getMotionLoader().getDescription() + "; features=BallA; " + getClassifier().getDescription();
    }

    @Override
    public void learnClassifier(List<Motion> motionsLearning) {
    }

    @Override
    public List<Feature> extractFeatures(Motion motion) { // mean, std and max of (3 pairs of (lower body angles))
        MotionJointCoordinates motionMJC = (MotionJointCoordinates) motion;
        List<Feature> features = new ArrayList();
        Feature feature = motionMJC.extractBoneAxisAngleFeature(new Axis("Y"), "lfemur", "lhipjoint");
        features.add(new Feature(new Parameter("mean:Y-lfemur-lhipjoint", feature.getMean())));
        features.add(new Feature(new Parameter("std:Y-lfemur-lhipjoint", feature.getStD())));
        features.add(new Feature(new Parameter("max:Y-lfemur-lhipjoint", feature.getMax())));
        feature = motionMJC.extractBoneAxisAngleFeature(new Axis("Y"), "rfemur", "rhipjoint");
        features.add(new Feature(new Parameter("mean:Y-rfemur-rhipjoint", feature.getMean())));
        features.add(new Feature(new Parameter("std:Y-rfemur-rhipjoint", feature.getStD())));
        features.add(new Feature(new Parameter("max:Y-rfemur-rhipjoint", feature.getMax())));
        feature = motionMJC.extractJointAngleFeature("ltibia", "lfemur", "lhipjoint");
        features.add(new Feature(new Parameter("mean:ltibia-lfemur-lhipjoint", feature.getMean())));
        features.add(new Feature(new Parameter("std:ltibia-lfemur-lhipjoint", feature.getStD())));
        features.add(new Feature(new Parameter("max:ltibia-lfemur-lhipjoint", feature.getMax())));
        feature = motionMJC.extractJointAngleFeature("rtibia", "rfemur", "rhipjoint");
        features.add(new Feature(new Parameter("mean:rtibia-rfemur-rhipjoint", feature.getMean())));
        features.add(new Feature(new Parameter("std:rtibia-rfemur-rhipjoint", feature.getStD())));
        features.add(new Feature(new Parameter("max:rtibia-rfemur-rhipjoint", feature.getMax())));
        feature = motionMJC.extractBoneAxisAngleFeature(new Axis("Z"), "lfoot", "ltibia");
        features.add(new Feature(new Parameter("mean:Z-lfoot-ltibia", feature.getMean())));
        features.add(new Feature(new Parameter("std:Z-lfoot-ltibia", feature.getStD())));
        features.add(new Feature(new Parameter("max:Z-lfoot-ltibia", feature.getMax())));
        feature = motionMJC.extractBoneAxisAngleFeature(new Axis("Z"), "rfoot", "rtibia");
        features.add(new Feature(new Parameter("mean:Z-rfoot-rtibia", feature.getMean())));
        features.add(new Feature(new Parameter("std:Z-rfoot-rtibia", feature.getStD())));
        features.add(new Feature(new Parameter("max:Z-rfoot-rtibia", feature.getMax())));
        return features;
    }
}
