This project has it's home page on https://gait.fi.muni.cz, where one finds publications 
documenting this framework and database. Details related to the framework are in the
paper (preprint on https://arxiv.org/abs/1701.00995):
    An Evaluation Framework and Database for MoCap-Based Gait Recognition Methods
    Michal Balazia, Petr Sojka
    15 pages. Full paper published at the 1st IAPR Workshop on Reproducible 
    Research in Pattern Recognition, Cancun, Mexico, December 2016.
    DOI: <a href="https://doi.org/10.1007/978-3-319-56414-2_3">10.1007/978-3-319-56414-2_3</a>
Read the Section 5 of this paper to reproduce the gait recognition evaluation and
use the gait database for comparison. 